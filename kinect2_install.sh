#!/bin/bash
# Date: 2015-01-21
cd /tmp
git clone https://github.com/OpenPTrack/libfreenect2.git

cd libfreenect2/depends/
sudo apt-get install git cmake cmake-curses-gui libXmu-dev libXi-dev libgl-dev dos2unix xorg-dev libglu1-mesa-dev libtool automake libudev-dev libgtk2.0-dev pkg-config libjpeg-turbo8-dev libturbojpeg libglew-dev libglewmx-dev
./install_ubuntu.sh
if [ ! -f /usr/lib/x86_64-linux-gnu/libturbojpeg.so ]
then
    sudo ln -s /usr/lib/x86_64-linux-gnu/libturbojpeg.so.0.0.0 /usr/lib/x86_64-linux-gnu/libturbojpeg.so
fi
cd ../examples/protonect/
mkdir build
cd build
cmake ..
make -j8 -l8
sudo make install

#install iai-kinect
#cd $ROS_WORKSPACE
source /home/orobot/workspace/ros/catkin/devel/setup.bash
roscd
cd ../src/

git clone https://github.com/OpenPTrack/iai_kinect2.git

Catkin

echo '# ATTR{product}=="Kinect2"
SUBSYSTEM=="usb", ATTR{idVendor}=="045e", ATTR{idProduct}=="02c4", MODE="0666"
SUBSYSTEM=="usb", ATTR{idVendor}=="045e", ATTR{idProduct}=="02d8", MODE="0666"
SUBSYSTEM=="usb", ATTR{idVendor}=="045e", ATTR{idProduct}=="02d9", MODE="0666"
' > ~/90-kinect2.rules

sudo mv ~/90-kinect2.rules /etc/udev/rules.d/90-kinect2.rules
