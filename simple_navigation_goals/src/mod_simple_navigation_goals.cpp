#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <visualization_msgs/MarkerArray.h>
#include <std_srvs/Empty.h>
#include "coverage_planner/CoveragePlanner.h"

//#include <geometry_msgs/PoseWithCovarianceStamped.h>
//#include <tf/transform_listener.h>
#include <clear_costmap_recovery/clear_costmap_recovery.h>
//#include <kobuki_msgs/BumperEvent.h>
#include <geometry_msgs/Twist.h>

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

const double PI = std::atan(1.0)*4;
const int RECOVERY_THRESHOLD = 2;
const int TRIAL_THRESHOLD = 5;
const int MAX_TRIALS = 10;

uint k = 0;
visualization_msgs::MarkerArray marker_array;

void publishMarker(const std::vector<move_base_msgs::MoveBaseGoal>& goals, const int i)
{
    for(int j = 0; j < marker_array.markers.size(); ++j)
    {
        visualization_msgs::Marker& marker = marker_array.markers[j];
        marker.header.seq = ++k;
        marker.header.frame_id = "map";
        marker.header.stamp = ros::Time();
        marker.ns = "my_namespace";
        marker.id = j;
        marker.type = visualization_msgs::Marker::SPHERE;
        marker.action = visualization_msgs::Marker::ADD;
        marker.pose.position.x = goals[j].target_pose.pose.position.x;
        marker.pose.position.y = goals[j].target_pose.pose.position.y;
        marker.pose.position.z = goals[j].target_pose.pose.position.z;
        marker.pose.orientation.x = 0.0;
        marker.pose.orientation.y = 0.0;
        marker.pose.orientation.z = 0.0;
        marker.pose.orientation.w = 1.0;
        marker.scale.x = 1;
        marker.scale.y = 0.2;
        marker.scale.z = 0.2;
        marker.color.a = 1.0; // Don't forget to set the alpha!
        marker.color.r = 0.0;
        marker.color.g = 0.0;
        marker.color.b = 1.0;
        if (j == i)
        {
            marker.color.b = 0.0;
            marker.color.g = 1.0;
            marker.color.r = 0.0;
        }
        else if(j > i)
        {
            marker.color.b = 0.0;
            marker.color.g = 0.0;
            marker.color.r = 1.0;

        }
        //only if using a MESH_RESOURCE marker type:
        //marker.mesh_resource = "package://pr2_description/meshes/base_v0/base.dae";
    }
}

int main(int argc, char** argv){

    ros::init(argc, argv, "mod_simple_navigation_goals");

    //tell the action client that we want to spin a thread by default
    MoveBaseClient ac("move_base", true);

    //wait for the action server to come up
    while(!ac.waitForServer(ros::Duration(5.0))){
        ROS_INFO("Waiting for the move_base action server to come up");
    }

    ros::NodeHandle n;
    
    ros::Publisher vis_pub = n.advertise<visualization_msgs::MarkerArray>( "goals_marker", 0 );
    ros::ServiceClient client = n.serviceClient<orobot_srvs::GoalsArray>("goals_array");
    ros::ServiceClient clear_maps_client = n.serviceClient<std_srvs::Empty>("/move_base/clear_costmaps");
    ros::Publisher cmd_vel_pub = n.advertise<geometry_msgs::Twist>("/mobile_base/commands/velocity", 1);

    orobot_srvs::GoalsArray srv;
    std_srvs::Empty empty;
    srv.request.req = true;
    if (client.call(srv))
    {
        std::copy(srv.response.goals.begin(), srv.response.goals.end(),
                  std::ostream_iterator<move_base_msgs::MoveBaseGoal>(std::cout, "\n"));
        marker_array.markers.resize(srv.response.goals.size());
    }
    else
    {
        ROS_ERROR_STREAM("orobot_srvs_client: Failed to call service goals_array");
        return 1;
    }

    /*
    while (ros::ok())
    {
	    std::cout << "Clearing costmaps...";
	    bool ret = clear_maps_client.call(empty);
	    if(!ret)
	    {
	        std::cerr << "I cannot do it!" << std::endl;
	    }
	    else
	    {
	   		std::cout << "Done!" << std::endl;
	   	}

	   	std::cout << "Resetting amcl covariance...";
	   	ros::param::set("/amcl/initial_cov_xx", 0.5*0.5);
	   	ros::param::set("/amcl/initial_cov_yy", 0.5*0.5);
	   	ros::param::set("/amcl/initial_cov_aa", (PI/12.0)*(PI/12.0));	
	   	std::cout << "Done!" << std::endl;
	}
	*/

    int number_of_trials = 1;
    for (unsigned int i = 0; ros::ok() && i < srv.response.goals.size(); i++)
    {
        std::cout << "Sending goal " << i + 1 << " of " << srv.response.goals.size() << std::endl;
        std::cout << "number_of_trials " << number_of_trials << std::endl;

        if (number_of_trials >= RECOVERY_THRESHOLD)
        {
        	if (number_of_trials >= TRIAL_THRESHOLD)
        	{
        		std::cout << "Robot is stuck, unstucking..." << std::endl;

        		geometry_msgs::Twist base_cmd;
			    base_cmd.linear.x = -0.1;
			    base_cmd.linear.y = base_cmd.linear.z = 0.0;
			    base_cmd.angular.x = base_cmd.angular.y = base_cmd.angular.z = 0.0;
			    ros::Duration duration(0.1);
			    for(unsigned int i = 0; i < 30; ++i)
			    {
			      cmd_vel_pub.publish(base_cmd);
			  	  duration.sleep();
			  	}
        	}

        	// clear map (to be sure...)
        	// if it doesn't work please check laser_min_range in amcl.launch.xml

            std::cout << "Clearing costmaps...";
            bool ret = clear_maps_client.call(empty);
            if(!ret)
            {
                std::cerr << "I cannot do it!" << std::endl;
            }
            else
            {
           		std::cout << "Done!" << std::endl;
           	}

           	// reset amcl covariance (it seems useless...)
           	/*
           	std::cout << "Resetting amcl covariance...";
           	ros::param::set("/amcl/initial_cov_xx", 0.5*0.5);
           	ros::param::set("/amcl/initial_cov_yy", 0.5*0.5);
           	ros::param::set("/amcl/initial_cov_aa", (PI/12.0)*(PI/12.0));	
           	std::cout << "Done!" << std::endl;
           	*/
			
        }

        // publish goal and wait

        move_base_msgs::MoveBaseGoal goal = srv.response.goals[i];

        publishMarker(srv.response.goals,i);
        vis_pub.publish(marker_array);
        ros::spinOnce();

        ac.sendGoal(goal);

        ac.waitForResult();

        // feedback to user

        if (ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
        {
            ROS_INFO("Hooray, the base moved forward");
            number_of_trials = 1;
        }
        else
        {
        	ROS_INFO("The base failed to move forward for some reason");

        	if (number_of_trials < MAX_TRIALS)
        	{
	        	ROS_INFO("Retrying");
	        	number_of_trials++;
	            i--; //redo current goal
	        }
	        else
	        {
	        	ROS_INFO("Max number of trials reached. Skipping to next goal");
	        	number_of_trials = 1;
	        }
        }

    }

    return 0;
}