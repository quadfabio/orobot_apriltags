#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <visualization_msgs/MarkerArray.h>
#include <std_srvs/Empty.h>
#include "coverage_planner/CoveragePlanner.h"

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

uint k = 0;
ros::Publisher vis_pub;
visualization_msgs::MarkerArray marker_array;

void publishMarker(const std::vector<move_base_msgs::MoveBaseGoal>& goals, const int i)
{
    for(int j = 0; j < marker_array.markers.size(); ++j)
    {
        visualization_msgs::Marker& marker = marker_array.markers[j];
        marker.header.seq = ++k;
        marker.header.frame_id = "map";
        marker.header.stamp = ros::Time();
        marker.ns = "my_namespace";
        marker.id = j;
        marker.type = visualization_msgs::Marker::SPHERE;
        marker.action = visualization_msgs::Marker::ADD;
        marker.pose.position.x = goals[j].target_pose.pose.position.x;
        marker.pose.position.y = goals[j].target_pose.pose.position.y;
        marker.pose.position.z = goals[j].target_pose.pose.position.z;
        marker.pose.orientation.x = 0.0;
        marker.pose.orientation.y = 0.0;
        marker.pose.orientation.z = 0.0;
        marker.pose.orientation.w = 1.0;
        marker.scale.x = 1;
        marker.scale.y = 0.2;
        marker.scale.z = 0.2;
        marker.color.a = 1.0; // Don't forget to set the alpha!
        marker.color.r = 0.0;
        marker.color.g = 0.0;
        marker.color.b = 1.0;
        if (j == i)
        {
            marker.color.b = 0.0;
            marker.color.g = 1.0;
            marker.color.r = 0.0;
        }
        else if(j > i)
        {
            marker.color.b = 0.0;
            marker.color.g = 0.0;
            marker.color.r = 1.0;

        }
        //only if using a MESH_RESOURCE marker type:
        //marker.mesh_resource = "package://pr2_description/meshes/base_v0/base.dae";
    }
}

int main(int argc, char** argv){

    ros::init(argc, argv, "simple_navigation_goals");

    //tell the action client that we want to spin a thread by default
    MoveBaseClient ac("move_base", true);

    //wait for the action server to come up
    while(!ac.waitForServer(ros::Duration(5.0))){
        ROS_INFO("Waiting for the move_base action server to come up");
    }

    ros::NodeHandle n;
    vis_pub = n.advertise<visualization_msgs::MarkerArray>( "goals_marker", 0 );
    ros::ServiceClient client = n.serviceClient<orobot_srvs::GoalsArray>("goals_array");
    ros::ServiceClient clear_maps_client = n.serviceClient<std_srvs::Empty>("/move_base/clear_costmaps");
    orobot_srvs::GoalsArray srv;
    std_srvs::Empty empty;
    srv.request.req = true;
    if (client.call(srv))
    {
        std::copy(srv.response.goals.begin(), srv.response.goals.end(),
                  std::ostream_iterator<move_base_msgs::MoveBaseGoal>(std::cout, "\n"));
        marker_array.markers.resize(srv.response.goals.size());
    }
    else
    {
        ROS_ERROR_STREAM("orobot_srvs_client: Failed to call service goals_array");
        return 1;
    }

    bool map_cleared = false;
    for (unsigned int i = 0; i < srv.response.goals.size(); i++)
    {
        std::cout << "Sending goal " << i + 1 << " of " << srv.response.goals.size() << std::endl;

        std::cout << "Clearing costmaps..." << std::endl;
        if(!clear_maps_client.call(empty))
        {
            std::cout << "I cannot clear the costmaps!" << std::endl;
        }

        std::cout << "Maps cleared!" << std::endl;

        move_base_msgs::MoveBaseGoal goal = srv.response.goals[i];

        publishMarker(srv.response.goals,i);
        vis_pub.publish(marker_array);
        ros::spinOnce();

        ac.sendGoal(goal);

        ac.waitForResult();

        if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
            ROS_INFO("Hooray, the base moved forward");
        else
        {
            if(!map_cleared)
            {
                //try to clear the costmaps and retry the goal
                map_cleared = true;
                clear_maps_client.call(empty);
                i--; //redo current goal
                continue;
            }
            else
            {
                map_cleared = false;
                //I have already retried to reach the goal but I fail => error!
                ROS_INFO("The base failed to move forward for some reason");
            }
        }
    }

    return 0;
}
