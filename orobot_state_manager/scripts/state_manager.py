#!/usr/bin/env python

from geometry_msgs.msg import Point, Quaternion, Pose, PoseStamped
from move_base_msgs.msg import MoveBaseGoal
from orobot_srvs.srv import *
from orobot_state_manager.srv import *
from simple_navigation_goals.srv import *

import json
import rospy
import sys
import time

#robotId = 'f7bcc568-2b97-422e-9932-a503b084a397'

class StateManager:

    def __init__(self):
        self.robotId = 'f7bcc568-2b97-422e-9932-a503b084a397'
        self.goals_array = []
        self.next_goal = 0
        self.is_navigation_stopped = False

    def set_goals(self, list_of_goals):
        self.goals_array = list_of_goals

        #print self.goals_array

    def send_navigation_stop_request(self):
        rospy.wait_for_service('one_goal_navigation_node/stop_navigation')
        try:
            self.is_navigation_stopped = True
            stop_simple_navigation = rospy.ServiceProxy('one_goal_navigation_node/stop_navigation', StopSimpleNavigation)
            stop_simple_navigation()
            return True
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e
            return False

    def handle_fall_detection_communication_service(self, req):
        print "Sending FALLEN_PERSON message"

        data = {}
        payload = {}

        data['header'] = 'FALLEN_PERSON'
        
        payload['robotId'] = self.robotId
        payload['x'] = req.x
        payload['y'] = req.y
        
        data['payload'] = payload

        json_data = json.dumps(data)
        
        print json_data

        if not(self.is_navigation_stopped):
            print "Sending NAVIGATION STOP request"
            ret = self.send_navigation_stop_request()
            print "Was navigation stopped? " + str(ret)

        return True

    def handle_navigation_communication_service(self, req):
        print "Sending NAVIGATION message"

        if self.is_navigation_stopped:
            time.sleep(10)
            self.is_navigation_stopped = False

        if len(self.goals_array) < self.next_goal + 1:
            print "Nothing to send"
            goals = []
            return goals
       
        print "self.next_goal"
        print self.next_goal

        p = Point()
        p.x = self.goals_array[self.next_goal][0]
        p.y = self.goals_array[self.next_goal][1]
        p.z = self.goals_array[self.next_goal][2]
        
        q = Quaternion()
        q.x = self.goals_array[self.next_goal][3]
        q.y = self.goals_array[self.next_goal][4]
        q.z = self.goals_array[self.next_goal][5]
        q.w = 1

        pose = Pose()
        pose.position = p
        pose.orientation = q

        pose_stamped = PoseStamped()
        pose_stamped.header.seq = 0
        pose_stamped.header.stamp = rospy.Time.now()
        pose_stamped.pose = pose
        pose_stamped.header.frame_id = "map"

        goal = MoveBaseGoal()
        goal.target_pose = pose_stamped

        print "goal"
        print goal

        res = GoalsArrayResponse()
        res.goals.append(goal)

        return res

    def handle_navigation_ack_service(self, req):
        print "ACK received"
        print "next pose is available"

        if not(self.is_navigation_stopped):
            self.next_goal = self.next_goal + 1;

            if len(self.goals_array) <= self.next_goal:
                self.next_goal = 0

        return True

    def handle_person_detection_communication_service(self, req):
        print "Sending DETECTED_PERSON message"

        data = {}
        payload = {}

        data['header'] = 'DETECTED_PERSON'

        payload['robotId'] = self.robotId
        payload['x'] = req.x
        payload['y'] = req.y
        payload['id'] = req.id

        data['payload'] = payload

        json_data = json.dumps(data)

        print json_data
        return True

    def handle_person_recognition_communication_service(self, req):
        print "Sending RECOGNIZED_PERSON message"

        data = {}
        payload = {}

        data['header'] = 'RECOGNIZED_PERSON'

        payload['robotId'] = self.robotId
        payload['name'] = req.name

        data['payload'] = payload

        json_data = json.dumps(data)

        print json_data
        return True
            
def fall_detection_server(state_manager):
    s = rospy.Service('orobot_state_manager/fall_detection_communication_service', 
        FallDetectionCommunicationService,
        state_manager.handle_fall_detection_communication_service)
    print "Ready to send FALLEN_PERSON message."

def navigation_server(state_manager):
    s = rospy.Service('goals_array', 
        GoalsArray,
        state_manager.handle_navigation_communication_service)
    print "Ready to send NAVIGATION GOAL."

def navigation_ack_server(state_manager):
    s = rospy.Service('orobot_state_manager/one_goal_navigation_ack', 
        NavigationAckService,
        state_manager.handle_navigation_ack_service)
    print "Ready to receive NAVIGATION ACK."

def person_detection_server(state_manager):
    s = rospy.Service('orobot_state_manager/person_detection_communication_service', 
        PersonDetectionCommunicationService,
        state_manager.handle_person_detection_communication_service)
    print "Ready to send DETECTED_PERSON message."

def person_recognition_server(state_manager):
    s = rospy.Service('orobot_state_manager/person_recognition_communication_service', 
        PersonRecognitionCommunicationService,
        state_manager.handle_person_recognition_communication_service)
    print "Ready to send RECOGNIZED_PERSON message."

if __name__ == "__main__":
    rospy.init_node('orobot_state_manager')

    s = StateManager()

    fall_detection_server(s)
    person_detection_server(s)
    person_recognition_server(s)

    if (not(rospy.has_param('/orobot_state_manager/rows')) or
        not(rospy.has_param('/orobot_state_manager/cols')) or
        not(rospy.has_param('/orobot_state_manager/positions')) or
        not(rospy.has_param('/orobot_state_manager/position_matrix'))):
        sys.exit(-1)

    rows = rospy.get_param('/orobot_state_manager/rows') # 1
    cols = rospy.get_param('/orobot_state_manager/cols') # 6
    number_of_positions = rospy.get_param('/orobot_state_manager/positions')

    position_matrix = rospy.get_param('/orobot_state_manager/position_matrix')

    if len(position_matrix)/cols != number_of_positions:
        print "Please check positions in config file"
        sys.exit(-1)

    i = 0
    position_list=[]
    while i < len(position_matrix):
      position_list.append(position_matrix[i : i + cols])
      i += cols

    s.set_goals(position_list)

    navigation_server(s)
    navigation_ack_server(s)

    rospy.spin()
