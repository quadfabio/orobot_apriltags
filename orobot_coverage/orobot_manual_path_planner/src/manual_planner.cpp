#include <ros/ros.h>
//#include <actionlib/client/simple_action_client.h>
#include <costmap_2d/costmap_2d_ros.h>
#include <costmap_2d/costmap_2d.h>

#include <geometry_msgs/PolygonStamped.h>
#include <geometry_msgs/PointStamped.h>

#include <frontier_exploration/geometry_tools.h>
#include <frontier_exploration/ExploreTaskAction.h>
#include <frontier_exploration/ExploreTaskActionGoal.h>
#include <frontier_exploration/GetNextFrontier.h>
#include <frontier_exploration/UpdateBoundaryPolygon.h>
#include <costmap_2d/footprint.h>
//#include <tf/transform_listener.h>

#include <ros/wall_timer.h>

#include <move_base_msgs/MoveBaseGoal.h>

#include <visualization_msgs/Marker.h>
#include <boost/foreach.hpp>

#include "orobot_srvs/GoalsArray.h"


namespace manual_planner {

/**
 * @brief ManualPlanner that receives control points from rviz, creates a path and advertise it.
 */
class ManualPlanner{

private:

    ros::NodeHandle nh_;
    ros::NodeHandle private_nh_;

    ros::Subscriber point_;
    ros::Publisher point_viz_pub_;
    ros::WallTimer point_viz_timer_;
    geometry_msgs::PolygonStamped input_;
    ros::ServiceServer path_advertiser_;

    bool waiting_for_end_;

    /**
     * @brief Service for advertising the path.
     */
    bool advertise_path(orobot_srvs::GoalsArray::Request &req,
        orobot_srvs::GoalsArray::Response &res)
    {
        std::vector<move_base_msgs::MoveBaseGoal> goals;
        goals.reserve(input_.polygon.points.size());
        for(uint i = 0; i < input_.polygon.points.size(); ++i)
        {
            move_base_msgs::MoveBaseGoal mbg;
            geometry_msgs::PoseStamped pose_stamped;
            pose_stamped.header.seq = i;
            pose_stamped.header.stamp = ros::Time::now();
            geometry_msgs::Pose pose;
            geometry_msgs::Point p;
            p.x = input_.polygon.points[i].x;
            p.y = input_.polygon.points[i].y; p.z = 0;
            geometry_msgs::Quaternion q;
            q.x = q.y = q.z = 0;
            q.w = 1;
            pose.position = p;
            pose.orientation = q;
            pose_stamped.pose = pose;
            pose_stamped.header.frame_id = "map";
            mbg.target_pose = pose_stamped;
            goals.push_back(mbg);
        }
        res.goals = goals;
        return true;
    }

    /**
     * @brief Publish markers for visualization of points for path.
     */
    void vizPubCb()
    {
        visualization_msgs::Marker points, line_strip;

        points.header = line_strip.header = input_.header;
        points.ns = line_strip.ns = "explore_points";

        points.id = 0;
        line_strip.id = 1;

        points.type = visualization_msgs::Marker::SPHERE_LIST;
        line_strip.type = visualization_msgs::Marker::LINE_STRIP;

        if(!input_.polygon.points.empty()){

            points.action = line_strip.action = visualization_msgs::Marker::ADD;
            points.pose.orientation.w = line_strip.pose.orientation.w = 1.0;

            points.scale.x = points.scale.y = 0.1;
            line_strip.scale.x = 0.05;

//            points.points.push_back(costmap_2d::toPoint(input_.polygon.points.front()));

            BOOST_FOREACH(geometry_msgs::Point32 point, input_.polygon.points){
                line_strip.points.push_back(costmap_2d::toPoint(point));
                points.points.push_back(costmap_2d::toPoint(point));
            }

            if(waiting_for_end_){
                //line_strip.points.push_back(costmap_2d::toPoint(input_.polygon.points.front()));
                points.color.a = points.color.r = line_strip.color.r = line_strip.color.a = 1.0;
            }else{
                points.color.a = points.color.b = line_strip.color.b = line_strip.color.a = 1.0;
            }
        }else{
            points.action = line_strip.action = visualization_msgs::Marker::DELETE;
        }
        point_viz_pub_.publish(points);
        point_viz_pub_.publish(line_strip);

    }

    /**
     * @brief Build path from points received through rviz gui.
     * @param point Received point from rviz
     */
    void pointCb(const geometry_msgs::PointStampedConstPtr& point){

        double average_distance = frontier_exploration::polygonPerimeter(input_.polygon) / input_.polygon.points.size();

        //std::cout << "input_.polygon.points.back() " << input_.polygon.points.back() << std::endl;

        if(waiting_for_end_)
        {
            ROS_INFO("Done, restarting path selection...");

            waiting_for_end_ = false;
            input_.polygon.points.clear();

        }
        else if(input_.polygon.points.empty())
        {
            //first control point, so initialize header of boundary polygon

            input_.header = point->header;
            input_.polygon.points.push_back(costmap_2d::toPoint32(point->point));

        }
        else if(input_.header.frame_id != point->header.frame_id)
        {
            ROS_ERROR("Frame mismatch, restarting path selection");
            input_.polygon.points.clear();

        }
        else if(input_.polygon.points.size() > 1 
            && frontier_exploration::pointsNearby(input_.polygon.points.back(), point->point,
                                                                    average_distance*0.1))
        {
            //check if last boundary point, i.e. nearby to last point

            waiting_for_end_ = true;
            ROS_WARN("The manual path is now available");

        }
        else
        {

            //otherwise, must be a regular point inside boundary polygon
            input_.polygon.points.push_back(costmap_2d::toPoint32(point->point));
            input_.header.stamp = ros::Time::now();
        }

    }

public:

    /**
     * @brief Constructor for the planner.
     */
    ManualPlanner() :
        nh_(),
        private_nh_("~"),
        waiting_for_end_(false)
    {
        input_.header.frame_id = "map";
        point_ = nh_.subscribe("/clicked_point",10,&ManualPlanner::pointCb, this);
        point_viz_pub_ = nh_.advertise<visualization_msgs::Marker>("manual_path_marker", 10);
        point_viz_timer_ = nh_.createWallTimer(ros::WallDuration(0.1), boost::bind(&ManualPlanner::vizPubCb, this));
            // what's the adv of a wallTimer?
        path_advertiser_ = nh_.advertiseService("goals_array", &ManualPlanner::advertise_path, this);
        ROS_INFO("Please use the 'Point' tool in Rviz to select a manual path.");
    }    

};

}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "manual_planner");

    manual_planner::ManualPlanner mp;
    ros::spin();
    return 0;
}
