
#include "coverage_planner/CoveragePlanner.h"
#include "ros/ros.h"

using namespace orobot;

int main(int argc, char** argv)
{
    ros::init(argc, argv, "coverage_planner");

    CoveragePlanner cp;
    cp.plan();
    //cp.publishPlan();

    ros::shutdown();
    return 0;
}
