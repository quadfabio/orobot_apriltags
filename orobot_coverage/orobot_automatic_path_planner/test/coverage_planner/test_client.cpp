/*
 *  Copyright (c) 2015-, Marco Carraro <carraromarco89@gmail.com>,
 *                       Morris Antonello <morris.antonello@dei.unipd.it>
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     1. Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *     2. Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *     3. Neither the name of the copyright holder(s) nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <move_base_msgs/MoveBaseGoal.h>
#include <visualization_msgs/MarkerArray.h>
#include "coverage_planner/CoveragePlanner.h"
#include "ros/ros.h"

using namespace orobot;

uint k = 0;
visualization_msgs::MarkerArray marker_array;

void publishMarker(const std::vector<move_base_msgs::MoveBaseGoal>& goals, const int i)
{
    for(int j = 0; j < marker_array.markers.size(); ++j)
    {
        visualization_msgs::Marker& marker = marker_array.markers[j];
        marker.header.seq = ++k;
        marker.header.frame_id = "map";
        marker.header.stamp = ros::Time();
        marker.ns = "my_namespace";
        marker.id = j;
        marker.type = visualization_msgs::Marker::SPHERE;
        marker.action = visualization_msgs::Marker::ADD;
        marker.pose.position.x = goals[j].target_pose.pose.position.x;
        marker.pose.position.y = goals[j].target_pose.pose.position.y;
        marker.pose.position.z = goals[j].target_pose.pose.position.z;
        marker.pose.orientation.x = 0.0;
        marker.pose.orientation.y = 0.0;
        marker.pose.orientation.z = 0.0;
        marker.pose.orientation.w = 1.0;
        marker.scale.x = 0.2;
        marker.scale.y = 0.2;
        marker.scale.z = 0.2;
        marker.color.a = 1.0; // Don't forget to set the alpha!
        marker.color.r = 0.0;
        marker.color.g = 0.0;
        marker.color.b = 1.0;
        if (j == i)
        {
            marker.color.b = 0.0;
            marker.color.g = 1.0;
            marker.color.r = 0.0;
        }
        else if(j > i)
        {
            marker.color.b = 0.0;
            marker.color.g = 0.0;
            marker.color.r = 1.0;

        }
        //only if using a MESH_RESOURCE marker type:
        //marker.mesh_resource = "package://pr2_description/meshes/base_v0/base.dae";
    }
}


int main(int argc, char** argv)
{
    ros::init(argc, argv, "coverage_planner_client");

    ros::NodeHandle n;
    ros::Publisher vis_pub = n.advertise<visualization_msgs::MarkerArray>( "goals_marker", 0 );
    ros::ServiceClient client = n.serviceClient<orobot_srvs::GoalsArray>("goals_array");
    orobot_srvs::GoalsArray srv;
    srv.request.req = true;
    srv.response.goals.clear();
    if (client.call(srv))
    {
        std::copy(srv.response.goals.begin(), srv.response.goals.end(),
                  std::ostream_iterator<move_base_msgs::MoveBaseGoal>(std::cout, "\n"));

        marker_array.markers.resize(srv.response.goals.size());

        while(1)
        {
            for(int i = 0; i < srv.response.goals.size(); ++i)
            {
                publishMarker(srv.response.goals, i);
                vis_pub.publish(marker_array);
                ros::spinOnce();
                ros::Rate(0.2).sleep();
            }
        }

    }
    else
    {
        ROS_ERROR_STREAM("orobot_srvs_client: Failed to call service goals_array");
        return 1;
    }

    return 0;
}
