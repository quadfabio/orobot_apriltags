/*
 *  Copyright (c) 2015-, Marco Carraro <carraromarco89@gmail.com>,
 *                       Morris Antonello <morris.antonello@dei.unipd.it>
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     1. Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *     2. Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *     3. Neither the name of the copyright holder(s) nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "coverage_planner/CoveragePlanner.h"
namespace orobot
{

void drawArrow(cv::Mat& image, cv::Point& p, cv::Point& q, const cv::Scalar& color, int arrowMagnitude = 9, int thickness=1, int line_type=8, int shift=0)
{
    //Draw the principle line
    cv::line(image, p, q, color, thickness, line_type, shift);
    //compute the angle alpha
    double angle = atan2((double)p.y-q.y, (double)p.x-q.x);
    //compute the coordinates of the first segment
    p.x = (int) ( q.x +  arrowMagnitude * cos(angle + M_PI/4));
    p.y = (int) ( q.y +  arrowMagnitude * sin(angle + M_PI/4));
    //Draw the first segment
    cv::line(image, p, q, color, thickness, line_type, shift);
    //compute the coordinates of the second segmen
    p.x = static_cast<int>( q.x +  arrowMagnitude * std::cos(angle - M_PI/4));
    p.y = static_cast<int> ( q.y +  arrowMagnitude * std::sin(angle - M_PI/4));
    //Draw the second segment
    cv::line(image, p, q, color, thickness, line_type, shift);
}

void CoveragePlanner::plan()
{
    this->divideMapInCells();

    cv::Mat initial_map;
    cv::cvtColor(map_, initial_map, CV_GRAY2BGR);

    cv::imwrite("/home/orobot/map.png", map_);

    for(uint r = 0; r < cell_matrix_.rows(); ++r)
    {
        for(uint c = 0; c < cell_matrix_.cols(); ++c)
        {
            cv::rectangle(initial_map,cell_matrix_(r,c).getP1(), cell_matrix_(r,c).getP2(), cv::Scalar(0,0,200),2);
        }
        //cv::circle(initial_map, plan_[i], cell_dim_ / 4, cv::Scalar(0,0,200),CV_FILLED);
        //cv::circle(initial_map, plan_[i + 1], cell_dim_ / 4, cv::Scalar(0,0,200), CV_FILLED);
        //drawArrow(map_colored, plan_[i], plan_[i + 1], cv::Scalar(200,0,0));
    }

    //cv::imshow("if you like it, close the window", initial_map);
    cv::imwrite("/home/orobot/celled_map.png",initial_map);
    //cv::waitKey(0);

    this->setEmptyCells();

    cv::Mat emptied_map;
    cv::cvtColor(map_, emptied_map, CV_GRAY2BGR);

    for(uint r = 0; r < cell_matrix_.rows(); ++r)
    {
        for(uint c = 0; c < cell_matrix_.cols(); ++c)
        {
            if(cell_matrix_(r,c).isEmpty())
                cv::rectangle(emptied_map,cell_matrix_(r,c).getP1(), cell_matrix_(r,c).getP2(), cv::Scalar(0,0,200),2);
        }
    }

    //cv::imshow("if you like it, close the window", emptied_map);
    cv::imwrite("/home/orobot/emptied_map.png",emptied_map);
    //cv::waitKey(0);

    this->computePlanningGoals();

    cv::Mat map_colored;
    cv::cvtColor(map_, map_colored, CV_GRAY2BGR);

    for(uint i = 0; i < plan_.size() - 1; ++i)
    {
        cv::circle(map_colored, plan_[i], cell_dim_ / 4, cv::Scalar(0,0,200),CV_FILLED);
        cv::circle(map_colored, plan_[i + 1], cell_dim_ / 4, cv::Scalar(0,0,200), CV_FILLED);
        //drawArrow(map_colored, plan_[i], plan_[i + 1], cv::Scalar(200,0,0));
    }

    //cv::imshow("if you like it, close the window", map_colored);
    cv::imwrite("/home/orobot/map_with_plan.png",map_colored);
    //cv::waitKey(0);
}
void CoveragePlanner::plan(actionlib::SimpleActionServer<orobot_state_manager::CoverageAction> &as_)
{
    if(!as_.isPreemptRequested())
        this->divideMapInCells();
    else return;

//    cv::Mat initial_map;
//    cv::cvtColor(map_, initial_map, CV_GRAY2BGR);

//    for(uint r = 0; r < cell_matrix_.rows(); ++r)
//    {
//        for(uint c = 0; c < cell_matrix_.cols(); ++c)
//        {
//            cv::rectangle(initial_map,cell_matrix_(r,c).getP1(), cell_matrix_(r,c).getP2(), cv::Scalar(0,0,200),2);
//        }
//   }

    if(!as_.isPreemptRequested())
        this->setEmptyCells();
    else
        return;

//    cv::Mat emptied_map;
//    cv::cvtColor(map_, emptied_map, CV_GRAY2BGR);

//    for(uint r = 0; r < cell_matrix_.rows(); ++r)
//    {
//        for(uint c = 0; c < cell_matrix_.cols(); ++c)
//        {
//            if(cell_matrix_(r,c).isEmpty())
//                cv::rectangle(emptied_map,cell_matrix_(r,c).getP1(), cell_matrix_(r,c).getP2(), cv::Scalar(0,0,200),2);
//        }
//    }

    if(!as_.isPreemptRequested())
        this->computePlanningGoals();
    else
        return;

//    cv::Mat map_colored;
//    cv::cvtColor(map_, map_colored, CV_GRAY2BGR);

//    for(uint i = 0; i < plan_.size() - 1; ++i)
//    {
//        cv::circle(map_colored, plan_[i], cell_dim_ / 4, cv::Scalar(0,0,200),CV_FILLED);
//        cv::circle(map_colored, plan_[i + 1], cell_dim_ / 4, cv::Scalar(0,0,200), CV_FILLED);
//        //drawArrow(map_colored, plan_[i], plan_[i + 1], cv::Scalar(200,0,0));
//    }

//    //cv::imshow("if you like it, close the window", map_colored);
//    cv::imwrite("/home/orobot/map_with_plan.png",map_colored);
//    //cv::waitKey(0);
}

void CoveragePlanner::readFromLauncher()
{
    if(!nh_.getParam("image",map_file_))
        ROS_FATAL("missing image parameter, check the map yaml file");
    double tmp;
    if(!nh_.getParam("resolution", tmp))
        ROS_FATAL("Missing resolution parameter, check the map yaml file!");
    meters_by_pixels_ratio_ = static_cast<float>(tmp);

    std::vector<float> origin(3);
    if(!nh_.getParam("origin", origin))
        ROS_FATAL("Missing origin parameter, check the map yaml file!");
    map_origin_.x = origin[0];
    map_origin_.y = origin[1];
    if(!pnh_.getParam("cell_dim",cell_dim_))
        ROS_FATAL("missing cell_dim parameter");
    int initial_point_x, initial_point_y;
    if(!pnh_.getParam("initial_point_x",initial_point_x))
        ROS_FATAL("missing initial_point_x parameter");
    if(!pnh_.getParam("initial_point_y",initial_point_y))
        ROS_FATAL("missing initial_point_y parameter");
    initial_point_.x = initial_point_x;
    initial_point_.y = initial_point_y;

    if(!pnh_.getParam("void_percentage",free_space_perc_))
        ROS_FATAL("missing free space percentage parameter");
    if(free_space_perc_ < 0 || free_space_perc_ > 100)
        ROS_FATAL("void_percentage <0 or >100");
}

//Edited by Gio
// void CoveragePlanner::readFromLauncher()
// {
//     if(!nh_.getParam("image",map_file_))
//         ROS_FATAL("missing image parameter, check the map yaml file");
//     double tmp;
//     if(!nh_.getParam("resolution", tmp))
//         ROS_FATAL("Missing resolution parameter, check the map yaml file!");
//     meters_by_pixels_ratio_ = static_cast<float>(tmp);

//     std::vector<float> origin(3);
//     if(!nh_.getParam("origin", origin))
//         ROS_FATAL("Missing origin parameter, check the map yaml file!");
//     map_origin_.x = origin[0];
//     map_origin_.y = origin[1];
//     if(!pnh_.getParam("~cell_dim",cell_dim_))
//         ROS_FATAL("missing cell_dim parameter");
//     int initial_point_x, initial_point_y;
//     if(!pnh_.getParam("~initial_point_x",initial_point_x))
//         ROS_FATAL("missing initial_point_x parameter");
//     if(!pnh_.getParam("~initial_point_y",initial_point_y))
//         ROS_FATAL("missing initial_point_y parameter");
//     initial_point_.x = initial_point_x;
//     initial_point_.y = initial_point_y;

//     if(!pnh_.getParam("~void_percentage",free_space_perc_))
//         ROS_FATAL("missing free space percentage parameter");
//     if(free_space_perc_ < 0 || free_space_perc_ > 100)
//         ROS_FATAL("void_percentage <0 or >100");
// }
void CoveragePlanner::initializeMap()
{
    map_ = cv::imread(map_file_,CV_LOAD_IMAGE_ANYDEPTH);
    if(initial_point_.x < 0 || initial_point_.x > map_.cols || initial_point_.y < 0 || initial_point_.y > map_.rows)
        ROS_FATAL_STREAM("initial_point indicated "<<initial_point_<<" is out of the map ("<<
                         map_.rows << "," << map_.cols << ")!");
}

void CoveragePlanner::divideMapInCells()
{
    std::cout<<"map cols: "<<map_.cols<<", cell dim: "<<cell_dim_<<std::endl;
    uint cols = std::floor(map_.cols / cell_dim_);
    uint rows = std::floor(map_.rows / cell_dim_);
    uint tot_cells = rows * cols;

    if(cols == 0 || rows == 0)
        ROS_FATAL("Rows or cols are zero! Check the cell_dim parameter and the map passed!");

    cell_matrix_.resize(rows, cols);
    //intializing the cells
    for(size_t i = 0; i < rows; ++i)
    {
        for(size_t j = 0; j < cols; ++j)
        {
            cell_matrix_(i,j) = Cell(cv::Point2i(j * cell_dim_, i * cell_dim_),false,cell_dim_);
        }
    }
}

void CoveragePlanner::getMapCoordsGivenCellCoords(const cv::Point2i& cell_coords, cv::Point2i& map_coords) const
{
    map_coords = cell_matrix_(cell_coords.y, cell_coords.x).getCenter();
}

void CoveragePlanner::getCellCoordsGivenCell(const Cell& cell, cv::Point2i coords) const
{
    coords.x = std::floor(cell.getCenter().x / cell_dim_);
    coords.y = std::floor(cell.getCenter().y / cell_dim_);
}

const Cell& CoveragePlanner::getCellGivenMapCoordinates(const cv::Point2i coord) const
{
    cv::Point2i res;
    this->getCellCoordinatesGivenMapCoordinates(coord,res);
    return cell_matrix_(res.y, res.x);
}
void CoveragePlanner::getCellCoordinatesGivenMapCoordinates(const cv::Point2i coord, cv::Point2i& res) const
{
    int x = std::floor(coord.x / cell_dim_);
    int y = std::floor(coord.y / cell_dim_);
    if(x == cell_matrix_.rows()) --x;
    if(y == cell_matrix_.cols()) --y;
    res.x = x;
    res.y = y;
}


void CoveragePlanner::setEmptyCells()
{
    uint min_free_pixels = cell_dim_ * cell_dim_ * free_space_perc_ / 100.0f;
    for(size_t y = 0; y < cell_matrix_.rows(); ++y)
    {
        for(size_t x = 0; x < cell_matrix_.cols(); ++x)
        {
            if(this->getFreePixelsInCell(cell_matrix_(y,x)) < min_free_pixels)
                continue;
            cell_matrix_(y,x).setIsEmpty(true);
        }
    }
}

void CoveragePlanner::setReachableCells(const Cell& begin, const cv::Point2i coords)
{

}
const uint CoveragePlanner::getFreePixelsInCell(const Cell& cell) const
{
    uint frees = 0;
    for(size_t x = cell.getP1().x; x < cell.getP2().x; ++x)
    {
        for(size_t y = cell.getP1().y; y < cell.getP2().y; ++y)
        {
            if(static_cast<int>(map_.at<uchar>(y,x)) > EMPTY_VALUE)
                ++frees;
        }
    }
    return frees;
}

void CoveragePlanner::computePlanningGoals()
{
    uint row(0);
    while (row < cell_matrix_.rows())
    {
        const Cell* first = this->getFirstEmptyCellInRow(row);
        if(first == NULL)
        {
            ++row; continue;
        }
        //plan_.push_back(actual->getCenter());
        cv::Point2i cell_coords;
        getCellCoordsGivenCell(*first, cell_coords);
        const Cell* last = this->getLastEmptyCellInRow(row++, cell_coords.x);
        if(last == NULL)
        {
            plan_.push_back(first->getCenter());
            continue;
        }
        //filling the array in serpentine
        if(row % 2 == 0)
        {
            plan_.push_back(first->getCenter());
            plan_.push_back(last->getCenter());
        }
        else
        {
            plan_.push_back(last->getCenter());
            plan_.push_back(first->getCenter());
        }
    }
    this->fillMoveBaseGoal();
}

const Cell* CoveragePlanner::getFirstEmptyCellInRow(const uint row) const
{
    for(uint col = 0; col < cell_matrix_.cols(); ++col)
    {
        if(cell_matrix_(row,col).isEmpty())
        {
            return &cell_matrix_(row,col);
        }
    }
    return NULL;
}

const Cell* CoveragePlanner::getLastEmptyCellInRow(const uint row, const uint col) const
{
    for(uint c = cell_matrix_.cols() - 1; c > col; --c)
    {
        if(cell_matrix_(row, c).isEmpty())
            return &cell_matrix_(row,c);
    }
    return NULL;
}

void CoveragePlanner::fillMoveBaseGoal()
{
    std::copy(plan_.begin(), plan_.end(), std::ostream_iterator<cv::Point>(std::cout, "\n"));
    std::cout << "Origin: " << map_origin_ << std::endl;
    goals_.reserve(plan_.size());
    for(uint i = 0; i < plan_.size(); ++i)
    {
        MBGoal mbg;
        geometry_msgs::PoseStamped pose_stamped;
        pose_stamped.header.seq = i;
        pose_stamped.header.stamp = ros::Time::now();
        geometry_msgs::Pose pose;
        geometry_msgs::Point p;
        p.x = (plan_[i].x) * meters_by_pixels_ratio_ + map_origin_.x;
        p.y = (map_.rows - plan_[i].y) * meters_by_pixels_ratio_ + map_origin_.y;
        p.z = 0;
        geometry_msgs::Quaternion q;
        q.x = q.y = q.z = 0;
        q.w = 1;
        pose.position = p;
        pose.orientation = q;
        pose_stamped.pose = pose;
        pose_stamped.header.frame_id = "map";
        mbg.target_pose = pose_stamped;
        goals_.push_back(mbg);
    }
}

bool CoveragePlanner::callback(orobot_srvs::GoalsArray::Request &req,
              orobot_srvs::GoalsArray::Response &res)
{
    res.goals = goals_;
    return true;
}

std::vector<move_base_msgs::MoveBaseGoal> CoveragePlanner::getGoals()
{
    std::vector<MBGoal> goals;
    goals = goals_;
    return goals;
}

}
