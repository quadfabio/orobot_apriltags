#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <orobot_state_manager/CoverageAction.h>
#include "coverage_planner/CoveragePlanner.h"
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <visualization_msgs/MarkerArray.h>
#include <std_srvs/Empty.h>

class CoverageAction
{

//private:

protected:
  std::string action_name_;
  ros::NodeHandle nh_;
  actionlib::SimpleActionServer<orobot_state_manager::CoverageAction> as_;

  orobot_state_manager::CoverageFeedback feedback_;
  orobot_state_manager::CoverageResult result_;
    

public:
  
  // void test_method(actionlib::SimpleActionServer<orobot_state_manager::CoverageAction> &as_)
  // {
  //     int i = 0;
  //     while(i < 10 && !as_.isPreemptRequested())
  //     {
  //         ROS_INFO("test method");
  //         ros::Duration(2.0).sleep();
  //         i++;
  //     }
  // }
  CoverageAction(std::string name) :
    as_(nh_, name, boost::bind(&CoverageAction::executeCB, this, _1), false),
    action_name_(name)
  {
    as_.start();
  }

  ~CoverageAction(void)
  {
  }

void coverage_method(std::vector<move_base_msgs::MoveBaseGoal> &goals)
{
    actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> ac_move("move_base", true);
    
    //wait for the action server to come up
    while(!ac_move.waitForServer(ros::Duration(5.0)))
        {
            ROS_INFO("Waiting for the move_base action server to come up");
        }

    int number_of_trials = 1;
    for (unsigned int i = 0; ros::ok() && i < goals.size(); i++)
    {
        std::cout << "Sending goal " << i + 1 << " of " << goals.size() << std::endl;
        std::cout << "number_of_trials " << number_of_trials << std::endl;

        if (number_of_trials >= 2 /*RECOVERY_THRESHOLD*/)
        {
//        	if (number_of_trials >= TRIAL_THRESHOLD)
//        	{
//        		std::cout << "Robot is stuck, unstucking..." << std::endl;
//
//        		geometry_msgs::Twist base_cmd;
//			    base_cmd.linear.x = -0.1;
//			    base_cmd.linear.y = base_cmd.linear.z = 0.0;
//			    base_cmd.angular.x = base_cmd.angular.y = base_cmd.angular.z = 0.0;
//			    ros::Duration duration(0.1);
//			    for(unsigned int i = 0; i < 30; ++i)
//			    {
//			      cmd_vel_pub.publish(base_cmd);
//			  	  duration.sleep();
//			  	}
//        	}       	
            std::cout << "Clearing costmaps...";
//            bool ret = clear_maps_client.call(empty);
//            if(!ret)
//            {
//                std::cerr << "I cannot do it!" << std::endl;
//            }
//            else
//            {
//           		std::cout << "Done!" << std::endl;
//           	}

           	// reset amcl covariance (it seems useless...)
           	/*
           	std::cout << "Resetting amcl covariance...";
           	ros::param::set("/amcl/initial_cov_xx", 0.5*0.5);
           	ros::param::set("/amcl/initial_cov_yy", 0.5*0.5);
           	ros::param::set("/amcl/initial_cov_aa", (PI/12.0)*(PI/12.0));	
           	std::cout << "Done!" << std::endl;
           	*/
			
        }

        // publish goal and wait

        move_base_msgs::MoveBaseGoal goal = goals[i];

//        publishMarker(srv.response.goals,i);
//        vis_pub.publish(marker_array);
//        ros::spinOnce();

        ac_move.sendGoal(goal);

        ac_move.waitForResult();

        // feedback to user

        if (ac_move.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
        {
            ROS_INFO("Hooray, the base moved forward");
            number_of_trials = 1;
        }
        else
        {
        	ROS_INFO("The base failed to move forward for some reason");

        	if (number_of_trials < 10 /*MAX_TRIALS*/)
        	{
	        	ROS_INFO("Retrying");
	        	number_of_trials++;
	            i--; //redo current goal
	        }
	        else
	        {
	        	ROS_INFO("Max number of trials reached. Skipping to next goal");
	        	number_of_trials = 1;
	        }
        }

    }

//    if (client.call(srv))
//    {
//        std::copy(srv.response.goals.begin(), srv.response.goals.end(),
//                  std::ostream_iterator<move_base_msgs::MoveBaseGoal>(std::cout, "\n"));
//        marker_array.markers.resize(srv.response.goals.size());
//    }
//    else
//    {
//        ROS_ERROR_STREAM("orobot_srvs_client: Failed to call service goals_array");
//        return 1;
//    }
}

  void executeCB(const orobot_state_manager::CoverageGoalConstPtr &goal)
  {
      ROS_INFO("AZIONEEEEEEEEEEEEEEEEEEEEEEE");

    // helper variables
    ros::Rate r(30);
    bool success = true;

    feedback_.status.clear();
    if(goal->planner == 1) //automatic coverage
    {
        ROS_INFO("Executing the automatic coverage");
        orobot::CoveragePlanner cp;

        ROS_INFO("quiiiiiiiiiiiiiiiiiiiiii");
        while(as_.isActive() &&  !as_.isPreemptRequested() && ros::ok())
        {
            cp.plan(as_);
            std::vector<move_base_msgs::MoveBaseGoal> goals;
            goals = cp.getGoals();

            coverage_method(goals);
           // service = nh_.advertiseService("goals_array", &orobot::CoveragePlanner::callback, &cp);
           // ros::spinOnce();
           // r.sleep();
        }

        // check that preempt has not been requested by the client
        if (as_.isPreemptRequested() || !ros::ok())
        {
            ROS_INFO("%s: Preempted", action_name_.c_str());
            // set the action state to preempted
            as_.setPreempted();
            success = false;
        }
        feedback_.status.push_back(1);
        as_.publishFeedback(feedback_);
        r.sleep();

        if(success)
        {
          result_.status = feedback_.status;
          ROS_INFO("%s: Succeeded", action_name_.c_str());
          // set the action state to succeeded
          as_.setSucceeded(result_);
        }
    }
    else if (goal->planner == 2) //manual coverage
    {
        ROS_INFO("Executing the manual coverage");
        //To do
    }
    else
        ROS_ERROR("Planner type unknown");
  }
};


//using namespace orobot;
int main(int argc, char** argv)
{
    ros::init(argc, argv, "coverage"); 
    ROS_INFO("AAAAAAAAAAAA");
    CoverageAction coverage(ros::this_node::getName());
    ROS_INFO("BBBBBBBB");
    ros::spin();

    return 0;
}
