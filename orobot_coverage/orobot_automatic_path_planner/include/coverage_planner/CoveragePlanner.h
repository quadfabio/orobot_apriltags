/*
 *  Copyright (c) 2015-, Marco Carraro <carraromarco89@gmail.com>,
 *                       Morris Antonello <morris.antonello@dei.unipd.it>
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     1. Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *     2. Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *     3. Neither the name of the copyright holder(s) nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#ifndef COVERAGE_PLANNER___H
#define COVERAGE_PLANNER___H


#include <ros/ros.h>
#include <move_base_msgs/MoveBaseGoal.h>
#include <geometry_msgs/PoseStamped.h>
#include "orobot_srvs/GoalsArray.h"

#include <opencv2/opencv.hpp>
#include <map>
#include <eigen3/Eigen/Eigen>

#include <actionlib/server/simple_action_server.h>
#include <orobot_state_manager/CoverageAction.h>

#include "coverage_planner/Cell.h"

namespace orobot
{

class CoveragePlanner
{

    typedef move_base_msgs::MoveBaseGoal MBGoal;

public:
    enum
    {
       EMPTY_VALUE = 250
    };

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    ///
    /// \brief CoveragePlanner: copy constructor
    /// \param obj
    ///
    inline CoveragePlanner(const CoveragePlanner& obj):
        map_(obj.map_),
        pnh_("~"),
        cell_dim_(obj.cell_dim_),
        map_file_(obj.map_file_),
        initial_point_(obj.initial_point_),
        cell_matrix_(obj.cell_matrix_),
        free_space_perc_(obj.free_space_perc_)
    {}
    ///
    /// \brief CoveragePlanner: default constructor
    ///
    inline CoveragePlanner():
        pnh_("~")
    {
            this->readFromLauncher();
            this->initializeMap();
    }

    ///
    /// \brief plan: it begins the coverage planning
    ///
    void plan(actionlib::SimpleActionServer<orobot_state_manager::CoverageAction> &as_);
    void plan();

    ///
    /// \brief publishPlan: it publish the plan constructed
    ///
    void publishPlan();

    void getMapCoordsGivenCellCoords(const cv::Point2i& cell_coords, cv::Point2i& map_coords) const;
    const Cell& getCellGivenMapCoordinates(const cv::Point2i coord) const;
    void getCellCoordinatesGivenMapCoordinates(const cv::Point2i coord, cv::Point2i &res) const;
    void getCellCoordsGivenCell(const Cell& cell, cv::Point2i coords) const;

    //callback for ros service
    bool callback(orobot_srvs::GoalsArray::Request &req,
                  orobot_srvs::GoalsArray::Response &res);

    std::vector<MBGoal> getGoals();
private:
    cv::Mat map_;
    ros::NodeHandle pnh_;
    ros::NodeHandle nh_;
    std::vector<cv::Point> plan_;

    //vector of cells representing the map
    Eigen::Matrix<orobot::Cell, Eigen::Dynamic, Eigen::Dynamic> cell_matrix_;
    std::vector<MBGoal> goals_;

    //parameters from yaml
    std::string map_file_;
    int cell_dim_;
    cv::Point2i initial_point_;
    int free_space_perc_;
    float meters_by_pixels_ratio_;
    geometry_msgs::Point map_origin_;

    void readFromLauncher();
    void initializeMap();
    void divideMapInCells();

    void setEmptyCells();
    void setReachableCells(const Cell& begin, const cv::Point2i coords);
    const uint getFreePixelsInCell(const Cell &cell) const;
    void computePlanningGoals();
    const Cell* getFirstEmptyCellInRow(const uint row) const;
    const Cell* getLastEmptyCellInRow(const uint row, const uint col) const;
    void fillMoveBaseGoal();
};

}


#endif //COVERAGE_PLANNER___H
