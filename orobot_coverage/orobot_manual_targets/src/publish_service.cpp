#include <ros/ros.h>
#include "orobot_srvs/GoalsArray.h"

ros::ServiceServer path_advertiser;

bool advertise(orobot_srvs::GoalsArray::Request &req,
               orobot_srvs::GoalsArray::Response &res)
{
    std::vector<move_base_msgs::MoveBaseGoal> goals;
    goals.reserve(11);

    move_base_msgs::MoveBaseGoal mbg;
    geometry_msgs::PoseStamped pose_stamped;
    geometry_msgs::Quaternion q;
    q.x = q.y = q.z = 0;
    q.w = 1;
    pose_stamped.header.seq = 0;
    pose_stamped.header.stamp = ros::Time::now();
    geometry_msgs::Pose pose;
    geometry_msgs::Point p;


    p.x = -2.5712;
    p.y = 0.943452;
    p.z = 0;
    pose.position = p;
    pose.orientation = q;
    pose_stamped.pose = pose;
    pose_stamped.header.frame_id = "map";
    mbg.target_pose = pose_stamped;
    goals.push_back(mbg);

    p.x = -3.23597;
    p.y = -3.62966;
    p.z = 0;
    pose.position = p;
    pose.orientation = q;
    pose_stamped.pose = pose;
    pose_stamped.header.frame_id = "map";
    mbg.target_pose = pose_stamped;
    goals.push_back(mbg);

    p.x = -0.0244322;
    p.y = -3.72084;
    p.z = 0;
    pose.position = p;
    pose.orientation = q;
    pose_stamped.pose = pose;
    pose_stamped.header.frame_id = "map";
    mbg.target_pose = pose_stamped;
    goals.push_back(mbg);

    p.x = -2.19559;
    p.y = -3.81378;
    p.z = 0;
    pose.position = p;
    pose.orientation = q;
    pose_stamped.pose = pose;
    pose_stamped.header.frame_id = "map";
    mbg.target_pose = pose_stamped;
    goals.push_back(mbg);

    p.x = -1.70273;
    p.y = -6.0; //86944;
    p.z = 0;
    pose.position = p;
    pose.orientation = q;
    pose_stamped.pose = pose;
    pose_stamped.header.frame_id = "map";
    mbg.target_pose = pose_stamped;
    goals.push_back(mbg);

//    p.x = -1.7299;
//    p.y = -6.89996;
//    p.z = 0;
//    pose.position = p;
//    pose.orientation = q;
//    pose_stamped.pose = pose;
//    pose_stamped.header.frame_id = "map";
//    mbg.target_pose = pose_stamped;
//    goals.push_back(mbg);

//    p.x = -0.0802526;
//    p.y = -6.60486;
//    p.z = 0;
//    pose.position = p;
//    pose.orientation = q;
//    pose_stamped.pose = pose;
//    pose_stamped.header.frame_id = "map";
//    mbg.target_pose = pose_stamped;
//    goals.push_back(mbg);

//    p.x = -1.7299;
//    p.y = -6.89996;
//    p.z = 0;
//    pose.position = p;
//    pose.orientation = q;
//    pose_stamped.pose = pose;
//    pose_stamped.header.frame_id = "map";
//    mbg.target_pose = pose_stamped;
//    goals.push_back(mbg);

    p.x = -2.28054;
    p.y = -3.71972;
    p.z = 0;
    pose.position = p;
    pose.orientation = q;
    pose_stamped.pose = pose;
    pose_stamped.header.frame_id = "map";
    mbg.target_pose = pose_stamped;
    goals.push_back(mbg);
    p.x = -3.29436;
    p.y = 0.271386;
    p.z = 0;
    pose.position = p;
    pose.orientation = q;
    pose_stamped.pose = pose;
    pose_stamped.header.frame_id = "map";
    mbg.target_pose = pose_stamped;
    goals.push_back(mbg);
    p.x = 0;
    p.y = 0;
    p.z = 0;
    pose.position = p;
    pose.orientation = q;
    pose_stamped.pose = pose;
    pose_stamped.header.frame_id = "map";
    mbg.target_pose = pose_stamped;
    goals.push_back(mbg);
    res.goals = goals;

    // CAD


//    p.x = 7.10759;
//    p.y = 1.14362;
//    p.z = 0;
//    pose.position = p;
//    pose.orientation = q;
//    pose_stamped.pose = pose;
//    pose_stamped.header.frame_id = "map";
//    mbg.target_pose = pose_stamped;
//    goals.push_back(mbg);

////    p.x = 6.82008;
////    p.y = -3.36493;
////    p.z = 0;
////    pose.position = p;
////    pose.orientation = q;
////    pose_stamped.pose = pose;
////    pose_stamped.header.frame_id = "map";
////    mbg.target_pose = pose_stamped;
////    goals.push_back(mbg);

////    p.x = 7.18524;
////    p.y = 1.22416;
////    p.z = 0;
////    pose.position = p;
////    pose.orientation = q;
////    pose_stamped.pose = pose;
////    pose_stamped.header.frame_id = "map";
////    mbg.target_pose = pose_stamped;
////    goals.push_back(mbg);

//    p.x = 10.8498;
//    p.y = 1.20849;
//    p.z = 0;
//    pose.position = p;
//    pose.orientation = q;
//    pose_stamped.pose = pose;
//    pose_stamped.header.frame_id = "map";
//    mbg.target_pose = pose_stamped;
//    goals.push_back(mbg);

//    p.x = 10.5171;
//    p.y = -2.82056;
//    p.z = 0;
//    pose.position = p;
//    pose.orientation = q;
//    pose_stamped.pose = pose;
//    pose_stamped.header.frame_id = "map";
//    mbg.target_pose = pose_stamped;
//    goals.push_back(mbg);

//    p.x = 10.8469;
//    p.y = 1.19454;
//    p.z = 0;
//    pose.position = p;
//    pose.orientation = q;
//    pose_stamped.pose = pose;
//    pose_stamped.header.frame_id = "map";
//    mbg.target_pose = pose_stamped;
//    goals.push_back(mbg);

//    p.x = 12.5925;
//    p.y = 3.52962;
//    p.z = 0;
//    pose.position = p;
//    pose.orientation = q;
//    pose_stamped.pose = pose;
//    pose_stamped.header.frame_id = "map";
//    mbg.target_pose = pose_stamped;
//    goals.push_back(mbg);

//    p.x = 12.163;
//    p.y = 1.19899;
//    p.z = 0;
//    pose.position = p;
//    pose.orientation = q;
//    pose_stamped.pose = pose;
//    pose_stamped.header.frame_id = "map";
//    mbg.target_pose = pose_stamped;
//    goals.push_back(mbg);

//    p.x = 13.8113;
//    p.y = 1.07997;
//    p.z = 0;
//    pose.position = p;
//    pose.orientation = q;
//    pose_stamped.pose = pose;
//    pose_stamped.header.frame_id = "map";
//    mbg.target_pose = pose_stamped;
//    goals.push_back(mbg);

//    p.x = 14.5021;
//    p.y = -2.42352;
//    p.z = 0;
//    pose.position = p;
//    pose.orientation = q;
//    pose_stamped.pose = pose;
//    pose_stamped.header.frame_id = "map";
//    mbg.target_pose = pose_stamped;
//    goals.push_back(mbg);

//    p.x = 13.6776;
//    p.y = 1.12429;
//    p.z = 0;
//    pose.position = p;
//    pose.orientation = q;
//    pose_stamped.pose = pose;
//    pose_stamped.header.frame_id = "map";
//    mbg.target_pose = pose_stamped;
//    goals.push_back(mbg);

//    p.x = 0;
//    p.y = 0;
//    p.z = 0;
//    pose.position = p;
//    pose.orientation = q;
//    pose_stamped.pose = pose;
//    pose_stamped.header.frame_id = "map";
//    mbg.target_pose = pose_stamped;
//    goals.push_back(mbg);
//    res.goals = goals;


    return true;
}

int main(int argc, char** argv)
{

    ros::init(argc, argv, "publish_service");

    ros::NodeHandle nh;

    path_advertiser = nh.advertiseService("goals_array", &advertise);

    ros::spin();

    ros::shutdown();
    return 0;
}
