#include "coverage_planner/CoveragePlanner.h"
#include "ros/ros.h"

using namespace orobot;

int main(int argc, char** argv)
{
    ros::init(argc, argv, "coverage_planner_client");

    ros::NodeHandle n;
    ros::ServiceClient client = n.serviceClient<orobot_srvs::GoalsArray>("goals_array");
    orobot_srvs::GoalsArray srv;
    srv.request.req = true;
    if (client.call(srv))
    {
        std::copy(srv.response.goals.begin(), srv.response.goals.end(),
                  std::ostream_iterator<move_base_msgs::MoveBaseGoal>(std::cout, "\n"));
    }
    else
    {
        ROS_ERROR_STREAM("orobot_srvs_client: Failed to call service goals_array");
        return 1;
    }

    return 0;
}
