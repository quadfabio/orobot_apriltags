REQUIREMENTS:

*1 - PyAudio (FOr the Microphone)  sudo apt-get install portaudio19-dev && sudo pip install PyAudio
*2 - SpeechRecognition 2.1.3 (Important! Do not install the newer versions, we need Python2 for ROS compatibility)
*3 - Unidecode (sudo pip install unidecode). We need it for converting sì to si (in general from unicode strings to ascii strings).