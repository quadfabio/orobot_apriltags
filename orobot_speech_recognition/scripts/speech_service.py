#!/usr/bin/env python

from orobot_speech_recognition.srv import *
import rospy
import speech_recognition as sr
# remove accents
from unidecode import unidecode

# for suppressing annoying errors on terminal
from ctypes import *
from contextlib import contextmanager
ERROR_HANDLER_FUNC = CFUNCTYPE(None, c_char_p, c_int, c_char_p, c_int, c_char_p)
def py_error_handler(filename, line, function, err, fmt):
		pass
c_error_handler = ERROR_HANDLER_FUNC(py_error_handler)
@contextmanager
def noalsaerr():
		asound = cdll.LoadLibrary('libasound.so')
		asound.snd_lib_error_set_handler(c_error_handler)
		yield
		asound.snd_lib_error_set_handler(None)


# obtain audio from the Microphone
r = sr.Recognizer(key="AIzaSyAcvgKoUOJeLyri7h6fJV-cgOTBNoFeVOQ", language="it-IT")

# def handle_request(req):
# 	with noalsaerr():
# 		with sr.Microphone() as source:
# 			print("Speech recognition listening...")
# 			audio = r.listen(source)
# 			# recognize speech using Google Speech Recognition
# 			try:
# 				# I'm using my Google Speech key, for changing it: https://www.chromium.org/developers/how-tos/api-keys
# 				result = r.recognize_google(audio, key="AIzaSyAcvgKoUOJeLyri7h6fJV-cgOTBNoFeVOQ", language="it-IT")
# 				print ("Understood: " + result)
# 				if "SI" in result.upper():
# 					return SpeechRecognitionResponse(1)          
# 			except sr.UnknownValueError:
# 				print("Google Speech Recognition could not understand audio")
# 				return SpeechRecognitionResponse(-1)
# 			except sr.RequestError as e:
# 				print("Could not request results from Google Speech Recognition service; {0}".format(e))
# 				return SpeechRecognitionResponse(-1)
# 			return SpeechRecognitionResponse(0)

def handle_request(req):
	with sr.Microphone() as source:                # use the default microphone as the audio source
		audio = r.listen(source, timeout=4)                   # listen for the first phrase and extract it into audio data

		try:
			text = r.recognize(audio)
			print("You said " + text)    # recognize speech using Google Speech Recognition
			if "SI" in unidecode(text.upper()):
				return SpeechRecognitionResponse(1)
		except LookupError:                            # speech is unintelligible
			print("Could not understand audio or limit exceeded")
			return SpeechRecognitionResponse(-1)
		return SpeechRecognitionResponse(0)

def speech_recognition_server():
	rospy.init_node('speech_recognition_server')
	s = rospy.Service('speech_recognition', SpeechRecognition, handle_request)
	print ("Ready for speech recognition!")
	rospy.spin()

if __name__ == "__main__":
		speech_recognition_server()