%% create seq file %%

clc
clear all
close all

addpath(genpath(['/media/annotation/piotr_toolbox']));

%% Parameters:
%curdir = pwd;
imm_dir = ['/data/fall_detection_cad1/cad1_rate1_5/rgb'];
fName = 'cad1';
imm_ext = '*.jpg';
width = 1920;
height = 1080;
fps = 30;
quality = 80;
codec = 'jpg';

%% Seq creation:
cd(imm_dir)
images = dir(imm_ext);
num_imm = size(images,1);

info = struct('width',width,'height',height,'fps',fps,'quality',quality,'codec',codec);

sobj = seqIo(fName,'w',info);

h = waitbar(0,'Please wait...');
for i = 1:num_imm
    Imm = imread(images(i).name);
    sobj.addframe(Imm);
    waitbar(i/num_imm,h)
end
sobj.close();  
close(h)

