%% from vbb annotation to ground truth matrix %%

clc
clear all
close all

%addpath(genpath([pwd '/vbbLabeler']));
%addpath(genpath([pwd '/piotr_toolbox_2_60']));
addpath(genpath(['/media/annotation/vbbcode3.2.1']));
addpath(genpath(['/media/annotation/piotr_toolbox']));

% Parameters:
max_frames_occluded = 90;   % frames occluded after which a new ID is initialized
occlusion_handled = 0;      % 1: remove people fully occluded, even if they are inside the image
                            % 0: remove people only when they are outside the image
curdir = pwd;
%filedir = ['/media/fall_detection_lab1/lab1_rate1_5/annotation'];
filedir = ['/data/fall_detection_cad1/cad1_rate1_5/annotation'];
if occlusion_handled
    filename = [filedir '/2011-09-09-17-27-25_occl.vbb'];
else
    filename = [filedir '/cad1_afull.vbb'];
end

cd(filedir)
annot = vbb('vbbLoad',filename);

% Get number of unique objects in annotation
n_tracks_gt = vbb( 'numObj', annot );
% Put gt data in format [id x y w h]
gt_test = zeros(n_tracks_gt,5,(annot.nFrame));
gt_mask = zeros(n_tracks_gt,(annot.nFrame));
occlusions = 0;
id_offset = 0;
for id = 1:n_tracks_gt
    obj = vbb( 'get', annot, id);
    for t = obj.str:obj.end
        if (obj.occl(t-obj.str+1) < 1) % person occluded 
            if (occlusions > max_frames_occluded)
                id_offset = id_offset + 1; 
            end
            gt_test(id+id_offset,:,t) = [id+id_offset round(obj.pos(t-obj.str+1,:))];
            gt_mask(id+id_offset,t) = 1;
            occlusions = 0;
        else
            occlusions = occlusions + 1;
        end
    end
end
n_tracks_gt = n_tracks_gt + id_offset;

% Remove empty lines:
for t = 1:size(gt_test,3)
    gt_test(:,:,t) = [gt_test(gt_mask(:,t) > 0,:,t); zeros(size(gt_test,1)-sum(gt_mask(:,t)),5)];
end

% gt_test = zeros(1,4,(annot.nFrame));
% 
% for frame = 1:(annot.nFrame)
%     [gt,posv,lbls] = vbb( 'frameAnn', annot, frame, 'person');
%     gt_test(1:size(gt,1),:,frame) = round(gt(:,1:4));
% end

%save([filename(1:end-4) '_gt.mat'],'gt_test','n_tracks_gt');

fileID = fopen('/data/fall_detection_cad1/cad1_rate1_5/ground_truth.txt','w');
for i = 0:length(gt_test)
    gt_test(1,1,i+1) = i;
end
%nbytes = fprintf(fileID,'topleftcorner1.x topleftcorner1.y width1 height1\n');
nbytes = fprintf(fileID,'%1d %1d %1d %1d %1d\n',gt_test(1,1:5,:));

cd(curdir)
