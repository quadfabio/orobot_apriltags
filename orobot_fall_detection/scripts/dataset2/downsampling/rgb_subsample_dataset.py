from shutil import copyfile
import glob, os
import re

src_dir = '/home/morris/dataset/fall_detection_dataset/lab1/raw_depth/'
dst_dir = '/home/morris/dataset/fall_detection_dataset/lab1_rate1_5/raw_depth/'

os.chdir(src_dir)
for file in glob.glob("*.png"):
    index = int(''.join(re.findall(r'\d+', file))) # :)
    print(index)
    print(file)
    if (index % 5 == 0):
    	copyfile(src_dir + file, dst_dir + file)
