from shutil import copyfile
import glob, os
import re

src_dir = '/home/morris/dataset/fall_detection_dataset/lab1_rate1_5/cloud/'
dst_dir = '/home/morris/dataset/fall_detection_dataset/lab1_rate1_5/cloud_renamed/'

os.chdir(src_dir)
for file in glob.glob("*.pcd"):
    index = int(''.join(re.findall(r'\d+', file))) # :)
    print(index)
    new_index = index + 5
    new_index_str = '{:05}'.format(new_index)
    print(new_index_str)
    copyfile(src_dir + file, dst_dir + 'cloud_' + new_index_str + '.pcd')
