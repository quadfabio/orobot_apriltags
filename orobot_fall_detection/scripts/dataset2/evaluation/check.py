import numpy as np
import cv2
import sys

def display_image_with_rects(path, index, 
	x11, y11, w1, h1, 
	x21, y21, w2, h2):
	filename = path + '/rgb_' + index.zfill(5) + '.jpg'
	img = cv2.imread(filename, 1)

	cv2.rectangle(img,(x11, y11),(x11 + w1, y11 + h1),(255,0,0),5)
	cv2.rectangle(img,(x21, y21),(x21 + w2, y21 + h2),(0,0,255),5)

	cv2.namedWindow(index, cv2.WINDOW_NORMAL)
	cv2.imshow(index, img)

	cv2.waitKey(0)
	cv2.destroyAllWindows()

def display_image_with_rect(path, index, 
	x11, y11, w1, h1):
	filename = path + '/rgb_' + index.zfill(5) + '.jpg'
	img = cv2.imread(filename, 1)

	cv2.rectangle(img,(x11, y11),(x11 + w1, y11 + h1),(255,0,0),5)

	cv2.namedWindow(index, cv2.WINDOW_NORMAL)
	cv2.imshow(index, img)

	cv2.waitKey(0)
	cv2.destroyAllWindows()	

path = '/media/fall_detection_lab1/lab1_rate1_5'
images = '/media/fall_detection_lab1/lab1_rate1_5/rgb'
check = path + sys.argv[1] #'/fp_check.txt'

res1 = []
res2 = []
for line in open(check, 'r'):
	line_terms = line.split()
	print(line_terms)

	index = str(int(line_terms[0])*5)

	if (len(line_terms) > 5):
		display_image_with_rects(images, index, int(line_terms[1]), int(line_terms[2]), int(line_terms[3]), int(line_terms[4]),
			int(line_terms[6]), int(line_terms[7]), int(line_terms[8]), int(line_terms[9]))
	else:
		display_image_with_rect(images, index, int(line_terms[1]), int(line_terms[2]), int(line_terms[3]), int(line_terms[4]))

	#vis = np.concatenate((img1, img2), axis=1)
	
