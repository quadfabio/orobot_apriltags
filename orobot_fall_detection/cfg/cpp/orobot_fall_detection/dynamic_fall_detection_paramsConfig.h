//#line 2 "/opt/ros/indigo/share/dynamic_reconfigure/templates/ConfigType.h.template"
// *********************************************************
// 
// File autogenerated for the orobot_fall_detection package 
// by the dynamic_reconfigure package.
// Please do not edit.
// 
// ********************************************************/

#ifndef __orobot_fall_detection__DYNAMIC_FALL_DETECTION_PARAMSCONFIG_H__
#define __orobot_fall_detection__DYNAMIC_FALL_DETECTION_PARAMSCONFIG_H__

#include <dynamic_reconfigure/config_tools.h>
#include <limits>
#include <ros/node_handle.h>
#include <dynamic_reconfigure/ConfigDescription.h>
#include <dynamic_reconfigure/ParamDescription.h>
#include <dynamic_reconfigure/Group.h>
#include <dynamic_reconfigure/config_init_mutex.h>
#include <boost/any.hpp>

namespace orobot_fall_detection
{
  class dynamic_fall_detection_paramsConfigStatics;
  
  class dynamic_fall_detection_paramsConfig
  {
  public:
    class AbstractParamDescription : public dynamic_reconfigure::ParamDescription
    {
    public:
      AbstractParamDescription(std::string n, std::string t, uint32_t l, 
          std::string d, std::string e)
      {
        name = n;
        type = t;
        level = l;
        description = d;
        edit_method = e;
      }
      
      virtual void clamp(dynamic_fall_detection_paramsConfig &config, const dynamic_fall_detection_paramsConfig &max, const dynamic_fall_detection_paramsConfig &min) const = 0;
      virtual void calcLevel(uint32_t &level, const dynamic_fall_detection_paramsConfig &config1, const dynamic_fall_detection_paramsConfig &config2) const = 0;
      virtual void fromServer(const ros::NodeHandle &nh, dynamic_fall_detection_paramsConfig &config) const = 0;
      virtual void toServer(const ros::NodeHandle &nh, const dynamic_fall_detection_paramsConfig &config) const = 0;
      virtual bool fromMessage(const dynamic_reconfigure::Config &msg, dynamic_fall_detection_paramsConfig &config) const = 0;
      virtual void toMessage(dynamic_reconfigure::Config &msg, const dynamic_fall_detection_paramsConfig &config) const = 0;
      virtual void getValue(const dynamic_fall_detection_paramsConfig &config, boost::any &val) const = 0;
    };

    typedef boost::shared_ptr<AbstractParamDescription> AbstractParamDescriptionPtr;
    typedef boost::shared_ptr<const AbstractParamDescription> AbstractParamDescriptionConstPtr;
    
    template <class T>
    class ParamDescription : public AbstractParamDescription
    {
    public:
      ParamDescription(std::string name, std::string type, uint32_t level, 
          std::string description, std::string edit_method, T dynamic_fall_detection_paramsConfig::* f) :
        AbstractParamDescription(name, type, level, description, edit_method),
        field(f)
      {}

      T (dynamic_fall_detection_paramsConfig::* field);

      virtual void clamp(dynamic_fall_detection_paramsConfig &config, const dynamic_fall_detection_paramsConfig &max, const dynamic_fall_detection_paramsConfig &min) const
      {
        if (config.*field > max.*field)
          config.*field = max.*field;
        
        if (config.*field < min.*field)
          config.*field = min.*field;
      }

      virtual void calcLevel(uint32_t &comb_level, const dynamic_fall_detection_paramsConfig &config1, const dynamic_fall_detection_paramsConfig &config2) const
      {
        if (config1.*field != config2.*field)
          comb_level |= level;
      }

      virtual void fromServer(const ros::NodeHandle &nh, dynamic_fall_detection_paramsConfig &config) const
      {
        nh.getParam(name, config.*field);
      }

      virtual void toServer(const ros::NodeHandle &nh, const dynamic_fall_detection_paramsConfig &config) const
      {
        nh.setParam(name, config.*field);
      }

      virtual bool fromMessage(const dynamic_reconfigure::Config &msg, dynamic_fall_detection_paramsConfig &config) const
      {
        return dynamic_reconfigure::ConfigTools::getParameter(msg, name, config.*field);
      }

      virtual void toMessage(dynamic_reconfigure::Config &msg, const dynamic_fall_detection_paramsConfig &config) const
      {
        dynamic_reconfigure::ConfigTools::appendParameter(msg, name, config.*field);
      }

      virtual void getValue(const dynamic_fall_detection_paramsConfig &config, boost::any &val) const
      {
        val = config.*field;
      }
    };

    class AbstractGroupDescription : public dynamic_reconfigure::Group
    {
      public:
      AbstractGroupDescription(std::string n, std::string t, int p, int i, bool s)
      {
        name = n;
        type = t;
        parent = p;
        state = s;
        id = i;
      }

      std::vector<AbstractParamDescriptionConstPtr> abstract_parameters;
      bool state;

      virtual void toMessage(dynamic_reconfigure::Config &msg, const boost::any &config) const = 0;
      virtual bool fromMessage(const dynamic_reconfigure::Config &msg, boost::any &config) const =0;
      virtual void updateParams(boost::any &cfg, dynamic_fall_detection_paramsConfig &top) const= 0;
      virtual void setInitialState(boost::any &cfg) const = 0;


      void convertParams()
      {
        for(std::vector<AbstractParamDescriptionConstPtr>::const_iterator i = abstract_parameters.begin(); i != abstract_parameters.end(); ++i)
        {
          parameters.push_back(dynamic_reconfigure::ParamDescription(**i));
        }
      }
    };

    typedef boost::shared_ptr<AbstractGroupDescription> AbstractGroupDescriptionPtr;
    typedef boost::shared_ptr<const AbstractGroupDescription> AbstractGroupDescriptionConstPtr;

    template<class T, class PT>
    class GroupDescription : public AbstractGroupDescription
    {
    public:
      GroupDescription(std::string name, std::string type, int parent, int id, bool s, T PT::* f) : AbstractGroupDescription(name, type, parent, id, s), field(f)
      {
      }

      GroupDescription(const GroupDescription<T, PT>& g): AbstractGroupDescription(g.name, g.type, g.parent, g.id, g.state), field(g.field), groups(g.groups)
      {
        parameters = g.parameters;
        abstract_parameters = g.abstract_parameters;
      }

      virtual bool fromMessage(const dynamic_reconfigure::Config &msg, boost::any &cfg) const
      {
        PT* config = boost::any_cast<PT*>(cfg);
        if(!dynamic_reconfigure::ConfigTools::getGroupState(msg, name, (*config).*field))
          return false;

        for(std::vector<AbstractGroupDescriptionConstPtr>::const_iterator i = groups.begin(); i != groups.end(); ++i)
        {
          boost::any n = &((*config).*field);
          if(!(*i)->fromMessage(msg, n))
            return false;
        }

        return true;
      }

      virtual void setInitialState(boost::any &cfg) const
      {
        PT* config = boost::any_cast<PT*>(cfg);
        T* group = &((*config).*field);
        group->state = state;

        for(std::vector<AbstractGroupDescriptionConstPtr>::const_iterator i = groups.begin(); i != groups.end(); ++i)
        {
          boost::any n = boost::any(&((*config).*field));
          (*i)->setInitialState(n);
        }

      }

      virtual void updateParams(boost::any &cfg, dynamic_fall_detection_paramsConfig &top) const
      {
        PT* config = boost::any_cast<PT*>(cfg);

        T* f = &((*config).*field);
        f->setParams(top, abstract_parameters);

        for(std::vector<AbstractGroupDescriptionConstPtr>::const_iterator i = groups.begin(); i != groups.end(); ++i)
        {
          boost::any n = &((*config).*field);
          (*i)->updateParams(n, top);
        }
      }

      virtual void toMessage(dynamic_reconfigure::Config &msg, const boost::any &cfg) const
      {
        const PT config = boost::any_cast<PT>(cfg);
        dynamic_reconfigure::ConfigTools::appendGroup<T>(msg, name, id, parent, config.*field);

        for(std::vector<AbstractGroupDescriptionConstPtr>::const_iterator i = groups.begin(); i != groups.end(); ++i)
        {
          (*i)->toMessage(msg, config.*field);
        }
      }

      T (PT::* field);
      std::vector<dynamic_fall_detection_paramsConfig::AbstractGroupDescriptionConstPtr> groups;
    };
    
class DEFAULT
{
  public:
    DEFAULT()
    {
      state = true;
      name = "Default";
    }

    void setParams(dynamic_fall_detection_paramsConfig &config, const std::vector<AbstractParamDescriptionConstPtr> params)
    {
      for (std::vector<AbstractParamDescriptionConstPtr>::const_iterator _i = params.begin(); _i != params.end(); ++_i)
      {
        boost::any val;
        (*_i)->getValue(config, val);

        if("enable_visualization"==(*_i)->name){enable_visualization = boost::any_cast<bool>(val);}
        if("enable_audible_alarm"==(*_i)->name){enable_audible_alarm = boost::any_cast<bool>(val);}
        if("leafSize"==(*_i)->name){leafSize = boost::any_cast<double>(val);}
        if("meanK"==(*_i)->name){meanK = boost::any_cast<int>(val);}
        if("stddevMulThresh"==(*_i)->name){stddevMulThresh = boost::any_cast<double>(val);}
        if("k2_height"==(*_i)->name){k2_height = boost::any_cast<double>(val);}
        if("ground_tolerance"==(*_i)->name){ground_tolerance = boost::any_cast<double>(val);}
        if("cluster_tolerance"==(*_i)->name){cluster_tolerance = boost::any_cast<double>(val);}
        if("starting_min_cluster"==(*_i)->name){starting_min_cluster = boost::any_cast<int>(val);}
        if("max_cluster"==(*_i)->name){max_cluster = boost::any_cast<int>(val);}
        if("min_cluster"==(*_i)->name){min_cluster = boost::any_cast<int>(val);}
        if("robot_height"==(*_i)->name){robot_height = boost::any_cast<double>(val);}
        if("max_height"==(*_i)->name){max_height = boost::any_cast<double>(val);}
        if("min_height"==(*_i)->name){min_height = boost::any_cast<double>(val);}
      }
    }

    bool enable_visualization;
bool enable_audible_alarm;
double leafSize;
int meanK;
double stddevMulThresh;
double k2_height;
double ground_tolerance;
double cluster_tolerance;
int starting_min_cluster;
int max_cluster;
int min_cluster;
double robot_height;
double max_height;
double min_height;

    bool state;
    std::string name;

    
}groups;



//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      bool enable_visualization;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      bool enable_audible_alarm;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      double leafSize;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      int meanK;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      double stddevMulThresh;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      double k2_height;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      double ground_tolerance;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      double cluster_tolerance;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      int starting_min_cluster;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      int max_cluster;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      int min_cluster;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      double robot_height;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      double max_height;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      double min_height;
//#line 218 "/opt/ros/indigo/share/dynamic_reconfigure/templates/ConfigType.h.template"

    bool __fromMessage__(dynamic_reconfigure::Config &msg)
    {
      const std::vector<AbstractParamDescriptionConstPtr> &__param_descriptions__ = __getParamDescriptions__();
      const std::vector<AbstractGroupDescriptionConstPtr> &__group_descriptions__ = __getGroupDescriptions__();

      int count = 0;
      for (std::vector<AbstractParamDescriptionConstPtr>::const_iterator i = __param_descriptions__.begin(); i != __param_descriptions__.end(); ++i)
        if ((*i)->fromMessage(msg, *this))
          count++;

      for (std::vector<AbstractGroupDescriptionConstPtr>::const_iterator i = __group_descriptions__.begin(); i != __group_descriptions__.end(); i ++)
      {
        if ((*i)->id == 0)
        {
          boost::any n = boost::any(this);
          (*i)->updateParams(n, *this);
          (*i)->fromMessage(msg, n);
        }
      }

      if (count != dynamic_reconfigure::ConfigTools::size(msg))
      {
        ROS_ERROR("dynamic_fall_detection_paramsConfig::__fromMessage__ called with an unexpected parameter.");
        ROS_ERROR("Booleans:");
        for (unsigned int i = 0; i < msg.bools.size(); i++)
          ROS_ERROR("  %s", msg.bools[i].name.c_str());
        ROS_ERROR("Integers:");
        for (unsigned int i = 0; i < msg.ints.size(); i++)
          ROS_ERROR("  %s", msg.ints[i].name.c_str());
        ROS_ERROR("Doubles:");
        for (unsigned int i = 0; i < msg.doubles.size(); i++)
          ROS_ERROR("  %s", msg.doubles[i].name.c_str());
        ROS_ERROR("Strings:");
        for (unsigned int i = 0; i < msg.strs.size(); i++)
          ROS_ERROR("  %s", msg.strs[i].name.c_str());
        // @todo Check that there are no duplicates. Make this error more
        // explicit.
        return false;
      }
      return true;
    }

    // This version of __toMessage__ is used during initialization of
    // statics when __getParamDescriptions__ can't be called yet.
    void __toMessage__(dynamic_reconfigure::Config &msg, const std::vector<AbstractParamDescriptionConstPtr> &__param_descriptions__, const std::vector<AbstractGroupDescriptionConstPtr> &__group_descriptions__) const
    {
      dynamic_reconfigure::ConfigTools::clear(msg);
      for (std::vector<AbstractParamDescriptionConstPtr>::const_iterator i = __param_descriptions__.begin(); i != __param_descriptions__.end(); ++i)
        (*i)->toMessage(msg, *this);

      for (std::vector<AbstractGroupDescriptionConstPtr>::const_iterator i = __group_descriptions__.begin(); i != __group_descriptions__.end(); ++i)
      {
        if((*i)->id == 0)
        {
          (*i)->toMessage(msg, *this);
        }
      }
    }
    
    void __toMessage__(dynamic_reconfigure::Config &msg) const
    {
      const std::vector<AbstractParamDescriptionConstPtr> &__param_descriptions__ = __getParamDescriptions__();
      const std::vector<AbstractGroupDescriptionConstPtr> &__group_descriptions__ = __getGroupDescriptions__();
      __toMessage__(msg, __param_descriptions__, __group_descriptions__);
    }
    
    void __toServer__(const ros::NodeHandle &nh) const
    {
      const std::vector<AbstractParamDescriptionConstPtr> &__param_descriptions__ = __getParamDescriptions__();
      for (std::vector<AbstractParamDescriptionConstPtr>::const_iterator i = __param_descriptions__.begin(); i != __param_descriptions__.end(); ++i)
        (*i)->toServer(nh, *this);
    }

    void __fromServer__(const ros::NodeHandle &nh)
    {
      static bool setup=false;

      const std::vector<AbstractParamDescriptionConstPtr> &__param_descriptions__ = __getParamDescriptions__();
      for (std::vector<AbstractParamDescriptionConstPtr>::const_iterator i = __param_descriptions__.begin(); i != __param_descriptions__.end(); ++i)
        (*i)->fromServer(nh, *this);

      const std::vector<AbstractGroupDescriptionConstPtr> &__group_descriptions__ = __getGroupDescriptions__();
      for (std::vector<AbstractGroupDescriptionConstPtr>::const_iterator i = __group_descriptions__.begin(); i != __group_descriptions__.end(); i++){
        if (!setup && (*i)->id == 0) {
          setup = true;
          boost::any n = boost::any(this);
          (*i)->setInitialState(n);
        }
      }
    }

    void __clamp__()
    {
      const std::vector<AbstractParamDescriptionConstPtr> &__param_descriptions__ = __getParamDescriptions__();
      const dynamic_fall_detection_paramsConfig &__max__ = __getMax__();
      const dynamic_fall_detection_paramsConfig &__min__ = __getMin__();
      for (std::vector<AbstractParamDescriptionConstPtr>::const_iterator i = __param_descriptions__.begin(); i != __param_descriptions__.end(); ++i)
        (*i)->clamp(*this, __max__, __min__);
    }

    uint32_t __level__(const dynamic_fall_detection_paramsConfig &config) const
    {
      const std::vector<AbstractParamDescriptionConstPtr> &__param_descriptions__ = __getParamDescriptions__();
      uint32_t level = 0;
      for (std::vector<AbstractParamDescriptionConstPtr>::const_iterator i = __param_descriptions__.begin(); i != __param_descriptions__.end(); ++i)
        (*i)->calcLevel(level, config, *this);
      return level;
    }
    
    static const dynamic_reconfigure::ConfigDescription &__getDescriptionMessage__();
    static const dynamic_fall_detection_paramsConfig &__getDefault__();
    static const dynamic_fall_detection_paramsConfig &__getMax__();
    static const dynamic_fall_detection_paramsConfig &__getMin__();
    static const std::vector<AbstractParamDescriptionConstPtr> &__getParamDescriptions__();
    static const std::vector<AbstractGroupDescriptionConstPtr> &__getGroupDescriptions__();
    
  private:
    static const dynamic_fall_detection_paramsConfigStatics *__get_statics__();
  };
  
  template <> // Max and min are ignored for strings.
  inline void dynamic_fall_detection_paramsConfig::ParamDescription<std::string>::clamp(dynamic_fall_detection_paramsConfig &config, const dynamic_fall_detection_paramsConfig &max, const dynamic_fall_detection_paramsConfig &min) const
  {
    return;
  }

  class dynamic_fall_detection_paramsConfigStatics
  {
    friend class dynamic_fall_detection_paramsConfig;
    
    dynamic_fall_detection_paramsConfigStatics()
    {
dynamic_fall_detection_paramsConfig::GroupDescription<dynamic_fall_detection_paramsConfig::DEFAULT, dynamic_fall_detection_paramsConfig> Default("Default", "", 0, 0, true, &dynamic_fall_detection_paramsConfig::groups);
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __min__.enable_visualization = 0;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __max__.enable_visualization = 1;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __default__.enable_visualization = 1;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      Default.abstract_parameters.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<bool>("enable_visualization", "bool", 0, "Enable 2D/3D visualization?", "", &dynamic_fall_detection_paramsConfig::enable_visualization)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __param_descriptions__.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<bool>("enable_visualization", "bool", 0, "Enable 2D/3D visualization?", "", &dynamic_fall_detection_paramsConfig::enable_visualization)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __min__.enable_audible_alarm = 0;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __max__.enable_audible_alarm = 1;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __default__.enable_audible_alarm = 0;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      Default.abstract_parameters.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<bool>("enable_audible_alarm", "bool", 0, "Enable audible alarm?", "", &dynamic_fall_detection_paramsConfig::enable_audible_alarm)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __param_descriptions__.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<bool>("enable_audible_alarm", "bool", 0, "Enable audible alarm?", "", &dynamic_fall_detection_paramsConfig::enable_audible_alarm)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __min__.leafSize = 0.0;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __max__.leafSize = 1.0;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __default__.leafSize = 0.06;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      Default.abstract_parameters.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<double>("leafSize", "double", 0, "VoxelGrid leafSize", "", &dynamic_fall_detection_paramsConfig::leafSize)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __param_descriptions__.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<double>("leafSize", "double", 0, "VoxelGrid leafSize", "", &dynamic_fall_detection_paramsConfig::leafSize)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __min__.meanK = 0;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __max__.meanK = 1000;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __default__.meanK = 50;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      Default.abstract_parameters.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<int>("meanK", "int", 0, "StatisticalOutlierRemoval meanK", "", &dynamic_fall_detection_paramsConfig::meanK)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __param_descriptions__.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<int>("meanK", "int", 0, "StatisticalOutlierRemoval meanK", "", &dynamic_fall_detection_paramsConfig::meanK)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __min__.stddevMulThresh = 0.0;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __max__.stddevMulThresh = 10.0;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __default__.stddevMulThresh = 0.3;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      Default.abstract_parameters.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<double>("stddevMulThresh", "double", 0, "StatisticalOutlierRemoval stddevMulThresh", "", &dynamic_fall_detection_paramsConfig::stddevMulThresh)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __param_descriptions__.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<double>("stddevMulThresh", "double", 0, "StatisticalOutlierRemoval stddevMulThresh", "", &dynamic_fall_detection_paramsConfig::stddevMulThresh)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __min__.k2_height = 0.0;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __max__.k2_height = 3.0;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __default__.k2_height = 1.161;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      Default.abstract_parameters.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<double>("k2_height", "double", 0, "GroundRemoval k2_height", "", &dynamic_fall_detection_paramsConfig::k2_height)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __param_descriptions__.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<double>("k2_height", "double", 0, "GroundRemoval k2_height", "", &dynamic_fall_detection_paramsConfig::k2_height)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __min__.ground_tolerance = 0.0;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __max__.ground_tolerance = 1.0;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __default__.ground_tolerance = 0.25;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      Default.abstract_parameters.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<double>("ground_tolerance", "double", 0, "GroundRemoval ground_tolerance (0.2)", "", &dynamic_fall_detection_paramsConfig::ground_tolerance)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __param_descriptions__.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<double>("ground_tolerance", "double", 0, "GroundRemoval ground_tolerance (0.2)", "", &dynamic_fall_detection_paramsConfig::ground_tolerance)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __min__.cluster_tolerance = 0.0;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __max__.cluster_tolerance = 1.0;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __default__.cluster_tolerance = 0.1;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      Default.abstract_parameters.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<double>("cluster_tolerance", "double", 0, "EuclideanClusterExtraction cluster_tolerance 2.0 * leafSize  (0.12)", "", &dynamic_fall_detection_paramsConfig::cluster_tolerance)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __param_descriptions__.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<double>("cluster_tolerance", "double", 0, "EuclideanClusterExtraction cluster_tolerance 2.0 * leafSize  (0.12)", "", &dynamic_fall_detection_paramsConfig::cluster_tolerance)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __min__.starting_min_cluster = 0;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __max__.starting_min_cluster = 100;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __default__.starting_min_cluster = 50;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      Default.abstract_parameters.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<int>("starting_min_cluster", "int", 0, "EuclideanClusterExtraction starting_min_cluster  (10)", "", &dynamic_fall_detection_paramsConfig::starting_min_cluster)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __param_descriptions__.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<int>("starting_min_cluster", "int", 0, "EuclideanClusterExtraction starting_min_cluster  (10)", "", &dynamic_fall_detection_paramsConfig::starting_min_cluster)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __min__.max_cluster = 0;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __max__.max_cluster = 1000000;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __default__.max_cluster = 500;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      Default.abstract_parameters.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<int>("max_cluster", "int", 0, "EuclideanClusterExtraction max_cluster", "", &dynamic_fall_detection_paramsConfig::max_cluster)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __param_descriptions__.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<int>("max_cluster", "int", 0, "EuclideanClusterExtraction max_cluster", "", &dynamic_fall_detection_paramsConfig::max_cluster)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __min__.min_cluster = 0;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __max__.min_cluster = 500;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __default__.min_cluster = 125;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      Default.abstract_parameters.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<int>("min_cluster", "int", 0, "GeometricCheck min_cluster (220)", "", &dynamic_fall_detection_paramsConfig::min_cluster)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __param_descriptions__.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<int>("min_cluster", "int", 0, "GeometricCheck min_cluster (220)", "", &dynamic_fall_detection_paramsConfig::min_cluster)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __min__.robot_height = 0.0;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __max__.robot_height = 2.0;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __default__.robot_height = 0.754;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      Default.abstract_parameters.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<double>("robot_height", "double", 0, "GeometricCheck robot_height", "", &dynamic_fall_detection_paramsConfig::robot_height)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __param_descriptions__.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<double>("robot_height", "double", 0, "GeometricCheck robot_height", "", &dynamic_fall_detection_paramsConfig::robot_height)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __min__.max_height = 0.0;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __max__.max_height = 2.0;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __default__.max_height = 0.4;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      Default.abstract_parameters.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<double>("max_height", "double", 0, "GeometricCheck max_height", "", &dynamic_fall_detection_paramsConfig::max_height)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __param_descriptions__.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<double>("max_height", "double", 0, "GeometricCheck max_height", "", &dynamic_fall_detection_paramsConfig::max_height)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __min__.min_height = 0.0;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __max__.min_height = 2.0;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __default__.min_height = 0.9;
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      Default.abstract_parameters.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<double>("min_height", "double", 0, "GeometricCheck min_height", "", &dynamic_fall_detection_paramsConfig::min_height)));
//#line 259 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __param_descriptions__.push_back(dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::ParamDescription<double>("min_height", "double", 0, "GeometricCheck min_height", "", &dynamic_fall_detection_paramsConfig::min_height)));
//#line 233 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      Default.convertParams();
//#line 233 "/opt/ros/indigo/lib/python2.7/dist-packages/dynamic_reconfigure/parameter_generator.py"
      __group_descriptions__.push_back(dynamic_fall_detection_paramsConfig::AbstractGroupDescriptionConstPtr(new dynamic_fall_detection_paramsConfig::GroupDescription<dynamic_fall_detection_paramsConfig::DEFAULT, dynamic_fall_detection_paramsConfig>(Default)));
//#line 353 "/opt/ros/indigo/share/dynamic_reconfigure/templates/ConfigType.h.template"

      for (std::vector<dynamic_fall_detection_paramsConfig::AbstractGroupDescriptionConstPtr>::const_iterator i = __group_descriptions__.begin(); i != __group_descriptions__.end(); ++i)
      {
        __description_message__.groups.push_back(**i);
      }
      __max__.__toMessage__(__description_message__.max, __param_descriptions__, __group_descriptions__); 
      __min__.__toMessage__(__description_message__.min, __param_descriptions__, __group_descriptions__); 
      __default__.__toMessage__(__description_message__.dflt, __param_descriptions__, __group_descriptions__); 
    }
    std::vector<dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr> __param_descriptions__;
    std::vector<dynamic_fall_detection_paramsConfig::AbstractGroupDescriptionConstPtr> __group_descriptions__;
    dynamic_fall_detection_paramsConfig __max__;
    dynamic_fall_detection_paramsConfig __min__;
    dynamic_fall_detection_paramsConfig __default__;
    dynamic_reconfigure::ConfigDescription __description_message__;

    static const dynamic_fall_detection_paramsConfigStatics *get_instance()
    {
      // Split this off in a separate function because I know that
      // instance will get initialized the first time get_instance is
      // called, and I am guaranteeing that get_instance gets called at
      // most once.
      static dynamic_fall_detection_paramsConfigStatics instance;
      return &instance;
    }
  };

  inline const dynamic_reconfigure::ConfigDescription &dynamic_fall_detection_paramsConfig::__getDescriptionMessage__() 
  {
    return __get_statics__()->__description_message__;
  }

  inline const dynamic_fall_detection_paramsConfig &dynamic_fall_detection_paramsConfig::__getDefault__()
  {
    return __get_statics__()->__default__;
  }
  
  inline const dynamic_fall_detection_paramsConfig &dynamic_fall_detection_paramsConfig::__getMax__()
  {
    return __get_statics__()->__max__;
  }
  
  inline const dynamic_fall_detection_paramsConfig &dynamic_fall_detection_paramsConfig::__getMin__()
  {
    return __get_statics__()->__min__;
  }
  
  inline const std::vector<dynamic_fall_detection_paramsConfig::AbstractParamDescriptionConstPtr> &dynamic_fall_detection_paramsConfig::__getParamDescriptions__()
  {
    return __get_statics__()->__param_descriptions__;
  }

  inline const std::vector<dynamic_fall_detection_paramsConfig::AbstractGroupDescriptionConstPtr> &dynamic_fall_detection_paramsConfig::__getGroupDescriptions__()
  {
    return __get_statics__()->__group_descriptions__;
  }

  inline const dynamic_fall_detection_paramsConfigStatics *dynamic_fall_detection_paramsConfig::__get_statics__()
  {
    const static dynamic_fall_detection_paramsConfigStatics *statics;
  
    if (statics) // Common case
      return statics;

    boost::mutex::scoped_lock lock(dynamic_reconfigure::__init_mutex__);

    if (statics) // In case we lost a race.
      return statics;

    statics = dynamic_fall_detection_paramsConfigStatics::get_instance();
    
    return statics;
  }


}

#endif // __DYNAMIC_FALL_DETECTION_PARAMSRECONFIGURATOR_H__
