#ifndef __BBOXPRINTER_H_INCLUDED__
#define __BBOXPRINTER_H_INCLUDED__

#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

class BBoxPrinter
{
public:
    //EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    const float sizeX_;
    const float sizeY_;
    const float focal_x_;
    const float focal_y_;
    const float cx_;
    const float cy_;
    cv::Mat frame_;
    cv::Mat mask_;

    BBoxPrinter() :
        sizeX_(1920),
        sizeY_(1080),
        focal_x_(1042.860226),
        focal_y_(1042.141285),
        cx_(961.871871),
        cy_(528.965054)
    {}

    void
    set_frame(const cv::Mat & frame);

    // way1
	int
	getPointUVCoordinates(const pcl::PointXYZ & pt,
	    cv::Point & UV_coordinates);

	// way2
	void
	project3DPointToPixel(const pcl::PointXYZ & pt,
	                      cv::Point & UV_coordinates);

	/* The translation from kinect2_head_ir_optical_frame
	to kinect2_head_rgb_optical_frame is neglected */
	void
	print2DBoundingBox(const pcl::PointXYZ & old_ref_min_p_cluster,
	    const pcl::PointXYZ & old_ref_max_p_cluster,
	    cv::Point & min_pt,
	    cv::Point & max_pt,
	    const bool found,
	    const std::string & label = "");

	void 
    basicPrint2DBoundingBox(pcl::PointCloud<pcl::PointXYZ>::Ptr & cluster_cloud,
	        cv::Point & point1,
	        cv::Point & point2,
	        const bool found,
	        const std::string & label = "");

	void
	getPointWithMinMaxCoords(const pcl::PointCloud<pcl::PointXYZ>::Ptr & cloud,
	        pcl::PointXYZ & pointWithMinX,
	        pcl::PointXYZ & pointWithMinY,
	        pcl::PointXYZ & pointWithMinZ,
	        pcl::PointXYZ & pointWithMaxX,
	        pcl::PointXYZ & pointWithMaxY,
	        pcl::PointXYZ & pointWithMaxZ);

	void advancedPrint2DBoundingBox(pcl::PointCloud<pcl::PointXYZ>::Ptr cluster_cloud,
	        cv::Point & point1,
	        cv::Point & point2,
	        const bool found,
	        const std::string & label = "");

    void
    mask_rgb_frame(const pcl::PointCloud<pcl::PointXYZ>::Ptr & cloud);
};
#endif
