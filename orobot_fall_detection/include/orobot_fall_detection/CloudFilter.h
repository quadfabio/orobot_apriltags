#ifndef __CLOUDFILTER_H_INCLUDED__
#define __CLOUDFILTER_H_INCLUDED__

class CloudFilter
{
public:
    void
    filter_cloud(const pcl::PointCloud<pcl::PointXYZ>::Ptr & input_cloud,
                 const float leafSize,
                 const int meanK,
                 const float stddevMulThresh,
                 pcl::PointCloud<pcl::PointXYZ>::Ptr & output_cloud);

    void
    extract_roi(const pcl::PointCloud<pcl::PointXYZ>::Ptr & input_cloud,
                pcl::PointCloud<pcl::PointXYZ>::Ptr & output_cloud);

    void
    remove_ground(const pcl::PointCloud<pcl::PointXYZ>::Ptr & input_cloud,
                  const float k2_height, // in meters
                  const float ground_tolerance,
                  pcl::PointCloud<pcl::PointXYZ>::Ptr & ground_cloud,
                  pcl::PointCloud<pcl::PointXYZ>::Ptr & no_ground_cloud);

    void
    find_candidates(const pcl::PointCloud<pcl::PointXYZ>::Ptr & input_cloud,
                    const float cluster_tolerance,
                    const int min_cluster,
                    const int max_cluster,
                    std::vector<pcl::PointIndices> & cluster_indices);

    void
    computeOBB(const pcl::PointCloud<pcl::PointXYZ>::Ptr& point_cloud_ptr,
                Eigen::Vector3f& tfinal,
                Eigen::Quaternionf& qfinal,
                pcl::PointXYZ& min_pt,
                pcl::PointXYZ& max_pt,
                pcl::PointXYZ& old_ref_min_pt,
                pcl::PointXYZ& old_ref_max_pt);

    void
    get_cluster_sorted_dims(const pcl::PointCloud<pcl::PointXYZ>::Ptr & cluster_cloud,
                            std::vector<float> & dims,
                            pcl::PointXYZ & old_ref_min_p_cluster,
                            pcl::PointXYZ & old_ref_max_p_cluster);
};
#endif
