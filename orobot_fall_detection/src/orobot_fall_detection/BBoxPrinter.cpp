#include "orobot_fall_detection/BBoxPrinter.h"

#include <opencv2/highgui/highgui.hpp>

#include <pcl/common/common.h>
#include <pcl/common/pca.h>

void
BBoxPrinter::set_frame(const cv::Mat & frame)
{
    frame_ = frame;
}

// way1
int
BBoxPrinter::getPointUVCoordinates(const pcl::PointXYZ & pt,
    cv::Point & UV_coordinates)
{
  if (pt.z > 0)
  {
    // project point on camera's image plane
    UV_coordinates.x = round((focal_x_ * (pt.x / pt.z) + cx_));
    UV_coordinates.y = round((focal_y_ * (pt.y / pt.z) + cy_));

    // point is visible!
    float normalized_uv_x = UV_coordinates.x / sizeX_;
    float normalized_uv_y = UV_coordinates.y / sizeY_;

    if (normalized_uv_x >= 0.0 && normalized_uv_x <= 1.0
        && normalized_uv_y >= 0.0 && normalized_uv_y <= 1.0)
      return 0; // point was visible by the camera

    // point is NOT visible by the camera
    return 1;
  }

  // point is NOT visible by the camera because it is behind it
  return 2;
}

// way2
void
BBoxPrinter::project3DPointToPixel(const pcl::PointXYZ & pt,
                      cv::Point & UV_coordinates)
{
    Eigen::Vector4f point3d(pt.x, pt.y, pt.z, 1);
    Eigen::Vector3f point2d;

    //P matrix is in camera_info.yaml
    Eigen::Matrix<float, 3,4> P;
    P <<  focal_x_, 0, cx_, 0, 0, focal_y_, cy_, 0, 0, 0, 1, 0;

    //Let's do P*point and rescale X,Y
    point2d = P * point3d;
    point2d[0] /= point2d[2];
    point2d[1] /= point2d[2];

    UV_coordinates.x = point2d[0];
    UV_coordinates.y = point2d[1];
}

/* The translation from kinect2_head_ir_optical_frame_
to kinect2_head_rgb_optical_frame_ is neglected */
void
BBoxPrinter::print2DBoundingBox(const pcl::PointXYZ & old_ref_min_p_cluster,
    const pcl::PointXYZ & old_ref_max_p_cluster,
    cv::Point & min_pt,
    cv::Point & max_pt,
    const bool found,
    const std::string & label)
{
    // from ir frame_ to rgb frame_
    Eigen::Transform<float, 3, Eigen::Affine> pose;
    pose(0,0) = 1.0; pose(0,1) = 0.0; pose(0,2) = 0.0; pose(0,3) = -0.052; // ir on the left, rgb on the right
    pose(1,0) = 0.0; pose(1,1) = 1.0; pose(1,2) = 0.0; pose(1,3) = 0.0;
    pose(2,0) = 0.0; pose(2,1) = 0.0; pose(2,2) = 1.0; pose(2,3) = 0.0;
    pcl::PointXYZ rgb_old_ref_min_p_cluster = pcl::transformPoint(old_ref_min_p_cluster, pose);
    pcl::PointXYZ rgb_old_ref_max_p_cluster = pcl::transformPoint(old_ref_max_p_cluster, pose);

    //cv::Point min_pt;
    int ret1 = getPointUVCoordinates(rgb_old_ref_min_p_cluster, min_pt);
    //cv::Point max_pt;
    int ret2 = getPointUVCoordinates(rgb_old_ref_max_p_cluster, max_pt);

    /*
    cv::Point min_pt2;
    int ret3 = getPointUVCoordinates(rgb_old_ref_min_p_cluster, min_pt2);
    cv::Point max_pt2;
    int ret4 = getPointUVCoordinates(rgb_old_ref_max_p_cluster, max_pt2);
    */

    /*
    cv::Point min_pt2;
    cv::Point max_pt2;
    project3DPointToPixel(old_ref_min_p_cluster, min_pt2);
    project3DPointToPixel(old_ref_min_p_cluster, max_pt2);

    std::cout << "min_pt " << min_pt << std::endl;
    std::cout << "min_pt2 " << min_pt2 << std::endl;
    */

    if (ret1 != 0 || ret2 != 0)
    {
        std::cerr << "Warning: out of image bounds box" << std::endl;
    }
    if (found)
    {
        cv::rectangle(frame_, cv::Point(min_pt.x, min_pt.y),
                  cv::Point(max_pt.x, max_pt.y),
                  cv::Scalar(255,0,0), 20);
    }
    else
    {
        cv::rectangle(frame_, cv::Point(min_pt.x, min_pt.y),
                  cv::Point(max_pt.x, max_pt.y),
                  cv::Scalar(0,0,255), 20);
    }
    cv::circle(frame_, min_pt,
              20, cv::Scalar(255,0,0), 5);
    cv::circle(frame_, max_pt,
              20, cv::Scalar(0,255,0), 5); // max "point" but lower point in 2D... why?
    cv::Point center_pt;
    center_pt.x = (min_pt.x + max_pt.x) / 2.0;
    center_pt.y = (min_pt.y + max_pt.y) / 2.0;
    cv::putText(frame_, label, cv::Point(min_pt.x, min_pt.y),
                cv::FONT_HERSHEY_SIMPLEX, 2, CV_RGB(0,0,0), 5, 8);

    /*
    if (ret3 != 0 || ret4 != 0)
    {
        std::cerr << "Warning: out of image bounds box 2" << std::endl;
    }
    cv::rectangle(frame_, cv::Point(min_pt2.x, min_pt2.y),
              cv::Point(max_pt2.x, max_pt2.y),
              cv::Scalar(255,255,255), 5);
    */

    cv::namedWindow("frame", CV_WINDOW_KEEPRATIO);
    cv::imshow( "frame", frame_ );
    cv::waitKey(1);
}

void 
BBoxPrinter::basicPrint2DBoundingBox(pcl::PointCloud<pcl::PointXYZ>::Ptr & cluster_cloud,
        cv::Point & point1,
        cv::Point & point2,
        const bool found,
        const std::string & label)
{
    Eigen::Transform<float, 3, Eigen::Affine> pose;
    pose(0,0) = 1.0; pose(0,1) = 0.0; pose(0,2) = 0.0; pose(0,3) = -0.052; // ir on the left, rgb on the right
    pose(1,0) = 0.0; pose(1,1) = 1.0; pose(1,2) = 0.0; pose(1,3) = 0.0;
    pose(2,0) = 0.0; pose(2,1) = 0.0; pose(2,2) = 1.0; pose(2,3) = 0.0;

    std::vector<cv::Point> cluster2D;
    for(size_t i = 0; i < cluster_cloud->points.size(); i++)
    {
        pcl::PointXYZ rgbPoint = pcl::transformPoint(cluster_cloud->points[i], pose);

        cv::Point point2D;
        int ret= getPointUVCoordinates(rgbPoint,
            point2D);
        if (ret == 0)
            cluster2D.push_back(point2D);
    }

    std::cout << cluster2D.size() << std::endl;
    if (cluster2D.size() <= 0)
        return;

    cv::Point pointWithMinX = cluster2D[0];
    cv::Point pointWithMinY = cluster2D[0];
    cv::Point pointWithMaxX = cluster2D[0];
    cv::Point pointWithMaxY = cluster2D[0];
    for(size_t i = 1; i < cluster2D.size(); i++)
    {
        if(cluster2D[i].x < pointWithMinX.x)
        {
            pointWithMinX = cluster2D[i];
        }
        if(cluster2D[i].x > pointWithMaxX.x)
        {
            pointWithMaxX = cluster2D[i];
        }
        if(cluster2D[i].y < pointWithMinY.y)
        {
            pointWithMinY = cluster2D[i];
        }
        if(cluster2D[i].y > pointWithMaxY.y)
        {
            pointWithMaxY = cluster2D[i];
        }


    }

    point1.x = pointWithMinX.x;
    point1.y = pointWithMinY.y;

    point2.x = pointWithMaxX.x;
    point2.y = pointWithMaxY.y;

    if (found)
    {
        cv::rectangle(frame_, cv::Point(point1.x, point1.y),
                  cv::Point(point2.x, point2.y),
                  cv::Scalar(0,255,0), 20);
    }
    else
    {
        cv::rectangle(frame_, cv::Point(point1.x, point1.y),
                  cv::Point(point2.x, point2.y),
                  cv::Scalar(0,0,255), 20);
    }

    if (frame_.cols <= 0 || frame_.rows <= 0)
    {
        std::cerr << "Please check topic for RGB images" << std::endl;
    }
    else
    {
        cv::namedWindow("frame", CV_WINDOW_KEEPRATIO);
        cv::imshow( "frame", frame_ );
        cv::waitKey(1);
    }
}

void
BBoxPrinter::getPointWithMinMaxCoords(const pcl::PointCloud<pcl::PointXYZ>::Ptr & cloud,
        pcl::PointXYZ & pointWithMinX,
        pcl::PointXYZ & pointWithMinY,
        pcl::PointXYZ & pointWithMinZ,
        pcl::PointXYZ & pointWithMaxX,
        pcl::PointXYZ & pointWithMaxY,
        pcl::PointXYZ & pointWithMaxZ)
{
    if (cloud->points.size() > 0)
    {
        pointWithMinX = cloud->points.at(0);
        pointWithMinY = cloud->points.at(0);
        pointWithMinZ = cloud->points.at(0);
        pointWithMaxX = cloud->points.at(0);
        pointWithMaxY = cloud->points.at(0);
        pointWithMaxZ = cloud->points.at(0);
        for(size_t i = 1; i < cloud->points.size(); i++)
        {
            if(cloud->points.at(i).x < pointWithMinX.x)
            {
                pointWithMinX = cloud->points.at(i);
            }
            if(cloud->points.at(i).x > pointWithMaxX.x)
            {
                pointWithMaxX = cloud->points.at(i);
            }
            if(cloud->points.at(i).y < pointWithMinY.y)
            {
                pointWithMinY = cloud->points.at(i);
            }
            if(cloud->points.at(i).y > pointWithMaxY.y)
            {
                pointWithMaxY = cloud->points.at(i);
            }
            if(cloud->points.at(i).z < pointWithMinZ.z)
            {
                pointWithMinZ = cloud->points.at(i);
            }
            if(cloud->points.at(i).z > pointWithMaxZ.z)
            {
                pointWithMaxZ = cloud->points.at(i);
            }
        }
    }
}

void 
BBoxPrinter::advancedPrint2DBoundingBox(pcl::PointCloud<pcl::PointXYZ>::Ptr cluster_cloud,
        cv::Point & point1,
        cv::Point & point2,
        const bool found,
        const std::string & label)
{
    // Find 3D borders

    pcl::PointXYZ pointWithMinX;
    pcl::PointXYZ pointWithMinY;
    pcl::PointXYZ pointWithMinZ;
    pcl::PointXYZ pointWithMaxX;
    pcl::PointXYZ pointWithMaxY;
    pcl::PointXYZ pointWithMaxZ;
    getPointWithMinMaxCoords(cluster_cloud,
                             pointWithMinX, pointWithMinY, pointWithMinZ,
                             pointWithMaxX, pointWithMaxY, pointWithMaxZ);

    // RT

    Eigen::Transform<float, 3, Eigen::Affine> pose;
    pose(0,0) = 1.0; pose(0,1) = 0.0; pose(0,2) = 0.0; pose(0,3) = -0.052; // ir on the left, rgb on the right
    pose(1,0) = 0.0; pose(1,1) = 1.0; pose(1,2) = 0.0; pose(1,3) = 0.0;
    pose(2,0) = 0.0; pose(2,1) = 0.0; pose(2,2) = 1.0; pose(2,3) = 0.0;
    pcl::PointXYZ rgbPointWithMinX = pcl::transformPoint(pointWithMinX, pose);
    pcl::PointXYZ rgbPointWithMinY = pcl::transformPoint(pointWithMinY, pose);
    pcl::PointXYZ rgbPointWithMaxX = pcl::transformPoint(pointWithMaxX, pose);
    pcl::PointXYZ rgbPointWithMaxY = pcl::transformPoint(pointWithMaxY, pose);

    // Find 2D borders

    cv::Point pointWithMinX2D;
    cv::Point pointWithMinY2D;
    cv::Point pointWithMaxX2D;
    cv::Point pointWithMaxY2D;
    int ret;
    ret = getPointUVCoordinates(rgbPointWithMinX,
        pointWithMinX2D);
    ret = getPointUVCoordinates(rgbPointWithMinY,
        pointWithMinY2D);
    ret = getPointUVCoordinates(rgbPointWithMaxX,
        pointWithMaxX2D);
    ret = getPointUVCoordinates(rgbPointWithMaxY,
        pointWithMaxY2D);
    //cv::Point point1;
    point1.x = pointWithMinX2D.x;
    point1.y = pointWithMinY2D.y;
    //cv::Point point2;
    point2.x = pointWithMaxX2D.x;
    point2.y = pointWithMaxY2D.y;

    if (found)
    {
        cv::rectangle(frame_, cv::Point(point1.x, point1.y),
                  cv::Point(point2.x, point2.y),
                  cv::Scalar(255,0,0), 20);
    }
    else
    {
        cv::rectangle(frame_, cv::Point(point1.x, point1.y),
                  cv::Point(point2.x, point2.y),
                  cv::Scalar(0,0,255), 20);
    }

    cv::namedWindow("frame", CV_WINDOW_KEEPRATIO);
    cv::imshow( "frame", frame_ );
    cv::waitKey(1);
}

void
BBoxPrinter::mask_rgb_frame(const pcl::PointCloud<pcl::PointXYZ>::Ptr & cloud)
{
    mask_.create(frame_.rows, frame_.cols, frame_.type());
    mask_ = cv::Scalar(0,0,0);

    for(size_t i = 0; i < cloud->points.size(); i++)
    {
        cv::Point point2D;
        int ret= getPointUVCoordinates(cloud->points[i],
            point2D);
        if (ret == 0)
                mask_.at<cv::Vec3b>(point2D) = frame_.at<cv::Vec3b>(point2D);
    }

    cv::namedWindow("mask", CV_WINDOW_KEEPRATIO);
    cv::imshow("mask", mask_);
    cv::waitKey(1);
}
