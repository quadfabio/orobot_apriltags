#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>

#include <pcl/common/common.h>
#include <pcl/common/pca.h>
#include <pcl/filters/conditional_removal.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/io/pcd_io.h>
#include <pcl/segmentation/extract_clusters.h>

#include "orobot_fall_detection/CloudFilter.h"

void
CloudFilter::filter_cloud(const pcl::PointCloud<pcl::PointXYZ>::Ptr & input_cloud,
             const float leafSize,
             const int meanK,
             const float stddevMulThresh,
             pcl::PointCloud<pcl::PointXYZ>::Ptr & output_cloud)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_cloud (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::VoxelGrid<pcl::PointXYZ> vg;
    vg.setInputCloud (input_cloud);
    vg.setLeafSize (leafSize, leafSize, leafSize);
    vg.filter (*tmp_cloud);

    pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor;
    sor.setInputCloud (tmp_cloud);
    sor.setMeanK (meanK);
    sor.setStddevMulThresh (stddevMulThresh);
    sor.filter (*output_cloud);
}

void
CloudFilter::extract_roi(const pcl::PointCloud<pcl::PointXYZ>::Ptr & input_cloud,
            pcl::PointCloud<pcl::PointXYZ>::Ptr & output_cloud)
{
    float z_lower_limit = 0;
    float z_upper_limit = 3.5;
    float y_lower_limit = 0;

    pcl::ConditionAnd<pcl::PointXYZ>::Ptr range_cond (new pcl::ConditionAnd<pcl::PointXYZ> ());

    range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
        pcl::FieldComparison<pcl::PointXYZ> ("z", pcl::ComparisonOps::GT, z_lower_limit)));

    range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
        pcl::FieldComparison<pcl::PointXYZ> ("z", pcl::ComparisonOps::LT, z_upper_limit)));

    range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
        pcl::FieldComparison<pcl::PointXYZ> ("y", pcl::ComparisonOps::GT, y_lower_limit)));

    // build the filter
    pcl::ConditionalRemoval<pcl::PointXYZ> condrem (range_cond);
    condrem.setInputCloud (input_cloud);
    condrem.setKeepOrganized(true);

    // apply filter
    condrem.filter (*output_cloud);

    /*
    pcl::PassThrough<pcl::PointXYZ> pass;
    pass.setInputCloud (input_cloud);
    pass.setFilterFieldName ("z");
    pass.setFilterLimits (z_lower_limit, z_upper_limit);
    pass.filter (*output_cloud);
    */
}

void
CloudFilter::remove_ground(const pcl::PointCloud<pcl::PointXYZ>::Ptr & input_cloud,
              const float k2_height, // in meters
              const float ground_tolerance,
              pcl::PointCloud<pcl::PointXYZ>::Ptr & ground_cloud,
              pcl::PointCloud<pcl::PointXYZ>::Ptr & no_ground_cloud)
{
    float lower_limit = k2_height - ground_tolerance; // y is inverted!
    float upper_limit = k2_height + 10*ground_tolerance; // attention to points under the ground! k2 fault

    pcl::PassThrough<pcl::PointXYZ> pass;
    pass.setInputCloud (input_cloud);
    pass.setFilterFieldName ("y");
    pass.setFilterLimits (lower_limit, upper_limit);
    pass.filter (*ground_cloud);

    pass.setFilterLimitsNegative (true);
    pass.filter (*no_ground_cloud);
}

void
CloudFilter::find_candidates(const pcl::PointCloud<pcl::PointXYZ>::Ptr & input_cloud,
                const float cluster_tolerance,
                const int min_cluster,
                const int max_cluster,
                std::vector<pcl::PointIndices> & cluster_indices)
{
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
    tree->setInputCloud (input_cloud);

    pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
    ec.setClusterTolerance (cluster_tolerance);
    ec.setMinClusterSize (min_cluster);
    ec.setMaxClusterSize (max_cluster);
    ec.setSearchMethod (tree);
    ec.setInputCloud (input_cloud);
    ec.extract (cluster_indices);
}

void
CloudFilter::computeOBB(const pcl::PointCloud<pcl::PointXYZ>::Ptr& point_cloud_ptr,
                Eigen::Vector3f& tfinal,
                Eigen::Quaternionf& qfinal,
                pcl::PointXYZ& min_pt,
                pcl::PointXYZ& max_pt,
                pcl::PointXYZ& old_ref_min_pt,
                pcl::PointXYZ& old_ref_max_pt)
{
    // compute principal direction
    Eigen::Vector4f centroid;
    pcl::compute3DCentroid(*point_cloud_ptr, centroid);
    Eigen::Matrix3f covariance;
    computeCovarianceMatrixNormalized(*point_cloud_ptr, centroid, covariance);
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigen_solver(covariance, Eigen::ComputeEigenvectors);
    Eigen::Matrix3f eigDx = eigen_solver.eigenvectors();
    eigDx.col(2) = eigDx.col(0).cross(eigDx.col(1));

    // move the points to that reference frame
    Eigen::Matrix4f p2w(Eigen::Matrix4f::Identity());
    p2w.block<3,3>(0,0) = eigDx.transpose();
    p2w.block<3,1>(0,3) = -1.f * (p2w.block<3,3>(0,0) * centroid.head<3>());
    pcl::PointCloud<pcl::PointXYZ> cPoints;
    pcl::transformPointCloud(*point_cloud_ptr, cPoints, p2w);

    pcl::getMinMax3D(cPoints, min_pt, max_pt);
    const Eigen::Vector3f mean_diag = 0.5f*(max_pt.getVector3fMap() + min_pt.getVector3fMap());

    // final transform
    qfinal = Eigen::Quaternionf(eigDx);
    tfinal = eigDx*mean_diag + centroid.head<3>();

    // draw the cloud and the box
    //      pcl::visualization::PCLVisualizer viewer;
    //      viewer.addPointCloud(point_cloud_ptr);
    //      viewer.addCube(tfinal, qfinal, max_pt.x - min_pt.x, max_pt.y - min_pt.y, max_pt.z - min_pt.z);
    //      viewer.spin();

    Eigen::Affine3f inv_p2w(p2w); // it would be better to retrieve them with the indexes
    inv_p2w = inv_p2w.inverse();
    old_ref_min_pt = pcl::transformPoint(min_pt, inv_p2w);
    old_ref_max_pt = pcl::transformPoint(max_pt, inv_p2w);
}

void
CloudFilter::get_cluster_sorted_dims(const pcl::PointCloud<pcl::PointXYZ>::Ptr & cluster_cloud,
                        std::vector<float> & dims,
                        pcl::PointXYZ & old_ref_min_p_cluster,
                        pcl::PointXYZ & old_ref_max_p_cluster)
{
    dims.resize(3);

    pcl::PointXYZ min_p_cluster;
    pcl::PointXYZ max_p_cluster;
    //pcl::PointXYZ old_ref_min_p_cluster;
    //pcl::PointXYZ old_ref_max_p_cluster;

    Eigen::Vector3f trasl;
    Eigen::Quaternionf quat;
    Eigen::Vector4f centroid;
    computeOBB(cluster_cloud, trasl, quat,
               min_p_cluster, max_p_cluster,
               old_ref_min_p_cluster, old_ref_max_p_cluster);

    dims[0] = max_p_cluster.x - min_p_cluster.x;
    dims[1] = max_p_cluster.y - min_p_cluster.y;
    dims[2] = max_p_cluster.z - min_p_cluster.z;
    std::sort(dims.begin(), dims.end());
}
