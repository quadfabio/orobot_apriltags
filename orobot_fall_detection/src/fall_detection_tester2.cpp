#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <time.h>

#include <ros/package.h>
#include <ros/ros.h>
#include <image_geometry/pinhole_camera_model.h>
#include <depth_image_proc/depth_conversions.h>

#include <dynamic_reconfigure/server.h>
#include <orobot_fall_detection/dynamic_fall_detection_paramsConfig.h>

// read image
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <opencv2/ml/ml.hpp>

// read cloud
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <boost/foreach.hpp>

#include "boost/date_time/posix_time/ptime.hpp"
#include <boost/filesystem.hpp>

#include "orobot_fall_detection/BBoxPrinter.h"
#include "orobot_fall_detection/CloudFilter.h"

#include <pcl/common/common.h>
#include <pcl/common/pca.h>
#include <pcl/filters/conditional_removal.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/io/pcd_io.h>
#include <pcl/segmentation/extract_clusters.h>

// Global variables
pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
cv::Mat frame;
orobot_fall_detection::dynamic_fall_detection_paramsConfig params;

void callback(orobot_fall_detection::dynamic_fall_detection_paramsConfig &config, uint32_t level) {
  params = config;
  ROS_INFO("Reconfigure Request: %s %s %f %d %f %f %f %f %d %d %d %f %f %f",
            config.enable_visualization?"True":"False",
            config.enable_audible_alarm?"True":"False",
            config.leafSize,
            config.meanK,
            config.stddevMulThresh,
            config.k2_height,
            config.ground_tolerance,
            config.cluster_tolerance,
            config.starting_min_cluster,
            config.max_cluster,
            config.min_cluster,
            config.robot_height,
            config.max_height,
            config.min_height);
}

/* TODO: use templates */
void publish_cloud(const ros::Publisher & pub,
    pcl::PointCloud<pcl::PointXYZ>::Ptr msg)
{
    msg->header.frame_id = "kinect2_head_ir_optical_frame";
    //msg->header.stamp = ros::Time::now().toNSec();
    pub.publish (msg);
}

void publish_cloud(const ros::Publisher & pub,
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr msg)
{
    msg->header.frame_id = "kinect2_head_ir_optical_frame";
    //msg->header.stamp = ros::Time::now().toNSec();
    pub.publish (msg);
}

int random_intensity()
{
    return rand() % 256;
}

float
check_with_classifier(const CvRTrees & rtrees,
        const std::vector<float> & features)
{
    cv::Mat sample = (cv::Mat_<float>(1,4)
        << 0.0f, features[0], features[1], features[2]);
    std::copy(features.begin(), features.end(),
        std::ostream_iterator<float>(std::cout, " "));

    return rtrees.predict_prob(sample);
}

void
add_entry_to_training_set(std::ofstream & training_file,
    const std::vector<float> & sorted_dims,
    const int c)
{
    training_file << c << ","
        << sorted_dims[0] << ","
        << sorted_dims[1] << ","
        << sorted_dims[2] << std::endl;
}

// return the filenames of all files that have the specified extension
// in the specified directory and all subdirectories
void get_sorted_all(const boost::filesystem::path& root, const std::string& ext, std::vector<boost::filesystem::path>& ret)
{
    if(!boost::filesystem::exists(root) || !boost::filesystem::is_directory(root)) return;

    boost::filesystem::recursive_directory_iterator it(root);
    boost::filesystem::recursive_directory_iterator endit;

    while(it != endit)
    {
        if(boost::filesystem::is_regular_file(*it) && it->path().extension() == ext)
            ret.push_back(it->path());
        ++it;
    }

    std::sort(ret.begin(), ret.end());
}

int main (int argc, char **argv)
{
    ros::init(argc, argv, "fall_detection_node");
    ros::NodeHandle nh;

    const int starting_frame = 0;
    boost::filesystem::path cloud_folder = "/data/fall_detection_cad1/cad1_rate1_5/cloud/"; //"/media/fall_detection_lab1/fall_detection_dataset/lab1_rate1_5/cloud/";
    boost::filesystem::path image_folder = "/data/fall_detection_cad1/cad1_rate1_5/rgb/"; //"/home/morris/dataset/fall_detection_dataset/lab1_rate1_5/rgb/";
    std::string results_filename = "/data/fall_detection_cad1/cad1_rate1_5/results_04_02_16.txt"; //"/home/morris/dataset/fall_detection_dataset/lab1_rate1_5/results_01_02_16.txt";
    std::vector<boost::filesystem::path> clouds;
    std::vector<boost::filesystem::path> images;
    get_sorted_all(cloud_folder, ".pcd", clouds);
    get_sorted_all(image_folder, ".jpg", images);

    std::cout << "clouds size " << clouds.size() << std::endl;
    std::cout << "images size " << images.size() << std::endl;

    std::ofstream results_file;
    results_file.open (results_filename.c_str());
    //results_file << "frame topleftcorner1.x topleftcorner1.y width1 height1..." << std::endl;

    dynamic_reconfigure::Server<orobot_fall_detection::dynamic_fall_detection_paramsConfig> server;
    dynamic_reconfigure::Server<orobot_fall_detection::dynamic_fall_detection_paramsConfig>::CallbackType f;

    f = boost::bind(&callback, _1, _2);
    server.setCallback(f);

    ros::Publisher cloud_pub;
    ros::Publisher filtered_cloud_pub;
    ros::Publisher ground_cloud_pub;
    ros::Publisher no_ground_cloud_pub;
    ros::Publisher clusterized_cloud_pub;
    ros::Publisher bbox3D_cloud_pub;
    if (params.enable_visualization)
    {
        cloud_pub = nh.advertise< pcl::PointCloud<pcl::PointXYZ> >
                ("/fall_detection/cloud", 1);
        filtered_cloud_pub = nh.advertise< pcl::PointCloud<pcl::PointXYZ> >
                ("/fall_detection/filtered_cloud", 1);
        ground_cloud_pub = nh.advertise< pcl::PointCloud<pcl::PointXYZ> >
                ("/fall_detection/ground_cloud", 1);
        no_ground_cloud_pub = nh.advertise< pcl::PointCloud<pcl::PointXYZ> >
                ("/fall_detection/no_ground_cloud", 1);
        clusterized_cloud_pub = nh.advertise< pcl::PointCloud<pcl::PointXYZRGB> >
                ("/fall_detection/clusterized_cloud", 1);
        bbox3D_cloud_pub = nh.advertise< pcl::PointCloud<pcl::PointXYZ> >
                ("/fall_detection/bbox3D_cloud", 1);
    }

    std::ofstream training_file;
    std::stringstream tfs;
    tfs << ros::package::getPath("orobot_fall_detection")
       << "/cfg/training_file.csv";
    training_file.open(tfs.str().c_str());

    CvRTrees rtrees;
    rtrees.load(std::string(ros::package::getPath("orobot_fall_detection") +
                            "/cfg/rtrees_classifier").c_str());

    srand (time(NULL));

    BBoxPrinter bbp;
    CloudFilter cf;

    int step = 1;
    int total_candidates = 0;
    int true_positives = 0;
    int false_positives = 0;
    int false_negatives = 0;
    ros::Rate loop_rate(30.0);
    for (unsigned int i = 0; i >= 0 && i < clouds.size() && ros::ok(); i = i + step)
    {
        if (i < starting_frame)
            continue;

        results_file << i << " ";

        std::cout << "Current cloud " << clouds[i] << std::endl;
        std::cout << "Current image " << images[i] << std::endl;
        if (pcl::io::loadPCDFile<pcl::PointXYZ> (clouds[i].c_str(), *cloud) == -1) //* load the file
        {
            PCL_ERROR ("Couldn't read file pcd \n");
            return (-1);
        }
        frame = cv::imread(images[i].c_str(), CV_LOAD_IMAGE_COLOR);
        bbp.set_frame(frame);
        cv::namedWindow("frame", CV_WINDOW_KEEPRATIO);
        cv::imshow( "frame", frame );
        char ch = cv::waitKey(1);
        if (ch == 's')
        {
            step = 0;
        }
        else if (ch == 'a')
        {
            step = -1;
        }
        else if (ch == 'd')
        {
            step = 1;
        }

        //mask_rgb_frame();

        boost::posix_time::ptime process_start, process_stop;
        process_start = boost::posix_time::microsec_clock::universal_time();

        /*
        ROS_INFO("Reconfigure Request: %s %s %f %d %f %f %f %f %d %d %d %f %f %f",
            params.enable_visualization?"True":"False",
            params.enable_audible_alarm?"True":"False",
            params.leafSize,
            params.meanK,
            params.stddevMulThresh,
            params.k2_height,
            params.ground_tolerance,
            params.cluster_tolerance,
            params.starting_min_cluster,
            params.max_cluster,
            params.min_cluster,
            params.robot_height,
            params.max_height,
            params.min_height);
        */

        int fallen_people = 0;

        // Extract a region of interest for efficiency and accuracy
        pcl::PointCloud<pcl::PointXYZ>::Ptr roi_cloud (new pcl::PointCloud<pcl::PointXYZ>);
        cf.extract_roi(cloud, roi_cloud);

        // Pre-filtering
        pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cloud (new pcl::PointCloud<pcl::PointXYZ>);
        cf.filter_cloud(roi_cloud, params.leafSize, params.meanK, params.stddevMulThresh, filtered_cloud);
        //std::cout << "Cloud before filtering: " << std::endl;
        //std::cout << *cloud << std::endl;
        //std::cout << "Cloud after filtering: " << std::endl;
        //std::cout << *filtered_cloud << std::endl;

        // Removing ground
        pcl::PointCloud<pcl::PointXYZ>::Ptr ground_cloud (new pcl::PointCloud<pcl::PointXYZ>);
        pcl::PointCloud<pcl::PointXYZ>::Ptr no_ground_cloud (new pcl::PointCloud<pcl::PointXYZ>);
        cf.remove_ground(filtered_cloud,
                      params.k2_height,
                      params.ground_tolerance,
                      ground_cloud,
                      no_ground_cloud);

        // Find candidates
        std::vector<pcl::PointIndices> cluster_indices;
        cf.find_candidates(no_ground_cloud, params.cluster_tolerance, params.starting_min_cluster, params.max_cluster, cluster_indices);
        ROS_INFO_STREAM("Clusters found! " << cluster_indices.size());

        pcl::PointCloud<pcl::PointXYZ>::Ptr bbox3D_cloud (new pcl::PointCloud<pcl::PointXYZ>);
        // TODO: inefficient loop
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr clusterized_cloud (new pcl::PointCloud<pcl::PointXYZRGB>);
        int id = 0;
        for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin ();
            it != cluster_indices.end ();
            ++it)
        {
            int random_r = random_intensity();
            int random_g = random_intensity();
            int random_b = random_intensity();
            if (params.enable_visualization)
            {
                random_r = random_intensity();
                random_g = random_intensity();
                random_b = random_intensity();
            }

            pcl::PointCloud<pcl::PointXYZ>::Ptr cluster_cloud (new pcl::PointCloud<pcl::PointXYZ>);
            for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
            {
                cluster_cloud->points.push_back (no_ground_cloud->points[*pit]);

                if (params.enable_visualization)
                {
                    pcl::PointXYZRGB pt;
                    pt.x = no_ground_cloud->points[*pit].x;
                    pt.y = no_ground_cloud->points[*pit].y;
                    pt.z = no_ground_cloud->points[*pit].z;
                    pt.r = random_r;
                    pt.g = random_g;
                    pt.b = random_b;
                    clusterized_cloud->points.push_back (pt);
                }
            }
            cluster_cloud->width = cluster_cloud->points.size ();
            cluster_cloud->height = 1;
            cluster_cloud->is_dense = true;

            pcl::PointXYZ min;
            pcl::PointXYZ max;
            pcl::getMinMax3D (*cluster_cloud, min, max); // TODO: at least one... more efficient










            pcl::PointXYZ best_min_p;
            pcl::PointXYZ best_max_p;
            std::vector<float> sorted_dims;
            cf.get_cluster_sorted_dims(cluster_cloud, sorted_dims, best_min_p, best_max_p);

            // For checking 3D boxes
            bbox3D_cloud->points.push_back(best_min_p);
            bbox3D_cloud->points.push_back(best_max_p);
            publish_cloud(bbox3D_cloud_pub, bbox3D_cloud);

            std::stringstream label;
            label << "S:";
            label << cluster_cloud->points.size();

            cv::Point min_pt;
            cv::Point max_pt;

            // min.y < robot_height = higher cluster point is higher than turtlebot platform
            // min.y < 0 = it is higher than k2
            if(min.y < params.max_height
                //|| max.y < params.min_height // no suspended/flying blobs
                || cluster_cloud->points.size() < params.min_cluster)
            {
                ROS_INFO_STREAM("Loose geometric check failed!");

                if (params.enable_visualization)
                {
                    //print2DBoundingBox(best_min_p, best_max_p, min_pt, max_pt, false, label.str());
                    bbp.basicPrint2DBoundingBox(cluster_cloud, min_pt, max_pt, false, label.str());
                }

                continue;
            }

            total_candidates++;

            float confidence = check_with_classifier(rtrees, sorted_dims);

            label << " C:" << confidence;
            if (confidence < 0.5)
            {
                ROS_INFO_STREAM("Classifier check failed!");

                if (params.enable_visualization)
                {
                    //print2DBoundingBox(best_min_p, best_max_p, min_pt, max_pt, false, label.str());
                    bbp.basicPrint2DBoundingBox(cluster_cloud, min_pt, max_pt, false, label.str());
                }

                continue;
            }

            ROS_INFO_STREAM("All checks passed!");

            if (params.enable_visualization) // true
            {
                //print2DBoundingBox(best_min_p, best_max_p, min_pt, max_pt, true, label.str());
                bbp.basicPrint2DBoundingBox(cluster_cloud, min_pt, max_pt, true, label.str());
            }

            if (params.enable_audible_alarm)
            {
                std::stringstream ss;
                ss << "canberra-gtk-play -f "<<
                      ros::package::getPath("orobot_fall_detection") <<
                      "/cfg/uomoaterra.ogg &";
                int ret = system(ss.str().c_str());
            }

            results_file << min_pt.x << " "
                         << min_pt.y << " "
                         << max_pt.x - min_pt.x << " "
                         << max_pt.y - min_pt.y << " ";

            /*
            cv::namedWindow("frame", CV_WINDOW_KEEPRATIO);
            cv::imshow( "frame", frame );
            char c = cv::waitKey(0);

            if (c == 's')
            {
                stop = !stop;
            }
            else if (ch == 'a')
            {
                step = 1;
            }
            else if (ch == 'd')
            {
                step = -1;
            }*/

            /*
            if (c == 'k')
            {
                true_positives++;
            }
            else
            {
                false_positives++;
            }


            fallen_people++;*/
        }

        results_file << std::endl;

        if (params.enable_visualization)
        {
            publish_cloud(cloud_pub, cloud);
            publish_cloud(filtered_cloud_pub, filtered_cloud);
            publish_cloud(ground_cloud_pub, ground_cloud);
            publish_cloud(no_ground_cloud_pub, no_ground_cloud);
            publish_cloud(clusterized_cloud_pub, clusterized_cloud);
        }

        /*
        std::cout << "NUMBER OF FALLEN PEOPLE: " << fallen_people << std::endl;
        if (fallen_people < 1)
        {
            false_negatives++;
        }
        */

        ros::spinOnce();
        loop_rate.sleep();

        process_stop = boost::posix_time::microsec_clock::universal_time();
        std::cout << "FPS = " << (1e6f) / (process_stop - process_start).total_microseconds()  << std::endl;
    }

    results_file.close();

    /*
    std::cout << "total candidates " << total_candidates << std::endl;
    std::cout << "true positives " << true_positives << std::endl; // max is 118
    std::cout << "false positives " << false_positives << std::endl;
    std::cout << "false negatives " << false_negatives << std::endl; // -7
    */

    training_file.close();

    return 0;
}
