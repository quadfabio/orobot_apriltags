/*
 * pointCloudUtils.h
 *
 *  Created on: Oct 18, 2012
 *      Author: Matteo Munaro
 */

#ifndef READ_AND_DISPLAY_UTILS_H_
#define READ_AND_DISPLAY_UTILS_H

#include <opencv2/opencv.hpp>
#include <dirent.h>
#include <opencv2/opencv.hpp>
//#include <stdio.h>
#include <cstring>
#include <fstream>
#include <istream>

//// CONSTANTS: ////
const int NUMBER_OF_FIDUCIAL_POINTS = 10;
const int SURF_DESCRIPTOR_DIMENSION = 64;//128;
const int NUMBER_OF_SKELETAL_JOINTS = 20;
const int NUMBER_OF_JOINT_INFO = 14;
const int NUMBER_OF_TRACKED_SKELETON = 2;
/////////////////////

//// Define useful colors: ////
const cv::Scalar red(0,0,255);
const cv::Scalar green(0,255,0);
const cv::Scalar blue(255,0,0);
const cv::Scalar cyan(255,255,0);
const cv::Scalar magenta(255,0,255);
const cv::Scalar black(0,0,0);
///////////////////////////////

using namespace std;

void readKinectDataFilenames(std::string dataDirectory, std::vector<std::string>& rgbFilenames, std::vector<std::string>& depthFilenames,
			std::vector<std::string>& userMapFilenames, std::vector<std::string>& skeletonFilenames, std::vector<std::string>& groundCoeffFilenames);

void readSkeletonData(std::string skeletonFilename, std::vector<float>& skeletonsData);

void readGroundCoeff(std::string groundCoeffFilename, std::vector<float>& currentGroundCoeff, std::vector<float>& validGroundCoeff);

void displaySkeletonOnImage(cv::Mat rgbImage, std::vector<float>& skeletonsData, bool singleColor, bool flipY, float scaleFactor);

void readFaceTrainingSet(std::string faceTrainingSetDir, std::string trainingSetFilename, std::string faceTrainingSetDescriptorsDir, std::string faceTrainingSetImagesDir,
		std::vector<int>& labels, std::vector<cv::Mat>& trainingDescriptors, std::vector<cv::Mat>& trainingFaces);

void generateColors(int colorsNumber, std::vector<cv::Vec3f>& bodyPartsColors);

#endif /* READ_AND_DISPLAY_UTILS_H */

