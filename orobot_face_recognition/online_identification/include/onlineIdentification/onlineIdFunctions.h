#ifndef ONLINE_ID_FUNCTIONS_H_
#define ONLINE_ID_FUNCTIONS_H_

//togliere quelli inutili
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/sample_consensus/sac_model_plane.h>
/////////////////////////////////

#include <faceRecognition/forest.hpp>
#include <faceRecognition/multi_part_sample.hpp>
#include <faceRecognition/head_pose_sample.hpp>
#include <faceRecognition/face_utils.hpp>
#include <istream>
#include <opencv2/opencv.hpp>
#include <boost/progress.hpp>
#include <boost/thread.hpp>
#include <faceRecognition/face_forest.hpp>
#include <faceRecognition/feature_channel_factory.hpp>
#include <faceRecognition/timing.hpp>

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <onlineIdentification/pointCloudUtils.h>
#include <onlineIdentification/CandidateUsers.h>

#include <sensor_msgs/Image.h>
#include <action_model/IdMessage.h>
#include <opt_msgs/TrackArray.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <std_msgs/Int32.h>
#include <tf/transform_listener.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <string>

#include <pcl_conversions/pcl_conversions.h>

//#include <people_tracking/TrackingResult.h>

class onlineIdFunctions
{
public:
	cv_bridge::CvImagePtr rgb_cv;
	cv_bridge::CvImagePtr depth_cv;
	pcl::PointCloud<pcl::PointXYZ> depthPointcloud;
	pcl::PointCloud<pcl::PointXYZ> skeletonCloud;
	std::vector<Eigen::Quaternion<float> > skeletonQuality;
    bool new_rgb, new_pointcloud, new_tracks, ids_ready, tracker_ready;
	int do_identification;
	std::vector<int> active_ids;
	opt_msgs::TrackArray tracks_;
//	std::vector<people_tracking::Track> tracks;

	/**
	 * \brief Constructor.
	 *
	 * \param[in] coeff1,..,coeff9 Coefficients for RGB intrinsic matrix.
	 */
	onlineIdFunctions(double coeff1, double coeff2, double coeff3, double coeff4,
			double coeff5, double coeff6, double coeff7, double coeff8,
			double coeff9);

	/**
	 * \brief Store ROS Image message in rgb_cv.
	 *
	 * \param[in] msg Ros Image message.
	 */
	void rgbCallback(const sensor_msgs::Image::ConstPtr& msg);

	/**
	 * \brief Read tf in /tf topic.
	 *
	 * \param[in] index action_model identification number for the user.
	 */
	bool readTf(int index);

	/**
	 * \brief Store ROS Image message in rgb_cv.
	 *
	 * \param[in] msg Ros Image message.
	 */
	void idsCallback(const opt_msgs::TrackArray& tracks_msg);

	/**
	 * \brief Store ROS Image message in depth_cv.
	 *
	 * \param[in] msg ROS Image message.
	 */
	void depthCallback(const sensor_msgs::Image::ConstPtr& msg);

	/**
	 * \brief Store point cloud from ROS message.
	 *
	 * \param[in] msg ROS PointCloud2 message.
	 */
	void cloudCallback(const pcl::PCLPointCloud2::ConstPtr& msg);

	/**
	 * \brief Store people_tracking TrackerResult message message in tracks.
	 *
	 * \param[in] msg people_tracking message.
	 */
//	void trackerCallback(const people_tracking::TrackingResult& msg);

	/**
	 * \brief Assign do_identification to 0 when callback is called.
	 *
	 * \param[in] msg Ros Int32 message.
	 */
	void lostCallback(const std_msgs::Int32::ConstPtr& msg);

	/**
	 * \brief Find faces in an image and store them in a vector.
	 *
	 * \param[in] faceForestObject FaceForest object.
	 * \param[in] depth2rgbScaleFactor scale factor for conversion from depth to rgb.
	 * \param[in] faceDetectionFlag always true.
	 * \param[in] rgbImage input Mat.
	 * \param[in] imageToDisplay output Mat where face selection rect is drawn.
	 * \param[in] imageSelectionGray Mat used to work on region of interest.
	 * \param[in] faces vector to save found faces.
	 * \param[in] debugFlag if true debug info are shown.
	 * \param[in] id_number user identification code.
	 * \param[in] counter used to save one face image of the user.
	 * \param[in] trainingFlag true=training false=testing.
	 * \param[in] descriptors_dir directory of descriptors
	 */
	void selectFace(FaceForest& faceForestObject, int depth2rgbScaleFactor,
			bool faceDetectionFlag, cv::Mat& rgbImage, cv::Mat& imageToDisplay,
			cv::Mat& imageSelectionGray, std::vector<Face>& faces,
			bool debugFlag,
			int id_number, int counter, bool trainingFlag, std::string descriptors_dir);

	void selectFaceWithROI(FaceForest &faceForestObject, int depth2rgbScaleFactor,
			bool faceDetectionFlag, cv::Mat &rgbImage,
			cv::Mat &imageToDisplay, cv::Mat &imageSelectionGray,
			std::vector<Face> &faces, bool debugFlag, int id_number,
			int counter, bool trainingFlag,
			std::string descriptors_dir, const cv::Point &topLeftCorner, const cv::Point bottomRightCorner);

	/**
	 * \brief Compute skeleton features given a PointColud.
	 *
	 * \param[in] originalSdkSkeletonJoints input PointCloud.
	 * \param[in] skeletonFeatures Mat where skeleton features are saved.
	 * \param[in] plane_coeffs plane coefficients (already estimated).
	 */
	void computeSkeletonFeatures(
			pcl::PointCloud<pcl::PointXYZ>& originalSdkSkeletonJoints,
			cv::Mat& skeletonFeatures, Eigen::VectorXf& plane_coeffs);

	/**
	 * \brief Compute quality of a Face.
	 *
	 * \param[in] face input face.
	 * \param[in] fiducialPointsQuality Mat where quality coefficients are saved.
	 */
	void computeFaceFiducialPointsQuality(Face& face,
			cv::Mat& fiducialPointsQuality);

	/**
	 * \brief Compute 2D point from 3D point.
	 *
	 * \param[in] x3D,y3D,z3D 3D point coordinates.
	 * \param[in] x2D,y2D 2D point coordinates.
	 */
	bool mapJointsTo2D(float x3D, float y3D, float z3D, float &x2D, float &y2D);

	/**
	 * \brief Draw skeleton joints on a Mat
	 * \param[in] skeletonCloud PointCloud of skeleton.
	 * \param[in] imageToDisplay Mat where points are drawn.
	 */
	void drawSkeletonPoints(pcl::PointCloud<pcl::PointXYZ>& skeletonCloud,
			cv::Mat& imageToDisplay);

	/**
	 * \brief Compute relative orientation of a link with respect to another.
	 *
	 * \param[in] link_index relative orientation to the link with this index.
	 * \param[in] base_link_index relative orientation from the link with this index.
	 * \param[out] computed relative orientation.
	 */
	Eigen::Vector3f computeRelativeOrientation(int link_index,
			int base_link_index,
			std::vector<Eigen::Quaternion<float> >& skeletonsData);

	/**
	 * \brief Compute if the current skeleton is valid.
	 *
	 * \param[out] true=skeleton valid false=skeleton not valid.
	 */
	bool skeletonIsValid();

	/**
	 * \brief Read training descriptors from files.
	 *
	 * \param[in] descriptors_vector Mat where descriptors are saved.
	 * \param[in] users_faces vector where users photos are saved.
	 * \param[in] number_of_users number of trained users.
	 * \param[in] descriptors_dir directory where users descriptors are saved.
	 * \param[in] mode execution mode (face,skeleton,face+skeleton).
	 */
	void readDescriptors(cv::Mat& descriptors_vector,
			std::vector<cv::Mat>& users_faces, int number_of_users,
			std::string descriptors_dir, int mode);

	/**
	 * \brief Read H and b from SVM file.
	 *
	 * \param[in] h Mat where H is saved.
	 * \param[in] b float where b is saved.
	 * \param[in] mode execution mode (face,skeleton,face+skeleton).
	 */
	void read_h_b(cv::Mat& h, float& b, int mode, const std::string &filename);

	/**
	 * \brief Calculate mean and standard deviation of all columns in descriptors Mat.
	 *
	 * \param[in] descriptors Mat where descriptors are found.
	 * \param[in] b float where b is saved.
	 * \param[in] mode execution mode (face,skeleton,face+skeleton).
	 */
	void calculateMeanStdDev(cv::Mat& descriptors, std::vector<float>& med,
			std::vector<float>& dev, int number_of_users);

	/**
	 * \brief Normalize Mat columns given mean and standard deviation vectors.
	 *
	 * \param[in] med vector where means are found.
	 * \param[in] dev vector where standard deviations are found.
	 */
	void normalizeData(cv::Mat& descriptors, std::vector<float>& med,
			std::vector<float>& dev);

	/**
	 * \brief Normalize Mat columns.
	 *
	 * \param[in] current_descriptor current testing descriptor.
	 * \param[in] descripotors Mat with all training descriptors.
	 * \param[in] svmTable Mat where normalized values are saved.
	 * \param[in] med vector where means are found.
	 * \param[in] dev vector where standard deviations are found.
	 * \param[in] number_of_users number of trained users.
	 */
	void normalizeSVM(cv::Mat& current_descriptor, cv::Mat& descriptors,
			cv::Mat& svmTable, std::vector<float>& med, std::vector<float>& dev,
			int number_of_users);

	/**
	 * \brief Find best match in a Mat.
	 *
	 * \param[in] svmTable Mat where normalized values are found.
	 * \param[in] h Mat where H is found.
	 * \param[in] b float where b is found.
	 * \param[in] number_of_users number of trained users.
	 */
	void matchSVM(cv::Mat& svmTable, cv::Mat& h, float& b, int& best_user,
			float& score, int number_of_users);
	/**
	 * \brief Initialize med and dev depending on mode.
	 *
	 * \param[in] med vector to initialize.
	 * \param[in] dev vector to initialize.
	 * \param[in] mode execution mode (face,skeleton,face+skeleton).
	 */
	void fillMedDevWithZero(std::vector<float>& med, std::vector<float>& dev,
			int mode);

	/**
	 * \brief Fill Info window for a certain id.
	 *
	 * \param[in] id id of the considered OpenNI user.
	 * \param[in] user identificator of the trained user considered.
	 * \param[in] index index of the object related to id in the vector.
	 * \param[in] cand_us CandidateUsers data structure.
	 * \param[in] code current code.
	 * \param[in] userface Mat shown in the window.
	 * \param[in] number_of_users number of trained users.
	 * \param[in] cols columns of the window.
	 * \param[in] rows rows of the window.
	 * \param[in] result true=user matched false=still matching.
	 */
	void showMatch(int id, int user, int index, CandidateUsers& cand_us,
			Timing code, cv::Mat& userface, int number_of_users, int cols, int rows,
			bool result);

	/**
	 * \brief Fill "USERS LIST" window.
	 *
	 * \param[in] users_faces Mat vector with users faces.
	 * \param[in] cols columns of the window.
	 * \param[in] rows rows of the window.
	 */
	void usersWindow(std::vector<cv::Mat>& users_faces, int cols, int rows);

	/**
	 * \brief Draw results after a reidentification session.
	 *
	 * \param[in] freq frequency vector.
	 * \param[in] n_users number of trained users.
	 */
	void drawGraph(int freq[], int n_users);

	/**
	 * \brief Read torso tf in /tf topic.
	 *
	 * \param[in] index action_model identification number for the user.
	 * \param[in] XYZPoint object PointXYZ where tf is saved.
	 */
	bool readTfOnlyTorso(int index, pcl::PointXYZ& XYZPoint);

	/**
	 * \briefShow on terminal all computed distances between an OpenNI id and all Tracker ids.
	 *
	 * \param[in] openniId OpenNI id of the user.
	 */
	void printDistances(int& openniId);

	/**
	 * \brief Link OpenNI user id with the nearest people_tracking user id.
	 *
	 * \param[in] openniId OpenNI id of the user.
	 * \param[out] linked people_tracking user id.
	 */
	int linkIds(int& openniId);

	/**
	 * \brief Send Int32 message in topic related to ros::Publisher pub containing the int openniId; assign do_identification=1 if message sent, do_identification=2 if no id was linked
	 *
	 * \param[in]
	 */
	void sendIdMsg(ros::Publisher& pub, int& openniId);

private:
	tf::TransformListener listener;
	double coeff1, coeff2, coeff3, coeff4, coeff5, coeff6, coeff7, coeff8,
			coeff9;
	float x3Dn, y3Dn, z3Dn;
	float x2Dn, y2Dn;
	float x3Dh, y3Dh, z3Dh;
	float x2Dh, y2Dh;

};

#endif /* ONLINE_ID_FUNCTIONS_H_ */
