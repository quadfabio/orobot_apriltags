#ifndef C_U_H_
#define C_U_H_

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <faceRecognition/timing.hpp>

class Cu
{

public:

	/**
	 * \brief Constructor.
	 *
	 * \param[in] id id of the skeleton.
	 * \param[in] code code needed to update matches.
	 * \param[in] number_of_users number of trained users.
	*/
	Cu(int id, Timing code, int number_of_users);

	/**
	 * \brief Set new id.
	 *
	 * \param[in] new_id the new id.
	*/
	void setId(int new_id);

	/**
	 * \brief Set new code.
	 *
	 * \param[in] new_code the new code.
	*/
	void updateCode(Timing new_code);

	/**
	 * \brief Increment number of user matches if code is updated.
	 *
	 * \param[in] new_code the new code.
	 * \param[in] user_matched the index of the user that matches.
	*/
	void updateByCode(Timing new_code, int user_matched);

	/**
	 * \brief Get the id of the user with more matches.
	 *
	 * \param[out] index of the user with more matches.
	*/
	int getBestUser();

	/**
	 * \brief Get the number of matches of the user with more matches.
	 *
	 * \param[out] number of the max matches.
	*/
	int getBestMatch();

	/**
	 * \brief Reset matches vector.
	 *
	 * \param[in] code code needed to update matches.
	*/
	void resetMatches(Timing code);

	std::vector<int> matches;
	int id;
	Timing code;
	int number_of_users;
	int totalmatches;

private:

};

#endif /* C_U_H_ */
