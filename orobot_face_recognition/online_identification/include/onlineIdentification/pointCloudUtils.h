/*
 * pointCloudUtils.h
 *
 *  Created on: Oct 18, 2012
 *      Author: Matteo Munaro
 */

#ifndef POINT_CLOUD_UTILS_H_
#define POINT_CLOUD_UTILS_H_

#include <opencv2/opencv.hpp>
#include <pcl/common/transforms.h>
#include <pcl/visualization/pcl_visualizer.h>

void createPointcloudFromDepthImage(cv::Mat& depthImage, pcl::PointCloud<pcl::PointXYZ>::Ptr& outputPointcloud, Eigen::Matrix3f& depthIntrinsicMatrix, int dimensionStride);

void createPointcloudFromRegisteredDepthImage(cv::Mat& depthImage, cv::Mat& rgbImage, pcl::PointCloud<pcl::PointXYZRGB>::Ptr& outputPointcloud, Eigen::Matrix3f& rgbIntrinsicMatrix, int dimensionStride);

void transformPointcloudWithAffineTransform(pcl::PointCloud<pcl::PointXYZ>::Ptr& XYZ_CloudIn, pcl::PointCloud<pcl::PointXYZ>::Ptr& XYZ_CloudOut, Eigen::Affine3f transform);

void createDepthImageFromPointcloud(pcl::PointCloud<pcl::PointXYZ>::Ptr& XYZ_Cloud, cv::Mat& depthImage, Eigen::Matrix3f intrinsicMatrix, int rows, int cols);

void createDepthImageAndUserMapFromPointcloud(pcl::PointCloud<pcl::PointXYZ>::Ptr& XYZ_Cloud, cv::Mat& depthImage, cv::Mat& userMapIn, cv::Mat& userMapOut, Eigen::Matrix3f intrinsicMatrix);

void depthOverlayOnRGB(cv::Mat& rgbImage, cv::Mat& depthImage, float maxDistance);

void displayPointcloud(pcl::PointCloud<pcl::PointXYZRGB>::Ptr xyzrgbPointcloud, pcl::visualization::PCLVisualizer& viewer,
		bool& pclViewerInitialized, std::string cloudName);

void displayPointcloud(pcl::PointCloud<pcl::PointXYZ>::Ptr xyzPointcloud, pcl::visualization::PCLVisualizer& viewer,
		bool& pclViewerInitialized, std::string cloudName);

void mapSkeletonToRGB(std::vector<float>& skeletonsData, pcl::PointCloud<pcl::PointXYZ>::Ptr& originalSdkSkeletonJoints,
		pcl::PointCloud<pcl::PointXYZ>::Ptr& depthPointcloud, Eigen::Affine3f depthToRGBTransform, Eigen::Matrix3f& rgbIntrinsicMatrix, int depthWidth,
		int NUMBER_OF_TRACKED_SKELETON, int NUMBER_OF_SKELETAL_JOINTS, int NUMBER_OF_JOINT_INFO);

void mapSkeletonToRGBOpenNI(std::vector<float>& skeletonsData, pcl::PointCloud<pcl::PointXYZ>::Ptr& originalSdkSkeletonJoints,
		pcl::PointCloud<pcl::PointXYZ>::Ptr& depthPointcloud, Eigen::Affine3f depthToRGBTransform, Eigen::Matrix3f& rgbIntrinsicMatrix, int depthWidth,
		int NUMBER_OF_TRACKED_SKELETON, int NUMBER_OF_SKELETAL_JOINTS, int NUMBER_OF_JOINT_INFO);

void selectPointsWithGivenIndex(pcl::PointCloud<pcl::PointXYZ>::Ptr& inputPointcloud, cv::Mat& indexMap, unsigned char index, pcl::PointCloud<pcl::PointXYZ>::Ptr outputPointcloud);

void pointDistanceFromSegment(float cx, float cy, float ax, float ay, float bx, float by, float &distanceSegment, float &distanceLine);

void pointDistanceFromSegment(Eigen::Vector3f A, Eigen::Vector3f B, Eigen::Vector3f C, float &distanceSegment, float &distanceLine);

void pointDistanceFromSegment3D(Eigen::Vector3f& p, Eigen::Vector3f& v, Eigen::Vector3f& w, float &distanceSegment, float &distanceLine);

void computeLinksExtremePoints(std::vector<float>& skeletonsData, int skelNumber, int NUMBER_OF_JOINT_INFO,
		int NUMBER_OF_SKELETAL_JOINTS, std::vector<cv::Vec3f>& bodyPartsColors, cv::Mat& extremePoints);

void computeLinksExtremePoints3D(std::vector<float>& skeletonsData, int skelNumber, int NUMBER_OF_JOINT_INFO,
		int NUMBER_OF_SKELETAL_JOINTS, std::vector<cv::Vec3f>& bodyPartsColors, cv::Mat& extremePoints);

void assignLabels(pcl::PointCloud<pcl::PointXYZ>::Ptr& personPointcloud, std::vector<float>& skeletonsData, cv::Mat& extremePoints,
		Eigen::Matrix3f& depthIntrinsicMatrix, pcl::PointCloud<pcl::PointXYZRGB>::Ptr& personPointcloudLabeled,
		cv::Mat& personPointsLabels, std::vector<cv::Vec3f>& bodyPartsColors);

void assignLabels3D(pcl::PointCloud<pcl::PointXYZ>::Ptr& personPointcloud, std::vector<float>& skeletonsData, cv::Mat& extremePoints,
		Eigen::Matrix3f& depthIntrinsicMatrix, pcl::PointCloud<pcl::PointXYZRGB>::Ptr& personPointcloudLabeled,
		cv::Mat& personPointsLabels, std::vector<cv::Vec3f>& bodyPartsColors);

void selectPointsWithGivenLabel(unsigned int queryLabel, pcl::PointCloud<pcl::PointXYZ>::Ptr& inputCloud,
		cv::Mat& labels, pcl::PointCloud<pcl::PointXYZ>::Ptr& outputCloud);

void projectSkeletonToImage(std::vector<float>& skeletonsData, Eigen::Matrix3f& intrinsicMatrix, int NUMBER_OF_TRACKED_SKELETON,
		int NUMBER_OF_JOINT_INFO, int NUMBER_OF_SKELETAL_JOINTS);

void computeSkeletonPointcloud(std::vector<float>& skeletonsData, int NUMBER_OF_TRACKED_SKELETON, int NUMBER_OF_JOINT_INFO, int NUMBER_OF_SKELETAL_JOINTS,
		pcl::PointCloud<pcl::PointXYZ>::Ptr& originalSdkSkeletonJoints);

void createDensePointcloud(pcl::PointCloud<pcl::PointXYZ>::Ptr& inputPointcloud, cv::Mat& image, Eigen::Matrix3f& intrinsicMatrix,
		pcl::PointCloud<pcl::PointXYZ>::Ptr& outputPointcloud);

void fromMillimetersToMeters(pcl::PointCloud<pcl::PointXYZ>::Ptr& inputPointcloud);

void findNearestInImageValidPoint(int x, int y, int width, int radius, pcl::PointCloud<pcl::PointXYZ>::Ptr& inputPointcloud, pcl::PointXYZ& point,
		int& outX, int& outY);

void findNearestInImageAndDirectionValidPoint(int x, int y, int x2, int y2, int width, int radius, pcl::PointCloud<pcl::PointXYZ>::Ptr& inputPointcloud,
		pcl::PointXYZ& point, int& outX, int& outY);

void mapExtremePointsTo3D(cv::Mat& extremePoints, pcl::PointCloud<pcl::PointXYZ>::Ptr& inputPointcloud, int imageWidth, cv::Mat& extremePoints3D);

void zPassThroughFiltering(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, float minValue, float maxValue, pcl::PointCloud<pcl::PointXYZ>::Ptr& cloudFiltered);

#endif /* POINT_CLOUD_UTILS_H_ */
