#include <onlineIdentification/onlineIdFunctions.h>

using namespace std;
using namespace cv;
namespace enc = sensor_msgs::image_encodings;

onlineIdFunctions::onlineIdFunctions(double coeff1, double coeff2,
				     double coeff3, double coeff4, double coeff5, double coeff6, double coeff7,
				     double coeff8, double coeff9)
{
  this->coeff1 = coeff1;
  this->coeff2 = coeff2;
  this->coeff3 = coeff3;
  this->coeff4 = coeff4;
  this->coeff5 = coeff5;
  this->coeff6 = coeff6;
  this->coeff7 = coeff7;
  this->coeff8 = coeff8;
  this->coeff9 = coeff9;
  x3Dn = 0;
  y3Dn = 0;
  z3Dn = 0;
  x2Dn = 0;
  y2Dn = 0;
  x3Dh = 0;
  y3Dh = 0;
  z3Dh = 0;
  x2Dh = 0;
  y2Dh = 0;
  new_rgb = false;
  new_pointcloud = false;
  new_tracks = false;
  ids_ready = false;
  tracker_ready = false;
  do_identification = 0;
}

void onlineIdFunctions::rgbCallback(const sensor_msgs::Image::ConstPtr& msg)
{
  //convert message to CvImagePtr
  try
  {
    rgb_cv = cv_bridge::toCvCopy(msg, enc::BGR8);
    cv::imshow("RGB IMAGE", rgb_cv->image);
    cv::waitKey(1);
    new_rgb = true;

  } catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }
}

bool onlineIdFunctions::readTf(int index)
{
  try
  {
    tf::StampedTransform transform;

    stringstream ss;

    //update skeleton pointCloud from tf datas
    //and evaluate if it is a good skeleton
    /*
                 * 0 torso /skeleton_i_torso
                 * 1 torso /skeleton_i_torso
                 * 2 neck /skeleton_i_neck
                 * 3 head /skeleton_i_head
                 * 4 left shoulder /skeleton_i_left_shoulder
                 * 5 left elbow /skeleton_i_left_elbow
                 * 6 left hand /skeleton_i_left_hand
                 * 7 left hand /skeleton_i_left_hand
                 * 8 right shoulder /skeleton_i_right_shoulder
                 * 9 right elbow /skeleton_i_right_elbow
                 * 10 right hand /skeleton_i_right_hand
                 * 11 right hand /skeleton_i_right_hand
                 * 12 left hip /skeleton_i_left_hip
                 * 13 left knee /skeleton_i_left_knee
                 * 14 left ankle /skeleton_i_left_foot
                 * 15 left ankle /skeleton_i_left_foot
                 * 16 right hip /skeleton_i_right_hip
                 * 17 right knee /skeleton_i_right_knee
                 * 18 right ankle /skeleton_i_right_foot
                 * 19 right ankle /skeleton_i_right_foot
                 */
    skeletonCloud.clear();
    vector<Eigen::Quaternion<float> >().swap(skeletonQuality);

    pcl::PointXYZ XYZpoint;
    Eigen::Quaternion<float> rotation_values;

    ss << "/skeleton_" << index << "_torso";
    listener.lookupTransform("/camera_rgb_optical_frame", ss.str(),
                             ros::Time(0), transform);
    XYZpoint.x = transform.getOrigin().x();
    XYZpoint.y = transform.getOrigin().y();
    XYZpoint.z = transform.getOrigin().z();
    skeletonCloud.points.push_back(XYZpoint); //0
    skeletonCloud.points.push_back(XYZpoint); //1
    rotation_values.w() = transform.getRotation().w();
    rotation_values.x() = transform.getRotation().x();
    rotation_values.y() = transform.getRotation().y();
    rotation_values.z() = transform.getRotation().z();
    skeletonQuality.push_back(rotation_values);
    skeletonQuality.push_back(rotation_values);

    ss.str("");
    ss << "/skeleton_" << index << "_neck";
    listener.lookupTransform("/camera_rgb_optical_frame", ss.str(),
                             ros::Time(0), transform);
    XYZpoint.x = transform.getOrigin().x();
    XYZpoint.y = transform.getOrigin().y();
    XYZpoint.z = transform.getOrigin().z();
    x3Dn = transform.getOrigin().x(); //for face selection
    y3Dn = transform.getOrigin().y(); //for face selection
    z3Dn = transform.getOrigin().z(); //for face selection
    skeletonCloud.points.push_back(XYZpoint); //2
    rotation_values.w() = transform.getRotation().w();
    rotation_values.x() = transform.getRotation().x();
    rotation_values.y() = transform.getRotation().y();
    rotation_values.z() = transform.getRotation().z();
    skeletonQuality.push_back(rotation_values);

    ss.str("");
    ss << "/skeleton_" << index << "_head";
    listener.lookupTransform("/camera_rgb_optical_frame", ss.str(),
                             ros::Time(0), transform);
    XYZpoint.x = transform.getOrigin().x();
    XYZpoint.y = transform.getOrigin().y();
    XYZpoint.z = transform.getOrigin().z();
    x3Dh = transform.getOrigin().x(); //for face selection
    y3Dh = transform.getOrigin().y(); //for face selection
    z3Dh = transform.getOrigin().z(); //for face selection
    skeletonCloud.points.push_back(XYZpoint); //3
    rotation_values.w() = transform.getRotation().w();
    rotation_values.x() = transform.getRotation().x();
    rotation_values.y() = transform.getRotation().y();
    rotation_values.z() = transform.getRotation().z();
    skeletonQuality.push_back(rotation_values);

    ss.str("");
    ss << "/skeleton_" << index << "_left_shoulder";
    listener.lookupTransform("/camera_rgb_optical_frame", ss.str(),
                             ros::Time(0), transform);
    XYZpoint.x = transform.getOrigin().x();
    XYZpoint.y = transform.getOrigin().y();
    XYZpoint.z = transform.getOrigin().z();
    skeletonCloud.points.push_back(XYZpoint); //4
    rotation_values.w() = transform.getRotation().w();
    rotation_values.x() = transform.getRotation().x();
    rotation_values.y() = transform.getRotation().y();
    rotation_values.z() = transform.getRotation().z();
    skeletonQuality.push_back(rotation_values);

    ss.str("");
    ss << "/skeleton_" << index << "_left_elbow";
    listener.lookupTransform("/camera_rgb_optical_frame", ss.str(),
                             ros::Time(0), transform);
    XYZpoint.x = transform.getOrigin().x();
    XYZpoint.y = transform.getOrigin().y();
    XYZpoint.z = transform.getOrigin().z();
    skeletonCloud.points.push_back(XYZpoint); //5
    rotation_values.w() = transform.getRotation().w();
    rotation_values.x() = transform.getRotation().x();
    rotation_values.y() = transform.getRotation().y();
    rotation_values.z() = transform.getRotation().z();
    skeletonQuality.push_back(rotation_values);

    ss.str("");
    ss << "/skeleton_" << index << "_left_hand";
    listener.lookupTransform("/camera_rgb_optical_frame", ss.str(),
                             ros::Time(0), transform);
    XYZpoint.x = transform.getOrigin().x();
    XYZpoint.y = transform.getOrigin().y();
    XYZpoint.z = transform.getOrigin().z();
    skeletonCloud.points.push_back(XYZpoint); //6
    skeletonCloud.points.push_back(XYZpoint); //7
    rotation_values.w() = transform.getRotation().w();
    rotation_values.x() = transform.getRotation().x();
    rotation_values.y() = transform.getRotation().y();
    rotation_values.z() = transform.getRotation().z();
    skeletonQuality.push_back(rotation_values);
    skeletonQuality.push_back(rotation_values);

    ss.str("");
    ss << "/skeleton_" << index << "_right_shoulder";
    listener.lookupTransform("/camera_rgb_optical_frame", ss.str(),
                             ros::Time(0), transform);
    XYZpoint.x = transform.getOrigin().x();
    XYZpoint.y = transform.getOrigin().y();
    XYZpoint.z = transform.getOrigin().z();
    skeletonCloud.points.push_back(XYZpoint); //8
    rotation_values.w() = transform.getRotation().w();
    rotation_values.x() = transform.getRotation().x();
    rotation_values.y() = transform.getRotation().y();
    rotation_values.z() = transform.getRotation().z();
    skeletonQuality.push_back(rotation_values);

    ss.str("");
    ss << "/skeleton_" << index << "_right_elbow";
    listener.lookupTransform("/camera_rgb_optical_frame", ss.str(),
                             ros::Time(0), transform);
    XYZpoint.x = transform.getOrigin().x();
    XYZpoint.y = transform.getOrigin().y();
    XYZpoint.z = transform.getOrigin().z();
    skeletonCloud.points.push_back(XYZpoint); //9
    rotation_values.w() = transform.getRotation().w();
    rotation_values.x() = transform.getRotation().x();
    rotation_values.y() = transform.getRotation().y();
    rotation_values.z() = transform.getRotation().z();
    skeletonQuality.push_back(rotation_values);

    ss.str("");
    ss << "/skeleton_" << index << "_right_hand";
    listener.lookupTransform("/camera_rgb_optical_frame", ss.str(),
                             ros::Time(0), transform);
    XYZpoint.x = transform.getOrigin().x();
    XYZpoint.y = transform.getOrigin().y();
    XYZpoint.z = transform.getOrigin().z();
    skeletonCloud.points.push_back(XYZpoint); //10
    skeletonCloud.points.push_back(XYZpoint); //11
    rotation_values.w() = transform.getRotation().w();
    rotation_values.x() = transform.getRotation().x();
    rotation_values.y() = transform.getRotation().y();
    rotation_values.z() = transform.getRotation().z();
    skeletonQuality.push_back(rotation_values);
    skeletonQuality.push_back(rotation_values);

    ss.str("");
    ss << "/skeleton_" << index << "_left_hip";
    listener.lookupTransform("/camera_rgb_optical_frame", ss.str(),
                             ros::Time(0), transform);
    XYZpoint.x = transform.getOrigin().x();
    XYZpoint.y = transform.getOrigin().y();
    XYZpoint.z = transform.getOrigin().z();
    skeletonCloud.points.push_back(XYZpoint); //12
    rotation_values.w() = transform.getRotation().w();
    rotation_values.x() = transform.getRotation().x();
    rotation_values.y() = transform.getRotation().y();
    rotation_values.z() = transform.getRotation().z();
    skeletonQuality.push_back(rotation_values);

    ss.str("");
    ss << "/skeleton_" << index << "_left_knee";
    listener.lookupTransform("/camera_rgb_optical_frame", ss.str(),
                             ros::Time(0), transform);
    XYZpoint.x = transform.getOrigin().x();
    XYZpoint.y = transform.getOrigin().y();
    XYZpoint.z = transform.getOrigin().z();
    skeletonCloud.points.push_back(XYZpoint); //13
    rotation_values.w() = transform.getRotation().w();
    rotation_values.x() = transform.getRotation().x();
    rotation_values.y() = transform.getRotation().y();
    rotation_values.z() = transform.getRotation().z();
    skeletonQuality.push_back(rotation_values);

    ss.str("");
    ss << "/skeleton_" << index << "_left_foot";
    listener.lookupTransform("/camera_rgb_optical_frame", ss.str(),
                             ros::Time(0), transform);
    XYZpoint.x = transform.getOrigin().x();
    XYZpoint.y = transform.getOrigin().y();
    XYZpoint.z = transform.getOrigin().z();
    skeletonCloud.points.push_back(XYZpoint); //14
    skeletonCloud.points.push_back(XYZpoint); //15
    rotation_values.w() = transform.getRotation().w();
    rotation_values.x() = transform.getRotation().x();
    rotation_values.y() = transform.getRotation().y();
    rotation_values.z() = transform.getRotation().z();
    skeletonQuality.push_back(rotation_values);
    skeletonQuality.push_back(rotation_values);

    ss.str("");
    ss << "/skeleton_" << index << "_right_hip";
    listener.lookupTransform("/camera_rgb_optical_frame", ss.str(),
                             ros::Time(0), transform);
    XYZpoint.x = transform.getOrigin().x();
    XYZpoint.y = transform.getOrigin().y();
    XYZpoint.z = transform.getOrigin().z();
    skeletonCloud.points.push_back(XYZpoint); //16
    rotation_values.w() = transform.getRotation().w();
    rotation_values.x() = transform.getRotation().x();
    rotation_values.y() = transform.getRotation().y();
    rotation_values.z() = transform.getRotation().z();
    skeletonQuality.push_back(rotation_values);

    ss.str("");
    ss << "/skeleton_" << index << "_right_knee";
    listener.lookupTransform("/camera_rgb_optical_frame", ss.str(),
                             ros::Time(0), transform);
    XYZpoint.x = transform.getOrigin().x();
    XYZpoint.y = transform.getOrigin().y();
    XYZpoint.z = transform.getOrigin().z();
    skeletonCloud.points.push_back(XYZpoint); //17
    rotation_values.w() = transform.getRotation().w();
    rotation_values.x() = transform.getRotation().x();
    rotation_values.y() = transform.getRotation().y();
    rotation_values.z() = transform.getRotation().z();
    skeletonQuality.push_back(rotation_values);

    ss.str("");
    ss << "/skeleton_" << index << "_right_foot";
    listener.lookupTransform("/camera_rgb_optical_frame", ss.str(),
                             ros::Time(0), transform);
    XYZpoint.x = transform.getOrigin().x();
    XYZpoint.y = transform.getOrigin().y();
    XYZpoint.z = transform.getOrigin().z();
    skeletonCloud.points.push_back(XYZpoint); //18
    skeletonCloud.points.push_back(XYZpoint); //19
    rotation_values.w() = transform.getRotation().w();
    rotation_values.x() = transform.getRotation().x();
    rotation_values.y() = transform.getRotation().y();
    rotation_values.z() = transform.getRotation().z();
    skeletonQuality.push_back(rotation_values);
    skeletonQuality.push_back(rotation_values);

    //get points of neck and head in 2D for face selection
    mapJointsTo2D(x3Dn, y3Dn, z3Dn, x2Dn, y2Dn);
    mapJointsTo2D(x3Dh, y3Dh, z3Dh, x2Dh, y2Dh);

    return true;

  } catch (tf::TransformException& ex)
  {
    //ROS_ERROR("Waiting for tf topics to be published.");
    return false;
  }
}

void onlineIdFunctions::idsCallback(const opt_msgs::TrackArray& tracks_msg)
{
  tracks_ = tracks_msg;
  active_ids.resize(tracks_.tracks.size());
  for(int i = 0; i < tracks_.tracks.size(); ++i)
    active_ids[0] = tracks_.tracks[0].id;
  ids_ready = true;
}

void onlineIdFunctions::depthCallback(const sensor_msgs::Image::ConstPtr& msg)
{
  //convert message to CvImagePtr
  try
  {
    depth_cv = cv_bridge::toCvCopy(msg, enc::TYPE_8UC1);

  } catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }

  //cv::imshow("DEPTH IMAGE", depth_cv->image);
  //cv::waitKey(1);
}

void onlineIdFunctions::cloudCallback(const pcl::PCLPointCloud2::ConstPtr& msg)
{
  //convert message to PointCloud<T>
  pcl::fromPCLPointCloud2(*msg, depthPointcloud);
  new_pointcloud = true;
}

void onlineIdFunctions::selectFace(FaceForest& faceForestObject, int depth2rgbScaleFactor,
				   bool faceDetectionFlag, cv::Mat& rgbImage, cv::Mat& imageToDisplay,
				   cv::Mat& imageSelectionGray, std::vector<Face>& faces, bool debugFlag,
				   int id_number, int counter, bool trainingFlag, std::string descriptors_dir)
{
  //shoulder center is equal to the neck point
  cv::Point shoulderCenter(x2Dn, y2Dn);
  //head is the head point
  cv::Point head(x2Dh, y2Dh);
  //head center is between head and neck joint, but nearer to head:
  cv::Point headCenter((shoulderCenter.x + 2 * head.x) / 3,
                       (shoulderCenter.y + 2 * head.y) / 3);
  //evaluate head dimension (to decide the box dimensions for face recognition)
  complex<float> pointsDifference(shoulderCenter.x - head.x,
                                  shoulderCenter.y - head.y);
  float headDimension = std::abs(pointsDifference);
  // Select box around head && inside the rgb image:
  cv::Point topLeftCorner(std::max((float) 0, headCenter.x - headDimension),
                          std::max((float) 0, headCenter.y - headDimension));
  cv::Point bottomRightCorner(
        std::min((float) rgbImage.cols - 1, headCenter.x + headDimension),
        std::min((float) rgbImage.rows - 1, headCenter.y + headDimension));
  int roiWidth = bottomRightCorner.x - topLeftCorner.x + 1;
  int roiHeight = bottomRightCorner.y - topLeftCorner.y + 1;
  //check if roi is inside the image
  if (topLeftCorner.x >= 0 && topLeftCorner.x < rgbImage.cols
      && bottomRightCorner.x >= 0 && bottomRightCorner.x < rgbImage.cols
      && topLeftCorner.y >= 0 && topLeftCorner.y < rgbImage.rows
      && bottomRightCorner.y >= 0 && bottomRightCorner.y < rgbImage.rows)
  {

    cv::rectangle(imageToDisplay, topLeftCorner, bottomRightCorner,
                  cv::Scalar(255, 0, 0), 3);

    //Select region of interest
    cv::Mat roi(rgbImage,
                cv::Rect(topLeftCorner.x, topLeftCorner.y, roiWidth, roiHeight));

    cv::Mat faceToSave = roi.clone();

    // Convert region of interest to grayscale
    cvtColor(roi, imageSelectionGray, CV_BGR2GRAY);
    // Face detection inside the image selection:

    if (faceDetectionFlag)
    {
      faceForestObject.analize_image(imageSelectionGray, faces);
    }
    else
    {
      Face face;
      faceForestObject.analize_face(imageSelectionGray,
                                    cv::Rect(0, 0, imageSelectionGray.cols, imageSelectionGray.rows),
                                    face);
      faces.push_back(face);
    }

    if (faces.size() > 1)		// If more than one face is detected
    {	// choose the biggest one
      int biggestWidth = 0;
      int biggestIndex = 0;
      for (uint i = 0; i < faces.size(); i++)
      {
        if (faces[i].bbox.width > biggestWidth)
        {
          biggestWidth = faces[i].bbox.width;
          biggestIndex = i;
        }
      }
      faces[0] = faces[biggestIndex];// put the selected face at the first position
    }

    if (faces.size() > 0)		// If at least one face is detected:
    {
      // Display results:
      // display output face detector:
      cv::rectangle(imageToDisplay,
                    cv::Point(faces[0].bbox.x + topLeftCorner.x,
                    faces[0].bbox.y + topLeftCorner.y),
          cv::Point(
            faces[0].bbox.x + faces[0].bbox.width + topLeftCorner.x,
          faces[0].bbox.y + faces[0].bbox.height + topLeftCorner.y),
          cv::Scalar(0, 0, 255), 3);

      if (debugFlag)
      {
        // display output facial feature detection:
        roi = cv::Mat(rgbImage,
                      cv::Rect(topLeftCorner.x, topLeftCorner.y, roiWidth,
                               roiHeight));
        FaceForest::show_results(roi, faces, "", 1, false);
      }

      if (counter == 0 && trainingFlag)
      {
        //save face for identification
        stringstream ss;
        ss << descriptors_dir << "face_" << id_number << ".jpg";
        cv::imwrite(ss.str(), faceToSave);
      }
    }
  }
  else
  {
    ROS_ERROR("Roi out of image");
  }
}

void onlineIdFunctions::selectFaceWithROI(FaceForest& faceForestObject, int depth2rgbScaleFactor,
                                                 bool faceDetectionFlag, cv::Mat& rgbImage, cv::Mat& imageToDisplay,
                                                 cv::Mat& imageSelectionGray, std::vector<Face>& faces, bool debugFlag,
                                                 int id_number, int counter, bool trainingFlag, std::string descriptors_dir,
                                                 const cv::Point& topLeftCorner, const cv::Point bottomRightCorner)
{
  int roiWidth = bottomRightCorner.x - topLeftCorner.x + 1;
  int roiHeight = bottomRightCorner.y - topLeftCorner.y + 1;
  //check if roi is inside the image
  if (topLeftCorner.x >= 0 && topLeftCorner.x < rgbImage.cols
      && bottomRightCorner.x >= 0 && bottomRightCorner.x < rgbImage.cols
      && topLeftCorner.y >= 0 && topLeftCorner.y < rgbImage.rows
      && bottomRightCorner.y >= 0 && bottomRightCorner.y < rgbImage.rows)
  {

    cv::rectangle(imageToDisplay, topLeftCorner, bottomRightCorner,
                  cv::Scalar(255, 0, 0), 3);

    //Select region of interest
    cv::Mat roi(rgbImage,
                cv::Rect(topLeftCorner.x, topLeftCorner.y, roiWidth, roiHeight));

    // Convert region of interest to grayscale
    cvtColor(roi, imageSelectionGray, CV_BGR2GRAY);
    // Face detection inside the image selection:

    if (faceDetectionFlag)
    {
      faceForestObject.analize_image(imageSelectionGray, faces);
    }
    else
    {
      Face face;
      faceForestObject.analize_face(imageSelectionGray,
                                    cv::Rect(0, 0, imageSelectionGray.cols, imageSelectionGray.rows),
                                    face);
      faces.push_back(face);
    }

    if (faces.size() > 1)		// If more than one face is detected
    {	// choose the biggest one
      int biggestWidth = 0;
      int biggestIndex = 0;
      for (uint i = 0; i < faces.size(); i++)
      {
        if (faces[i].bbox.width > biggestWidth)
        {
          biggestWidth = faces[i].bbox.width;
          biggestIndex = i;
        }
      }
      faces[0] = faces[biggestIndex];// put the selected face at the first position
    }

    if (faces.size() > 0)		// If at least one face is detected:
    {
      // Display results:
      // display output face detector:
      cv::rectangle(imageToDisplay,
                    cv::Point(faces[0].bbox.x + topLeftCorner.x,
                    faces[0].bbox.y + topLeftCorner.y),
          cv::Point(
            faces[0].bbox.x + faces[0].bbox.width + topLeftCorner.x,
          faces[0].bbox.y + faces[0].bbox.height + topLeftCorner.y),
          cv::Scalar(0, 0, 255), 3);
      if (debugFlag)
      {
        // display output facial feature detection:
        roi = cv::Mat (rgbImage,
                      cv::Rect(topLeftCorner.x, topLeftCorner.y, roiWidth,
                               roiHeight));
        FaceForest::show_results(roi, faces, "", 1, false);
      }

      if (trainingFlag)
      {
        //save face for identification
        stringstream ss;
        ss << descriptors_dir << "face_" << id_number << "_" << counter << ".jpg";
        cv::imwrite(ss.str(), roi(faces[0].bbox));
      }
    }
    else
    {
      //ROS_WARN("No face detected");
    }
  }
  else
  {
    ROS_ERROR("Roi out of image");
  }
}


void onlineIdFunctions::computeSkeletonFeatures(
    pcl::PointCloud<pcl::PointXYZ>& originalSdkSkeletonJoints,
    cv::Mat& skeletonFeatures, Eigen::VectorXf& plane_coeffs)
{

  float sqrtGroundCoeffs = (plane_coeffs
                            - Eigen::Vector4f(0.0f, 0.0f, 0.0f, plane_coeffs(3))).norm();

  // distance head-floor (3-floor)
  Eigen::Vector4f headPoint(originalSdkSkeletonJoints.points[3].x,
      originalSdkSkeletonJoints.points[3].y,
      originalSdkSkeletonJoints.points[3].z, 1);
  skeletonFeatures.at<float>(0, 0) = std::fabs(headPoint.dot(plane_coeffs));
  skeletonFeatures.at<float>(0, 0) /= sqrtGroundCoeffs;

  // distance neck-floor (2-floor)
  Eigen::Vector4f neckPoint(originalSdkSkeletonJoints.points[2].x,
      originalSdkSkeletonJoints.points[2].y,
      originalSdkSkeletonJoints.points[2].z, 1);
  skeletonFeatures.at<float>(1, 0) = std::fabs(neckPoint.dot(plane_coeffs));
  skeletonFeatures.at<float>(1, 0) /= sqrtGroundCoeffs;

  // distance neck-left shoulder (2-4)
  Eigen::Vector4f leftShoulderPoint(originalSdkSkeletonJoints.points[4].x,
      originalSdkSkeletonJoints.points[4].y,
      originalSdkSkeletonJoints.points[4].z, 1);
  skeletonFeatures.at<float>(2, 0) = (neckPoint - leftShoulderPoint).norm();

  // distance neck-right shoulder (2-8)
  Eigen::Vector4f rightShoulderPoint(originalSdkSkeletonJoints.points[8].x,
      originalSdkSkeletonJoints.points[8].y,
      originalSdkSkeletonJoints.points[8].z, 1);
  skeletonFeatures.at<float>(3, 0) = (neckPoint - rightShoulderPoint).norm();

  // distance torso center - right shoulder (1-8)
  Eigen::Vector4f torsoPoint(originalSdkSkeletonJoints.points[1].x,
      originalSdkSkeletonJoints.points[1].y,
      originalSdkSkeletonJoints.points[1].z, 1);
  skeletonFeatures.at<float>(4, 0) = (torsoPoint - rightShoulderPoint).norm();

  // right arm length (8-9 + 9-10)
  Eigen::Vector4f rightElbowPoint(originalSdkSkeletonJoints.points[9].x,
      originalSdkSkeletonJoints.points[9].y,
      originalSdkSkeletonJoints.points[9].z, 1);
  Eigen::Vector4f rightWristPoint(originalSdkSkeletonJoints.points[10].x,
      originalSdkSkeletonJoints.points[10].y,
      originalSdkSkeletonJoints.points[10].z, 1);
  skeletonFeatures.at<float>(5, 0) =
      (rightShoulderPoint - rightElbowPoint).norm()
      + (rightWristPoint - rightElbowPoint).norm();

  // left arm length (4-5 + 5-6)
  Eigen::Vector4f leftElbowPoint(originalSdkSkeletonJoints.points[5].x,
      originalSdkSkeletonJoints.points[5].y,
      originalSdkSkeletonJoints.points[5].z, 1);
  Eigen::Vector4f leftWristPoint(originalSdkSkeletonJoints.points[6].x,
      originalSdkSkeletonJoints.points[6].y,
      originalSdkSkeletonJoints.points[6].z, 1);
  skeletonFeatures.at<float>(6, 0) =
      (leftShoulderPoint - leftElbowPoint).norm()
      + (leftWristPoint - leftElbowPoint).norm();

  // right leg length (16-17)
  Eigen::Vector4f rightHipPoint(originalSdkSkeletonJoints.points[16].x,
      originalSdkSkeletonJoints.points[16].y,
      originalSdkSkeletonJoints.points[16].z, 1);
  Eigen::Vector4f rightKneePoint(originalSdkSkeletonJoints.points[17].x,
      originalSdkSkeletonJoints.points[17].y,
      originalSdkSkeletonJoints.points[17].z, 1);
  skeletonFeatures.at<float>(7, 0) = (rightHipPoint - rightKneePoint).norm();

  // left leg length (12-13)
  Eigen::Vector4f leftHipPoint(originalSdkSkeletonJoints.points[12].x,
      originalSdkSkeletonJoints.points[12].y,
      originalSdkSkeletonJoints.points[12].z, 1);
  Eigen::Vector4f leftKneePoint(originalSdkSkeletonJoints.points[13].x,
      originalSdkSkeletonJoints.points[13].y,
      originalSdkSkeletonJoints.points[13].z, 1);
  skeletonFeatures.at<float>(8, 0) = (leftHipPoint - leftKneePoint).norm();

  // torso length	(0-2)
  Eigen::Vector4f hipCenterPoint(originalSdkSkeletonJoints.points[0].x,
      originalSdkSkeletonJoints.points[0].y,
      originalSdkSkeletonJoints.points[0].z, 1);
  skeletonFeatures.at<float>(9, 0) = (hipCenterPoint - neckPoint).norm();

  // hips distance (16-0 + 12-0)
  skeletonFeatures.at<float>(10, 0) = (leftHipPoint - hipCenterPoint).norm()
      + (rightHipPoint - hipCenterPoint).norm();

  // ratio torso/right leg
  skeletonFeatures.at<float>(11, 0) = skeletonFeatures.at<float>(9, 0)
      / skeletonFeatures.at<float>(7, 0);

  // ratio torso/left leg
  skeletonFeatures.at<float>(12, 0) = skeletonFeatures.at<float>(9, 0)
      / skeletonFeatures.at<float>(8, 0);
}

void onlineIdFunctions::computeFaceFiducialPointsQuality(Face& face,
                                                         cv::Mat& fiducialPointsQuality)
{
  // compute binary vector stating if the facial fiducial points are reliable or not
  fiducialPointsQuality.zeros(fiducialPointsQuality.rows,
                              fiducialPointsQuality.cols, CV_32F);
  if ((face.ffd_cordinates[0].x < face.ffd_cordinates[1].x)
      && (face.ffd_cordinates[0].y < face.ffd_cordinates[8].y))
    fiducialPointsQuality.at<float>(0, 0) = 1;
  else
    fiducialPointsQuality.at<float>(0, 0) = 0;
  if ((face.ffd_cordinates[1].x < face.ffd_cordinates[6].x)
      && (face.ffd_cordinates[1].y < face.ffd_cordinates[8].y))
    fiducialPointsQuality.at<float>(1, 0) = 1;
  else
    fiducialPointsQuality.at<float>(1, 0) = 0;
  if ((face.ffd_cordinates[2].x < face.ffd_cordinates[5].x)
      && (face.ffd_cordinates[2].y > face.ffd_cordinates[5].y))
    fiducialPointsQuality.at<float>(2, 0) = 1;
  else
    fiducialPointsQuality.at<float>(2, 0) = 0;
  if ((face.ffd_cordinates[3].x > face.ffd_cordinates[5].x)
      && (face.ffd_cordinates[3].y > face.ffd_cordinates[9].y))
    fiducialPointsQuality.at<float>(3, 0) = 1;
  else
    fiducialPointsQuality.at<float>(3, 0) = 0;
  if ((face.ffd_cordinates[4].y > face.ffd_cordinates[5].y)
      && (face.ffd_cordinates[4].y > face.ffd_cordinates[2].y))
    fiducialPointsQuality.at<float>(4, 0) = 1;
  else
    fiducialPointsQuality.at<float>(4, 0) = 0;
  if ((face.ffd_cordinates[5].y > face.ffd_cordinates[8].y)
      && (face.ffd_cordinates[5].y < face.ffd_cordinates[2].y))
    fiducialPointsQuality.at<float>(5, 0) = 1;
  else
    fiducialPointsQuality.at<float>(5, 0) = 0;
  if ((face.ffd_cordinates[6].x < face.ffd_cordinates[7].x)
      && (face.ffd_cordinates[6].y < face.ffd_cordinates[9].y))
    fiducialPointsQuality.at<float>(6, 0) = 1;
  else
    fiducialPointsQuality.at<float>(6, 0) = 0;
  if ((face.ffd_cordinates[7].x > face.ffd_cordinates[6].x)
      && (face.ffd_cordinates[7].y < face.ffd_cordinates[9].y))
    fiducialPointsQuality.at<float>(7, 0) = 1;
  else
    fiducialPointsQuality.at<float>(7, 0) = 0;
  if ((face.ffd_cordinates[8].x < face.ffd_cordinates[9].x)
      && (face.ffd_cordinates[8].y > face.ffd_cordinates[1].y))
    fiducialPointsQuality.at<float>(8, 0) = 1;
  else
    fiducialPointsQuality.at<float>(8, 0) = 0;
  if ((face.ffd_cordinates[9].x > face.ffd_cordinates[8].x)
      && (face.ffd_cordinates[9].y < face.ffd_cordinates[5].y))
    fiducialPointsQuality.at<float>(9, 0) = 1;
  else
    fiducialPointsQuality.at<float>(9, 0) = 0;
}

bool onlineIdFunctions::mapJointsTo2D(float x3D, float y3D, float z3D,
                                      float &x2D, float &y2D)
{
  Eigen::Matrix3f intrinsicMatrix;
  intrinsicMatrix << coeff1, coeff2, coeff3, coeff4, coeff5, coeff6, coeff7, coeff8, coeff9; //RGB intrinsic matrix

  // Compute projection matrix:
  Eigen::MatrixXf projectionMatrix;
  Eigen::MatrixXf m(3, 4);
  m << 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0;
  projectionMatrix = intrinsicMatrix * m;
  Eigen::Vector4f homogeneousPoint;
  Eigen::Vector3f projectedPoint;
  homogeneousPoint << x3D, y3D, z3D, 1;
  projectedPoint = projectionMatrix * homogeneousPoint;
  projectedPoint /= projectedPoint(2);

  x2D = projectedPoint(0);
  y2D = projectedPoint(1);
  if (projectedPoint(0) != projectedPoint(0))
  {
    return false;
  }
  return true;
}

void onlineIdFunctions::drawSkeletonPoints(
    pcl::PointCloud<pcl::PointXYZ>& skeletonCloud, cv::Mat& imageToDisplay)
{
  for (uint i = 0; i < skeletonCloud.size(); i++)
  {
    float x;
    float y;
    mapJointsTo2D(skeletonCloud.points[i].x, skeletonCloud.points[i].y,
                  skeletonCloud.points[i].z, x, y);
    if (x >= 0 && x < imageToDisplay.cols && y >= 0
        && y < imageToDisplay.rows)
    {
      Point ptn = Point(x, y);
      circle(imageToDisplay, ptn, 6, Scalar(0, 0, 255), -1, 8);
    }

  }
}

Eigen::Vector3f onlineIdFunctions::computeRelativeOrientation(int link_index,
							      int base_link_index,
							      std::vector<Eigen::Quaternion<float> >& skeletonsData)
{
  // Returns relative orientation (in terms of Euler angles (x,y,z) in degrees) of a link with respect to another
  Eigen::Vector3f euler_angles;
  Eigen::Quaternion<float> base_link_orientation;
  Eigen::Quaternion<float> link_orientation = skeletonsData[link_index];

  if (base_link_index >= 0)
  {
    base_link_orientation = skeletonsData[base_link_index];
  }
  else
  {
    base_link_orientation = Eigen::Quaternion<float>(1, 0, 0, 0);
  }

  // Compute relative orientation:
  Eigen::Quaternion<float> link_relative_orientation = link_orientation
      * base_link_orientation.inverse();
  link_relative_orientation.normalize();
  euler_angles = (link_relative_orientation.toRotationMatrix().eulerAngles(0,
                                                                           1, 2)) * 180 / M_PI; // convert to euler angles in degrees

  return (euler_angles);
}

bool onlineIdFunctions::skeletonIsValid()
{
  Eigen::Vector3f euler_angles_hip = computeRelativeOrientation(0, -1,
                                                                skeletonQuality); //hip?torso absolute orientation
  Eigen::Vector3f euler_angles_upper_left_leg_rel_hip =
      computeRelativeOrientation(13, 0, skeletonQuality); // up_left_leg orientation wrt hip?torso
  Eigen::Vector3f euler_angles_upper_right_leg_rel_hip =
      computeRelativeOrientation(17, 0, skeletonQuality); // up_right_leg orientation wrt hip?torso
  Eigen::Vector3f euler_angles_lower_left_leg_rel_hip =
      computeRelativeOrientation(14, 0, skeletonQuality); // low_left_leg orientation wrt hip?torso
  Eigen::Vector3f euler_angles_lower_right_leg_rel_hip =
      computeRelativeOrientation(18, 0, skeletonQuality); // low_right_leg orientation wrt hip?torso
  Eigen::Vector3f euler_angles_upper_left_leg_rel_upper_right_leg =
      computeRelativeOrientation(13, 17, skeletonQuality); // up_left_leg orientation wrt up_right_leg
  return (std::fabs(euler_angles_hip(0)) < 90)
      && (std::fabs(euler_angles_upper_left_leg_rel_hip(0)) < 90)
      && (std::fabs(euler_angles_upper_right_leg_rel_hip(0)) < 90)
      && (std::fabs(euler_angles_lower_left_leg_rel_hip(0)) < 90)
      && (std::fabs(euler_angles_lower_right_leg_rel_hip(0)) < 90)
      && (euler_angles_upper_left_leg_rel_upper_right_leg(1) < 60)
      && (euler_angles_upper_left_leg_rel_upper_right_leg(1) > -30)
      && (std::fabs(euler_angles_upper_left_leg_rel_upper_right_leg(2)) < 30);
}

void onlineIdFunctions::readDescriptors(cv::Mat& descriptors_vector,
					vector<cv::Mat>& users_faces, int number_of_users, string descriptors_dir,
					int mode)
{
  //read descriptors, user i has face files "i_0.yml"..."i_29.yml" and file "face_i.jpg"
  //and skeleton files "is_0.yml"..."is_29.yml".

  if (mode == 0)
  { //only face is used
    for (int user = 1; user <= number_of_users; user++)
    {
      for (uint j = 0; j <= 29; j++)
      {
        Mat temp;
        stringstream ss;
        ss << descriptors_dir << user << "_" << j << ".yml";
        FileStorage fs(ss.str(), FileStorage::READ);
        fs["faceDescriptor"] >> temp;
        temp = temp.reshape(1, 1);
        descriptors_vector.push_back(temp);
      }
      //face image
      stringstream ss;
      ss << descriptors_dir << "face_" << user << "_0.jpg";
      Mat temp = imread(ss.str());
      users_faces.push_back(temp);
    }
  }
}

void onlineIdFunctions::read_h_b(Mat& h, float& b, int mode,
                                 const std::string& filename)
{

  if (mode == 0)
  { //if only face
    h = Mat(1, 640, CV_32FC1);
    std::ifstream infile(filename.c_str());
    int i = 0;
    float val = 0;
    infile >> b;
    while (infile >> val)
    {
      h.at<float>(0, i) = val;
      i++;
    }
//    std::string line;
//    std::getline(infile, line);
//    std::istringstream iss(line);
//    iss >> b;
//    while (std::getline(infile, line))
//    {
//        std::cout << line << std::endl;
//        std::istringstream iss(line);
////        int a, b;
////        if (!(iss >> a >> b)) { break; } // error

//        // process pair (a,b)
//    }
  }
  if (mode == 1)
  { //if only skeleton
    h = Mat(1, 13, CV_32FC1);
    ifstream infile("data/Generic_SVM_skel");
    int i = 0;
    float val = 0;
    infile >> b;
    while (infile >> val)
    {
      h.at<float>(0, i) = val;
      i++;
    }
  }
  if (mode == 2)
  {
    //i didn't have a trained svm for face+skeleton
  }
}

void onlineIdFunctions::calculateMeanStdDev(Mat& descriptors,
                                            vector<float>& med, vector<float>& dev, int number_of_users)
{
  float s = number_of_users * 30; //or descriptors.rows
  //FEATURES MEAN
  for (int row = 0; row < descriptors.rows; row++)
  {
    //for each feature
    for (int feat = 0; feat < descriptors.cols; feat++)
    {
      med[feat] = med[feat] + (float) (descriptors.at<float>(row, feat) / s);
    }
  }
  //STD DEV
  for (int row = 0; row < descriptors.rows; row++)
  {
    for (int feat = 0; feat < descriptors.cols; feat++)
    {
      dev[feat] = dev[feat]
          + (float) pow(
            (float) (descriptors.at<float>(row, feat) - med[feat]), 2);
    }
  }
  for (uint feat = 0; feat < dev.size(); feat++)
  {
    dev[feat] = sqrt((float) (dev[feat] / s));
  }
}

void onlineIdFunctions::normalizeData(Mat& descriptors, vector<float>& med,
                                      vector<float>& dev)
{
  for (int i = 0; i < descriptors.rows; i++)
  {
    for (int j = 0; j < descriptors.cols; j++)
    {
      descriptors.at<float>(i, j) = (descriptors.at<float>(i, j) - med[j])
          / dev[j];
    }
  }
}

void onlineIdFunctions::normalizeSVM(Mat& current_descriptor, Mat& descriptors,
				     Mat& svmTable, vector<float>& med, vector<float>& dev,
				     int number_of_users)
{
  //svmTable[i][j] = | current_descriptor[j] - descriptors[i][j] |
  for (int i = 0; i < descriptors.rows; i++)
  {
    for (int j = 0; j < descriptors.cols; j++)
    {
      svmTable.at<float>(i, j) = abs(
            (float) (current_descriptor.at<float>(0, j)
                     - descriptors.at<float>(i, j)));
    }
  }
  //mean and standard deviation of svmTable
  calculateMeanStdDev(svmTable, med, dev, number_of_users);
  //svmTable[i][j] = (svmTable[i][j] - med[j]) / dev[j]
  normalizeData(svmTable, med, dev);
}

void onlineIdFunctions::matchSVM(Mat& svmTable, Mat& h, float& b,
                                 int& best_user, float& score, int number_of_users)
{
  best_user = -1;
  score = -1;
  // method 1: best "single score" selection
  /*
         for (int i = 0; i < svmTable.rows; i++)
         {
         //score = h ^ svmTable.row(i) + b
         float temp = h.dot(svmTable.row(i)) + b;
         //if higher score update max
         if (temp > score)
         {
         score = temp;
         best_user = (int) (i / 30);
         }
         }
         */
  // method 2: best "sum of all scores of a user" selection
  vector<float> temp(number_of_users, 0);
  for (int user = 0; user < number_of_users; user++)
  { //svmTable has 30*n_users rows; every user has 30 descriptors
    for (int i = user * 30; i <= user * 30 + 29; i++)
    {
      //temp[user] = h ^ svmTable.row(i) + b
      temp[user] = temp[user] + (float) (h.dot(svmTable.row(i))) + b;
    }
  }
  for (int user = 0; user < number_of_users; user++)
  {
    //cout << temp[user] << " ";
    if (temp[user] > score)
    {
      score = temp[user];
      best_user = user;
    }
  }
  //cout << endl;
}

void onlineIdFunctions::fillMedDevWithZero(vector<float>& med,
                                           vector<float>& dev, int mode)
{
  //reset med dev
  vector<float>().swap(med);
  vector<float>().swap(dev);

  if (mode == 0)
  { // only face
    for (int i = 0; i < 640; i++)
    {
      med.push_back(0.0);
      dev.push_back(0.0);
    }
  }
  if (mode == 1)
  { // only skeleton
    for (int i = 0; i < 13; i++)
    {
      med.push_back(0.0);
      dev.push_back(0.0);
    }
  }
  if (mode == 2)
  { // face+skeleton
    for (int i = 0; i < 653; i++)
    {
      med.push_back(0.0);
      dev.push_back(0.0);
    }
  }
}

void onlineIdFunctions::showMatch(int id, int user, int index,
				  CandidateUsers& cand_us, Timing code, cv::Mat& userface, int number_of_users,
				  int cols, int rows, bool result)
{

  Mat matchUser;
  resize(userface, matchUser, Size(150, 150));

  cv::Mat all(rows, cols, CV_8UC3);
  all.setTo(cv::Scalar(213, 214, 213));
  cv::Rect roi(cv::Point((int) ((cols - 150) / 2), rows - 165),
               matchUser.size());

  cv::line(all, Point(3, 3), Point(3, rows - 2), Scalar(0, 0, 0), 1, 8);
  cv::line(all, Point(cols - 3, 3), Point(cols - 3, rows - 2), Scalar(0, 0, 0),
           1, 8);
  cv::line(all, Point(3, 2), Point(cols - 3, 2), Scalar(0, 0, 0), 1, 8);
  cv::line(all, Point(3, rows - 2), Point(cols - 3, rows - 2), Scalar(0, 0, 0),
           1, 8);
  cv::line(all, Point(3, (int) (rows / 2)), Point(cols - 3, (int) (rows / 2)),
           Scalar(0, 0, 0), 1, 8);
  cv::line(all, Point(3, (int) (rows / 2) + 2),
           Point(cols - 3, (int) (rows / 2) + 2), Scalar(0, 0, 0), 1, 8);

  if (result)
  {
    //cout
    std::cout << std::endl;
    std::cout << "--- RESULT ---" << std::endl;
    cout << "best skeleton id " << id << endl;
    cout << "best user matched " << user + 1 << endl;
    cv::rectangle(matchUser, Point(2, 2),
                  Point(matchUser.rows - 3, matchUser.cols - 3), Scalar(0, 200, 0), 3,
                  1);
    cv::putText(all, "MATCHED", Point((int) ((cols - 100) / 2), rows - 168),
                FONT_HERSHEY_SIMPLEX, 0.75, Scalar::all(0), 1, 8);
  }
  else
  {
    cv::putText(all, "MATCHING...",
                Point((int) ((cols - 140) / 2), rows - 168), FONT_HERSHEY_SIMPLEX,
                0.75, Scalar::all(0), 1, 8);
  }

  matchUser.copyTo(all(roi));

  stringstream ss;
  /*
         ss.str("");
         ss << "ID " << id;
         cv::imshow(ss.str(), all);
         cv::waitKey(10);
         */

  //datas
  for (int i = 0; i < number_of_users; i++)
  {
    ss.str("");
    if (cand_us.candidate[index].matches[i] > 0)
    {
      ss << "User " << i + 1 << ": number of matches "
         << cand_us.candidate[index].matches[i] << ".  "
         << (int) (((float) cand_us.candidate[index].matches[i]
                    / (float) cand_us.candidate[index].totalmatches) * 100)
         << "%";
    }
    else {
      ss << "User " << i + 1 << ": number of matches "
         << cand_us.candidate[index].matches[i] << ".  "
         <<"0%";
    }
    cv::putText(all, ss.str(),
                Point(10, 20 + i * (rows / (2 * number_of_users + 1))),
                FONT_HERSHEY_SIMPLEX, 0.5, Scalar::all(0), 1, 8);
  }

  ss.str("");
  ss << "ID " << id;
  cv::imshow(ss.str(), all);
  cv::waitKey(1);
}

void onlineIdFunctions::usersWindow(std::vector<cv::Mat>& users_faces, int cols,
                                    int rows)
{
  Mat toDraw(rows, cols, CV_8UC3);
  toDraw.setTo(cv::Scalar(213, 214, 213));

  cv::line(toDraw, Point(1, 1), Point(1, rows - 1), Scalar(0, 0, 0), 1, 8);
  cv::line(toDraw, Point(cols - 1, 1), Point(cols - 1, rows - 1),
           Scalar(0, 0, 0), 1, 8);
  cv::line(toDraw, Point(1, 1), Point(cols - 1, 1), Scalar(0, 0, 0), 1, 8);
  cv::line(toDraw, Point(1, rows - 1), Point(cols - 1, rows - 1),
           Scalar(0, 0, 0), 1, 8);


  int size = std::min((float) (rows - 2) / users_faces.size() - 2,
                      (float) cols / 2 - 10);
  int coffset = cols / 4 - size / 2;
  for (uint i = 0; i < users_faces.size(); i++)
  {
    Mat temp = users_faces[i].clone();
    cv::rectangle(temp, Point(0, 0), Point(temp.rows - 1, temp.cols - 1),
                  Scalar(0, 0, 0), 2, 1);
    resize(temp, temp, Size(size, size));
    cv::Rect roi(cv::Point(coffset, 3 + i * (2 + size)), temp.size());
    temp.copyTo(toDraw(roi));
    stringstream ss;
    ss << "User " << i+1;
    cv::putText(toDraw, ss.str(),
                cv::Point((int) (cols / 2), (int) (3 + i * (2 + size) + size / 2)),
                FONT_HERSHEY_SIMPLEX, 0.5, Scalar::all(0), 1, 8);
  }


  cv::imshow("USERS LIST", toDraw);
  cv::waitKey(1);
}

void onlineIdFunctions::drawGraph(int freq[], int n_users)
{
  cv::namedWindow("GLOBAL GRAPH", CV_WINDOW_AUTOSIZE);
  cv::moveWindow("GLOBAL GRAPH", 0, 10);
  Mat result = Mat(240, 620, CV_8UC3);
  result.setTo(cv::Scalar(255, 255, 255));
  cv::line(result, Point(20, 1), Point(20, 220), Scalar(0, 0, 0), 2, 8);
  cv::line(result, Point(20, 220), Point(610, 220), Scalar(0, 0, 0), 2, 8);
  int offset = (int) (550 / n_users);
  for (int i = 1; i <= n_users; i++)
  {
    stringstream ss;
    ss << i;
    cv::putText(result, ss.str(), Point(20 + offset * i, 235),
                FONT_HERSHEY_SIMPLEX, 0.5, Scalar::all(0), 1, 8);
  }
  for (int i = 0; i < 5; i++)
  {
    cv::line(result, Point(20, 20 + 40 * i), Point(620, 20 + 40 * i),
             Scalar(0, 0, 0), 1, 8);
    stringstream ss;
    ss << (100 - i * 20) << "%";
    cv::putText(result, ss.str(), Point(0, 20 + 40 * i), FONT_HERSHEY_SIMPLEX,
                0.25, Scalar::all(0), 1, 8);
  }
  for (int i = 1; i <= n_users; i++)
  {
    circle(result, Point(20 + offset * i, 220 - freq[i - 1] * 4), 4,
        Scalar(200, 0, 0), -1, 1);
    stringstream ss;
    ss << freq[i - 1] * 2 << "%";
    cv::putText(result, ss.str(),
                Point(22 + offset * i, 213 - freq[i - 1] * 4), FONT_HERSHEY_SIMPLEX,
        0.4, Scalar(200, 0, 0), 1, 8);
  }
  cv::imshow("GLOBAL GRAPH", result);
  cv::waitKey(20000);
}

//void onlineIdFunctions::trackerCallback(
//		const people_tracking::TrackingResult& msg)
//{
//	tracks = msg.tracks;
//	/*
//	 cout << "Tracks ids: ";
//	 for (uint i = 0; i < tracks.size(); i++)
//	 {
//	 cout << tracks[i].id << " ";
//	 }
//	 cout << endl;
//	 */
//	tracker_ready = true;

//}

void onlineIdFunctions::lostCallback(const std_msgs::Int32::ConstPtr& msg)
{
  do_identification = 0;
}

bool onlineIdFunctions::readTfOnlyTorso(int index, pcl::PointXYZ& XYZPoint)
{
  try
  {
    tf::StampedTransform transform;
    stringstream ss;
    ss << "/skeleton_" << index << "_torso";
    listener.lookupTransform("/camera_rgb_optical_frame", ss.str(),
                             ros::Time(0), transform);
    XYZPoint.x = transform.getOrigin().x();
    XYZPoint.y = transform.getOrigin().y();
    XYZPoint.z = transform.getOrigin().z();
    return true;
  } catch (tf::TransformException& ex)
  {
    return false;
  }
}

void onlineIdFunctions::printDistances(int& openniId)
{
  cout << "--- Printing distances from openniId " << openniId
       << " to all tracker ids ---" << endl;
  pcl::PointXYZ openniTorso;
  while (!tracker_ready || !readTfOnlyTorso(openniId, openniTorso))
  {
    //wait until points are available
    //ROS_ERROR("Waiting tracker points");
  }
  //	cout << "Tracker id vector size: " << tracks.size() << endl;
  //	for (uint i = 0; i < tracks.size(); i++)
  //	{
  //		tf::StampedTransform transform1;
  //		listener.lookupTransform("/camera_rgb_optical_frame", "/odom",
  //				ros::Time(0), transform1);
  //		tf::Vector3 t(tracks[i].x, tracks[i].y, tracks[i].height / 2);
  //		t = transform1(t);
  //		float temp_distance = abs(t.x() - openniTorso.x)
  //				+ abs(t.y() - openniTorso.y) + abs(t.z() - openniTorso.z);
  //		cout << "Openni->   x=" << openniTorso.x << " y=" << openniTorso.y
  //				<< " z=" << openniTorso.z << endl;
  //		cout << "Track id" << tracks[i].id << " x=" << t.x() << " y=" << t.y()
  //				<< " z=" << t.z() << endl;
  //		cout << "xy=" << abs(t.x() - openniTorso.x) + abs(t.y() - openniTorso.y)
  //				<< " z=" << abs(t.z() - openniTorso.z) << endl;
  //		cout << "Distance= " << temp_distance << endl;
  //		cout << "---" << endl;
  //	}
}

int onlineIdFunctions::linkIds(int& openniId)
{
  cout << "--- Linking Ids ---" << endl;
  pcl::PointXYZ openniTorso;
  while (!tracker_ready || !readTfOnlyTorso(openniId, openniTorso))
  {
    //wait until points are available
    //ROS_ERROR("Waiting tracker points");
  }

  // lookupTransform("camera_rgb_optical_frame","odom", ..., transform);
  // tf::Vector3 t(x, y, height/2); // coordinate di tracking results
  // t = transform(t); // ora è in coordinate di openni

  float best_distance = 999;
  int linkedId = -1;
  //	cout << "Tracker id vector size: " << tracks.size() << endl;
  //	//find best distance between openniTorso and tracks points
  //	for (uint i = 0; i < tracks.size(); i++)
  //	{
  //		tf::StampedTransform transform1;
  //		listener.lookupTransform("/camera_rgb_optical_frame", "/odom",
  //				ros::Time(0), transform1);
  //		tf::Vector3 t(tracks[i].x, tracks[i].y, tracks[i].height / 2);
  //		t = transform1(t);

  //		float temp_distance = abs(t.x() - openniTorso.x)
  //				+ abs(t.y() - openniTorso.y) + abs(t.z() - openniTorso.z);

  //		cout << "openniId: " << openniId << " trackingId: " << tracks[i].id
  //				<< " distance: " << temp_distance << endl;
  //		if (temp_distance <= 0.5 && temp_distance < best_distance)
  //		{
  //			best_distance = temp_distance;
  //			linkedId = tracks[i].id;
  //		}
  //	}
  return linkedId;
}

void onlineIdFunctions::sendIdMsg(ros::Publisher& pub, int& openniId)
{
  //link ids and send the matched id in the topic person_to_follow
  int linkedId = linkIds(openniId);
  if (linkedId != -1)
  {
    cout << "--- Sending linked id: " << linkedId << " ---" << endl;
    std_msgs::Int32 msg;
    msg.data = linkedId;
    pub.publish(msg);
    cout << "--- Message sent in /person_to_follow ---" << endl;
    do_identification = 1;
  }
  else
  {
    cout << "--- NO ID LINKED. RESTART ---" << endl;
    do_identification = 2;
  }
}
