/*
 * readAndDisplayUtils.cpp
 *
 *  Created on: Oct 18, 2012
 *      Author: Matteo Munaro
 */

#include <onlineIdentification/readAndDisplayUtils.h>

void readKinectDataFilenames(std::string dataDirectory, std::vector<std::string>& rgbFilenames, std::vector<std::string>& depthFilenames,
			std::vector<std::string>& userMapFilenames, std::vector<std::string>& skeletonFilenames, std::vector<std::string>& groundCoeffFilenames)
{
	// Given a directory, read filenames of RGB images, depth images and skeleton data files and put them into vectors of strings
    DIR *dir;
    struct dirent *ent;
    dir = opendir (dataDirectory.c_str());
    int len;
    if (dir != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            len = std::strlen (ent->d_name);
            if (len >= 5)
            {
            	switch ((ent->d_name[len - 5]))
            	{
					case 'b' :											// RGB images end with "b.jpg"
						rgbFilenames.push_back(ent->d_name);
						break;

					case 'h' :											// depth images end with "h.pgm"
						depthFilenames.push_back(ent->d_name);
						break;

					case 'p' :											// user map files end with "p.pgm"
						userMapFilenames.push_back(ent->d_name);
						break;

					case 'f' :											// ground coefficients files end with "f.txt"
						groundCoeffFilenames.push_back(ent->d_name);
						break;

					case 'l' :											// skeleton files end with "l.txt"
						skeletonFilenames.push_back(ent->d_name);
						break;
            	}
            }
        }
        closedir (dir);
    }
    // Sort filenames in alphabetical order:
    sort (rgbFilenames.begin(), rgbFilenames.end(), less<string>());
    sort (depthFilenames.begin(), depthFilenames.end(), less<string>());
    sort (userMapFilenames.begin(), userMapFilenames.end(), less<string>());
    sort (groundCoeffFilenames.begin(), groundCoeffFilenames.end(), less<string>());
    sort (skeletonFilenames.begin(), skeletonFilenames.end(), less<string>());
}

void readSkeletonData(std::string skeletonFilename, std::vector<float>& skeletonsData)
{
	// read file with skeleton data and put them into an array
	std::string line;
	std::ifstream skeletonFile;
	skeletonFile.open(skeletonFilename.c_str());
	// For every tracked user:
	for (int t = 0; t < NUMBER_OF_TRACKED_SKELETON; t++)
	{
		for (int i = 0; i < NUMBER_OF_SKELETAL_JOINTS; i++)
		{
			getline (skeletonFile,line);  		// read line
			size_t prev_tok_end = -1;
			for (int j = 0; j < NUMBER_OF_JOINT_INFO; j++)
			{
				size_t tok_end = line.find_first_of(",", prev_tok_end+1);  // search for token ","
				skeletonsData[t*(NUMBER_OF_SKELETAL_JOINTS*NUMBER_OF_JOINT_INFO) + i*NUMBER_OF_JOINT_INFO + j] =
						std::atof((line.substr(prev_tok_end+1, tok_end - prev_tok_end-1 )).c_str()); 		// insert data into the vector
				prev_tok_end = tok_end;
			}
		}
	}
	skeletonFile.close();
}

void readGroundCoeff(std::string groundCoeffFilename, std::vector<float>& currentGroundCoeff, std::vector<float>& validGroundCoeff)
{
	// read file with ground coefficients and put them into a vector
	std::string line;
	std::ifstream groundCoeffFile;
	groundCoeffFile.open(groundCoeffFilename.c_str());
	getline (groundCoeffFile,line);  		// read line
	// For every coefficient:
	size_t prev_tok_end = -1;
	for (unsigned int i = 0; i < currentGroundCoeff.size(); i++)
	{
		size_t tok_end = line.find_first_of(",", prev_tok_end+1);  // search for token ","
		currentGroundCoeff[i] = std::atof((line.substr(prev_tok_end+1, tok_end - prev_tok_end-1 )).c_str()); 		// insert data into the vector
		prev_tok_end = tok_end;
	}

	groundCoeffFile.close();
	if ((currentGroundCoeff[0] != 0) || (currentGroundCoeff[1] != 0) || (currentGroundCoeff[2] != 0) || (currentGroundCoeff[3] != 0))
	{
		validGroundCoeff[0] = currentGroundCoeff[0];
		validGroundCoeff[1] = currentGroundCoeff[1];
		validGroundCoeff[2] = currentGroundCoeff[2];
		validGroundCoeff[3] = currentGroundCoeff[3];
	}
}

void displaySkeletonOnImage(cv::Mat rgbImage, std::vector<float>& skeletonsData, bool singleColor, bool flipY, float scaleFactor)
{
	int lineWidth = 3;
	std::vector<cv::Point > points2D;
	for (int t = 0; t < NUMBER_OF_TRACKED_SKELETON; t++)				// for every user
	{
		points2D.clear();			// clear vector of 2D joints position
		if (skeletonsData[t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS] > 0) // if it is a valid user
		{
			//std::cout << "t = " << t << ", " << skeletonsData[t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS] << std::endl;
			for (int i = 0; i < NUMBER_OF_SKELETAL_JOINTS; i++)			// for every skeleton joint
			{
				if (skeletonsData[6 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS] == 2)
				{
					if (flipY)
					{
						points2D.push_back(cv::Point(scaleFactor*skeletonsData[4 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS],
							rgbImage.rows-scaleFactor*skeletonsData[5 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]));
					}
					else
					{
						points2D.push_back(cv::Point(scaleFactor*skeletonsData[4 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS],
							scaleFactor*skeletonsData[5 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]));
					}
					cv::circle(rgbImage, points2D[i], 5, red, 3);				// plot the joint center
				}
				else
				{
					points2D.push_back(cv::Point(-1,-1));
				}
			}
			if (singleColor)
			{
				cv::Scalar linesColor(255*std::fabs(t-1), 0, 255*t);
				// Plot the links with the same color:
				if (points2D[2].x > 0)
				{
					if (points2D[3].x > 0)
						cv::line(rgbImage, points2D[3],points2D[2],linesColor,lineWidth);
					if (points2D[4].x > 0)
						cv::line(rgbImage, points2D[2],points2D[4],linesColor,lineWidth);
					if (points2D[1].x > 0)
						cv::line(rgbImage, points2D[2],points2D[1],linesColor,lineWidth);
					if (points2D[8].x > 0)
						cv::line(rgbImage, points2D[2],points2D[8],linesColor,lineWidth);
				}
				if (points2D[0].x > 0)
				{
					if (points2D[1].x > 0)
						cv::line(rgbImage, points2D[1],points2D[0],linesColor,lineWidth);
					if (points2D[12].x > 0)
						cv::line(rgbImage, points2D[0],points2D[12],linesColor,lineWidth);
					if (points2D[16].x > 0)
						cv::line(rgbImage, points2D[0],points2D[16],linesColor,lineWidth);
				}
				if ((points2D[4].x > 0) && (points2D[5].x > 0))
					cv::line(rgbImage, points2D[4],points2D[5],linesColor,lineWidth);
				if ((points2D[5].x > 0) && (points2D[6].x > 0))
					cv::line(rgbImage, points2D[5],points2D[6],linesColor,lineWidth);
				if ((points2D[6].x > 0) && (points2D[7].x > 0))
					cv::line(rgbImage, points2D[6],points2D[7],linesColor,lineWidth);

				if ((points2D[8].x > 0) && (points2D[9].x > 0))
					cv::line(rgbImage, points2D[8],points2D[9],linesColor,lineWidth);
				if ((points2D[9].x > 0) && (points2D[10].x > 0))
					cv::line(rgbImage, points2D[9],points2D[10],linesColor,lineWidth);
				if ((points2D[10].x > 0) && (points2D[11].x > 0))
					cv::line(rgbImage, points2D[10],points2D[11],linesColor,lineWidth);

				if ((points2D[12].x > 0) && (points2D[13].x > 0))
					cv::line(rgbImage, points2D[12],points2D[13],linesColor,lineWidth);
				if ((points2D[13].x > 0) && (points2D[14].x > 0))
					cv::line(rgbImage, points2D[13],points2D[14],linesColor,lineWidth);
				if ((points2D[14].x > 0) && (points2D[15].x > 0))
					cv::line(rgbImage, points2D[14],points2D[15],linesColor,lineWidth);

				if ((points2D[16].x > 0) && (points2D[17].x > 0))
					cv::line(rgbImage, points2D[16],points2D[17],linesColor,lineWidth);
				if ((points2D[17].x > 0) && (points2D[18].x > 0))
					cv::line(rgbImage, points2D[17],points2D[18],linesColor,lineWidth);
				if ((points2D[18].x > 0) && (points2D[19].x > 0))
					cv::line(rgbImage, points2D[18],points2D[19],linesColor,lineWidth);
			}
			else
			{
				// Plot the links with different colors:
				if (points2D[2].x > 0)
				{
					if (points2D[3].x > 0)
						cv::line(rgbImage, points2D[3],points2D[2],green,lineWidth);
					if (points2D[4].x > 0)
						cv::line(rgbImage, points2D[2],points2D[4],cyan,lineWidth);
					if (points2D[1].x > 0)
						cv::line(rgbImage, points2D[2],points2D[1],black,lineWidth);
					if (points2D[8].x > 0)
						cv::line(rgbImage, points2D[2],points2D[8],magenta,lineWidth);
				}
				if (points2D[0].x > 0)
				{
					if (points2D[1].x > 0)
						cv::line(rgbImage, points2D[1],points2D[0],black,lineWidth);
					if (points2D[12].x > 0)
						cv::line(rgbImage, points2D[0],points2D[12],blue,lineWidth);
					if (points2D[16].x > 0)
						cv::line(rgbImage, points2D[0],points2D[16],blue,lineWidth);
				}
				if ((points2D[4].x > 0) && (points2D[5].x > 0))
					cv::line(rgbImage, points2D[4],points2D[5],cyan,lineWidth);
				if ((points2D[5].x > 0) && (points2D[6].x > 0))
					cv::line(rgbImage, points2D[5],points2D[6],cyan,lineWidth);
				if ((points2D[6].x > 0) && (points2D[7].x > 0))
					cv::line(rgbImage, points2D[6],points2D[7],cyan,lineWidth);

				if ((points2D[8].x > 0) && (points2D[9].x > 0))
					cv::line(rgbImage, points2D[8],points2D[9],magenta,lineWidth);
				if ((points2D[9].x > 0) && (points2D[10].x > 0))
					cv::line(rgbImage, points2D[9],points2D[10],magenta,lineWidth);
				if ((points2D[10].x > 0) && (points2D[11].x > 0))
					cv::line(rgbImage, points2D[10],points2D[11],magenta,lineWidth);

				if ((points2D[12].x > 0) && (points2D[13].x > 0))
					cv::line(rgbImage, points2D[12],points2D[13],blue,lineWidth);
				if ((points2D[13].x > 0) && (points2D[14].x > 0))
					cv::line(rgbImage, points2D[13],points2D[14],blue,lineWidth);
				if ((points2D[14].x > 0) && (points2D[15].x > 0))
					cv::line(rgbImage, points2D[14],points2D[15],blue,lineWidth);

				if ((points2D[16].x > 0) && (points2D[17].x > 0))
					cv::line(rgbImage, points2D[16],points2D[17],blue,lineWidth);
				if ((points2D[17].x > 0) && (points2D[18].x > 0))
					cv::line(rgbImage, points2D[17],points2D[18],blue,lineWidth);
				if ((points2D[18].x > 0) && (points2D[19].x > 0))
					cv::line(rgbImage, points2D[18],points2D[19],blue,lineWidth);

//				// Plot the links with different colors:
//				cv::line(rgbImage, points2D[3],points2D[2],green,lineWidth);
//				cv::line(rgbImage, points2D[2],points2D[4],cyan,lineWidth);
//				cv::line(rgbImage, points2D[4],points2D[5],cyan,lineWidth);
//				cv::line(rgbImage, points2D[5],points2D[6],cyan,lineWidth);
//				cv::line(rgbImage, points2D[6],points2D[7],cyan,lineWidth);
//				cv::line(rgbImage, points2D[2],points2D[8],magenta,lineWidth);
//				cv::line(rgbImage, points2D[8],points2D[9],magenta,lineWidth);
//				cv::line(rgbImage, points2D[9],points2D[10],magenta,lineWidth);
//				cv::line(rgbImage, points2D[10],points2D[11],magenta,lineWidth);
//				cv::line(rgbImage, points2D[2],points2D[1],black,lineWidth);
//				cv::line(rgbImage, points2D[1],points2D[0],black,lineWidth);
//				cv::line(rgbImage, points2D[0],points2D[12],blue,lineWidth);
//				cv::line(rgbImage, points2D[12],points2D[13],blue,lineWidth);
//				cv::line(rgbImage, points2D[13],points2D[14],blue,lineWidth);
//				cv::line(rgbImage, points2D[14],points2D[15],blue,lineWidth);
//				cv::line(rgbImage, points2D[0],points2D[16],blue,lineWidth);
//				cv::line(rgbImage, points2D[16],points2D[17],blue,lineWidth);
//				cv::line(rgbImage, points2D[17],points2D[18],blue,lineWidth);
//				cv::line(rgbImage, points2D[18],points2D[19],blue,lineWidth);
			}
		}
	}
}

void readFaceTrainingSet(std::string faceTrainingSetDir, std::string trainingSetFilename, std::string faceTrainingSetDescriptorsDir, std::string faceTrainingSetImagesDir,
		std::vector<int>& labels, std::vector<cv::Mat>& trainingDescriptors, std::vector<cv::Mat>& trainingFaces)
{
	// read training set file (names and labels) and put them into a vector:
	std::string line;
	std::ifstream trainingSetFile;				// file containing descriptors filenames and face labels
	trainingSetFile.open((faceTrainingSetDir + trainingSetFilename).c_str());
	while (trainingSetFile.good())				// until the file is finished
	{
		getline (trainingSetFile,line);  		// read line
		if (line.size() > 0)					// if valid line
		{
			size_t filenameEnd = line.find_first_of(" ", 0);  			// search for token " "
			std::string descriptorName = line.substr(0, filenameEnd);	// descriptor filename

			// Read label and push it into the labels vector:
			size_t labelEnd = line.find_first_of(" ", filenameEnd+1);  	// search for token " "
			labels.push_back(std::atof((line.substr(filenameEnd+1, labelEnd)).c_str()));	// push back descriptor label

			// Read descriptor and push it into the descriptors vector:
			cv::FileStorage fs(faceTrainingSetDescriptorsDir + descriptorName, cv::FileStorage::READ);
			cv::Mat currentDescriptor(NUMBER_OF_FIDUCIAL_POINTS, SURF_DESCRIPTOR_DIMENSION, CV_32FC1);
			fs["faceDescriptor"] >> currentDescriptor;						// write data from file to cv::Mat
			trainingDescriptors.push_back(currentDescriptor.reshape(1,1));	// reshape the cv::Mat to be one single row and push it into the descriptors vector

			// Read image and push it into the faces vector:
			trainingFaces.push_back(cv::imread(faceTrainingSetImagesDir + descriptorName.substr(0,descriptorName.size()-4) + ".png"));
		}
	}
	trainingSetFile.close();
}

void generateColors(int colorsNumber, std::vector<cv::Vec3f>& bodyPartsColors)
{
	for (unsigned int i = 0; i < colorsNumber; i++)
	{
		bodyPartsColors.push_back(cv::Vec3f(
				float(rand() % 256),// / 255,
				float(rand() % 256),// / 255,
				float(rand() % 256)));// / 255));
	}
}
