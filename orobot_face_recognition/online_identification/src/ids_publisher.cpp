#include <ros/ros.h>
#include <opt_msgs/TrackArray.h>
#include <action_model/IdMessage.h>

opt_msgs::TrackArray _tracks;
bool _new_callback(false);

void
tracksCb(const opt_msgs::TrackArrayConstPtr& tracks)
{
  if(tracks->tracks.size() == 0) return;
  _new_callback = true;
  _tracks = *tracks;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "ids_publisher");

  ros::NodeHandle node_handle;
  ros::Subscriber sub = node_handle.subscribe<opt_msgs::TrackArray>("/tracker/tracks_smoothed", 1, tracksCb);
  ros::Publisher pub = node_handle.advertise<action_model::IdMessage>("ids", 1);
  ros::Rate rate(30);

  while(ros::ok())
  {
    while(!_new_callback)
    {
      rate.sleep();
      ros::spinOnce();
    }
    _new_callback = false;
    action_model::IdMessage msg;
    msg.id_vector.resize(_tracks.tracks.size());
    for(uint i = 0; i < msg.id_vector.size(); ++i)
    {
      msg.id_vector[i] = _tracks.tracks[i].id;
    }
    pub.publish(msg);
    rate.sleep();
    ros::spinOnce();
  }

  ros::shutdown();
  return 0;
}
