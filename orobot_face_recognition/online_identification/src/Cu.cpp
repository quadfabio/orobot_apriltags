#include <onlineIdentification/Cu.h>

using namespace std;

Cu::Cu(int id, Timing code, int number_of_users)
{
	this->id = id;
	this->code = code;
	this->number_of_users = number_of_users;
	vector<int>(number_of_users, 0).swap(matches);
	totalmatches = 0;
}

void Cu::setId(int new_id)
{
	id = new_id;
}

void Cu::updateCode(Timing new_code)
{
	code = new_code;
}

void Cu::updateByCode(Timing new_code, int user_matched)
{
	if (new_code - code > 5000)
		vector<int>(number_of_users, 0).swap(matches);

	code = new_code;
	matches[user_matched] = matches[user_matched] + 1;
	totalmatches++;
}

int Cu::getBestUser()
{
	int max = -1;
	int user = -1;
	for (uint i = 0; i < matches.size(); i++)
	{
		if (max < matches[i])
		{
			max = matches[i];
			user = i;
		}
	}
	return user;
}

int Cu::getBestMatch()
{
	int max = -1;
	for (uint i = 0; i < matches.size(); i++)
	{
		if (max < matches[i])
		{
			max = matches[i];
		}
	}
	return max;
}

void Cu::resetMatches(Timing code)
{
	vector<int>(number_of_users, 0).swap(matches);
	totalmatches = 0;
	this->code = code;
}

