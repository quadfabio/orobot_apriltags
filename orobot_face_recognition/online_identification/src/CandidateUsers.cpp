#include <onlineIdentification/CandidateUsers.h>

using namespace std;

CandidateUsers::CandidateUsers()
{
	last_id_matched = 0;
	last_index_matched = 0;
	best_id = 0;
	best_index = 0;
	best_user = 0;
	ids_contained = 0;
	matches_[-1] = 0;
}

void CandidateUsers::addId(int id, Timing code, int number_of_users)
{
	Cu temp(id, code, number_of_users);
	candidate.push_back(temp);
	ids_contained ++;
}

void CandidateUsers::addMatch(int id, int best_user, Timing code, int number_of_users)
{
	int index = containsId(id);

	if (!candidate.empty() && index != -1)
	{
		candidate[index].updateByCode(code, best_user);
	}

	last_id_matched = id;
	last_index_matched = index;

	if (matches_.find(best_user) != matches_.end())
	{ //user already matched before
	  matches_[best_user]++;
	}
	else
	{
	  matches_[best_user] = 1;
	}
}

void CandidateUsers::unknownMatch()
{
  matches_[-1]++;
}

void CandidateUsers::updateCode(int id, Timing code)
{
	int index = containsId(id);
	if (index != -1)
	{
		candidate[index].updateCode(code);
	}
}

int CandidateUsers::containsId(int id)
{
	for (uint i = 0; i < candidate.size(); i++)
	{
		if (candidate[i].id == id)
		{
			return i;
		}
	}
	return -1;
}

int CandidateUsers::findMatchID(Timing code)
{
	int max = -1;
	best_user = -1;
	best_id = -1;
	best_index = -1;
	for (uint i = 0; i < candidate.size(); i++)
	{
		int bm = candidate[i].getBestMatch();
		if (code - candidate[i].code <= 5000 && bm > max)
		{
			best_user = candidate[i].getBestUser();
			best_id = candidate[i].id;
			best_index = i;
			max = bm;
		}
	}
	return best_id;
}

int CandidateUsers::findMatchUSER()
{
	return best_user;
}

int CandidateUsers::findMatchINDEX()
{
	return best_index;
}

void CandidateUsers::stamp()
{
	for (uint i = 0; i < candidate.size(); i++)
	{
		std::cout << "User openni Id: " << candidate[i].id;
		for (uint j = 0; j < candidate[i].matches.size(); j++)
		{
			std::cout << j << "-" << candidate[i].matches[j] << ";;;";
		}
		std::cout << std::endl << std::endl;
	}
}

void CandidateUsers::resetAll()
{
	last_id_matched = 0;
	last_index_matched = 0;
	best_id = 0;
	best_index = 0;
	best_user = 0;
	ids_contained = 0;
	std::vector<Cu>().swap(candidate);
	matches_.clear();
	matches_[-1] = 0;
}

void CandidateUsers::printMatches()
{
  std::cout << std::endl << std::endl << "Matches: " << std::endl;
  for(auto it = matches_.begin(); it != matches_.end(); ++it)
  {
    std::cout << "\t\t ID: " << it->first;
    std::cout << "\t\t MATCHES: " << it->second << std::endl << std::endl;
  }
}
