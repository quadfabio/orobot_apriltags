/*
 * mainIdentification.cpp
 *
 *  Created on: Oct 17, 2012
 *      Author: Matteo Munaro
 */

// ROS includes:
#include <ros/ros.h>
#include <ros/package.h>

//togliere quelli inutili
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/sample_consensus/sac_model_plane.h>
/////////////////////////////////

#include <faceRecognition/forest.hpp>
#include <faceRecognition/multi_part_sample.hpp>
#include <faceRecognition/head_pose_sample.hpp>
#include <faceRecognition/face_utils.hpp>
#include <istream>
#include <opencv2/opencv.hpp>
#include <boost/progress.hpp>
#include <boost/thread.hpp>
#include <faceRecognition/face_forest.hpp>
#include <faceRecognition/feature_channel_factory.hpp>
#include <faceRecognition/timing.hpp>

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <onlineIdentification/pointCloudUtils.h>
#include <onlineIdentification/readAndDisplayUtils.h>
#include <onlineIdentification/onlineIdFunctions.h>
#include <onlineIdentification/CandidateUsers.h>

#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Image.h>
#include <std_msgs/Int32.h>
//#include <action_model/IdMessage.h>
#include <opt_msgs/TrackArray.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <tf/transform_listener.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <string>

#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/synchronizer.h>
#include <message_filters/subscriber.h>

#include <sound_play/sound_play.h>
#include <unistd.h>

using namespace std;
using namespace cv;
namespace enc = sensor_msgs::image_encodings;

cv::Mat depthDistortionParameters(1, 5, CV_32F);

// For ground selection:
std::vector<cv::Point> _points;
bool _finished = false;
bool estimate_ground = false;
Eigen::Matrix3f points;
Eigen::VectorXf plane_coeffs;

int rows = 400;
int cols = 330;

int kfd = 0;
struct termios cooked;
void quit(int sig)
{
  tcsetattr(kfd, TCSANOW, &cooked);
  ros::shutdown();
  exit(0);
}

void sleepok(int t, ros::NodeHandle &nh)
{
  if (nh.ok())
    sleep(t);
}

void createInfoWindows(int ind, int current_user_id)
{
  stringstream ss;
  int s = 400 * (ind + 1);
  if (s > 1200)
    s = 1200;
  //create new info window for this id
  ss << "ID " << current_user_id;
  cv::namedWindow(ss.str(), 0);
  cv::moveWindow(ss.str(), s, 360);
  cv::resizeWindow(ss.str(), cols, rows);
  //create new elaboration window for this id
  ss.str("");
  ss << "ELAB ID " << current_user_id;
  cv::namedWindow(ss.str(), 0);
  cv::moveWindow(ss.str(), s, 10);
  cv::resizeWindow(ss.str(), 300, 250);
  cv::waitKey(1);
}

void click_cb(int event, int x, int y, int flags, void* param)
{
  switch (event)
  {
    case CV_EVENT_LBUTTONUP:
      {
        //TODO control if depth is nan
        cv::Point p(x, y);
        _points.push_back(p);
        break;
      }
    case CV_EVENT_RBUTTONUP:
      {
        _finished = true;
        break;
      }
  }
}



int main(int argc, char** argv)
{

  ros::init(argc, argv, "onlineId");

  ros::NodeHandle n;
  ros::NodeHandle nh;

  std::string("/home/marco/bags");

  //onlineIdFunctions object
  //	onlineIdFunctions onlineId(5.70e+02, 0.0, 3.20e+02, 0.0, 5.70e+02, 2.40e+02,
  //			0.0, 0.0, 1.0);
  onlineIdFunctions onlineId(1081.3720703125, 0, 959.5,
                             0, 1081.3720703125, 539.5,
                             0, 0, 1);
  //

  //SoundClient
  sound_play::SoundClient sc;
  //

  //CandidateUsers object
  CandidateUsers cand_us;
  //

  //descriptors path
  string descriptorsPath =
//      ros::package::getPath("online_identification") + "/descriptors/";
                        "/home/marco/bags/01-marco/01_marco_0_1m/";
  string num_users_filePath =
//      ros::package::getPath("online_identification") + "/descriptors/number_of_users.txt";
                        "/home/marco/bags/01-marco/01_marco_0_1m/number_of_users.txt";
  string unknownPath =
//      ros::package::getPath("online_identification") + "/descriptors/unknown.jpg";
                        "/home/marco/bags/01-marco/01_marco_0_1m/unknown.jpg";
  string face_config_dir =
      ros::package::getPath("online_identification") + "/data/";
  //launch parameters (trainingF modeF svmF stopC)
  int training = 0; //training=0 training; else testing
  int mode = 2; 	  //mode=0 face; mode=1 skeleton; mode=2 face+skeleton
  //in training mode it's always face+skeleton
  int svm = 1;      //0 NN; else SVM
  //in training mode this flag isn't used

  int stopCondition = 0;  //0=5seconds recognition
  //1=until match recognition

  int seconds_to_take_decision = 10;
  double minimum_age_to_be_considered = 0.0f;
  double maximum_distance_to_be_considered = 4.5f;

  n.getParam("trainingFlag", training);
  n.getParam("modeFlag", mode);
  n.getParam("svmFlag", svm);
  n.getParam("stopCondition", stopCondition);
  n.getParam("seconds_to_take_decision", seconds_to_take_decision);
  n.getParam("minimum_age_to_be_considered", minimum_age_to_be_considered);
  n.getParam("max_distance_to_be_considered", maximum_distance_to_be_considered);


  bool trainingFlag = true;

  //ros
  ros::Subscriber rgb_sub = n.subscribe("/kinect2_head/rgb/image", 1,
                                        &onlineIdFunctions::rgbCallback, &onlineId);
  ros::Subscriber cloud_sub = n.subscribe("/kinect2_head/depth_ir/points", 1,
                                          &onlineIdFunctions::cloudCallback, &onlineId);
  ros::Subscriber depth_sub = n.subscribe("/kinect2_head/depth/image",
                                          1, &onlineIdFunctions::depthCallback, &onlineId);
  ros::Subscriber ids_sub = n.subscribe("/tracker/tracks_smoothed", 1,
                                        &onlineIdFunctions::idsCallback, &onlineId);
  ros::Publisher pub = n.advertise<std_msgs::Int32>("/person_to_follow", 1);
  ros::Subscriber sub_lost = n.subscribe("/lost_track", 1,
                                         &onlineIdFunctions::lostCallback, &onlineId);
  //

  // parameters
  float height_percentage(0.3), left_width_percentage(0.2), top_height_offset(0.25),
      right_width_percentage(0.25), bottom_height_offset(0.75);
  //  n.getParam("height_percentage", height_percentage);
  //  n.getParam("top_height_offset", top_height_offset);
  //  n.getParam("right_width_percentage", right_width_percentage);
  //  n.getParam("bottom_height_offset", bottom_height_offset);
  //  n.getParam("left_width_percentage", left_width_percentage);

  //windows
  cv::namedWindow("RGB IMAGE", CV_WINDOW_KEEPRATIO);
  cv::moveWindow("RGB IMAGE", 0, 10);
  cv::namedWindow("USERS LIST", 0);
  cv::moveWindow("USERS LIST", 0, 360);
  cv::resizeWindow("USERS LIST", cols - 100, rows);
  //cv::namedWindow("DEPTH IMAGE", CV_WINDOW_KEEPRATIO);
  //cv::moveWindow("DEPTH IMAGE", 800, 10);
  //

  onlineId.rgb_cv.reset(new cv_bridge::CvImage);
  onlineId.depth_cv.reset(new cv_bridge::CvImage);

  Eigen::Matrix3f rgbIntrinsicMatrix;
  Eigen::Matrix3f depthIntrinsicMatrix;
  Eigen::Vector3f translationIR_RGB;
  Eigen::Vector3f rotationAnglesIR_RGB;
  Eigen::Affine3f depthToRGBTransform;

  int counter = 0;
  float keypointSize = 6;

  rgbIntrinsicMatrix << 1081.3720703125, 0, 959.5,
      0, 1081.3720703125, 539.5,
      0, 0, 1;
  depthIntrinsicMatrix << 365.9466857910156, 0, 257.2561950683594,
      0, 365.9466857910156, 202.9217071533203,
      0, 0, 1;

  translationIR_RGB << 0.0000, 0.00000, 0.000;
  rotationAnglesIR_RGB << 0.0, 0.0, 0.0;

  depthDistortionParameters =
      (cv::Mat_<double>(1, 5) << 0.09807992726564407, -0.2776226103305817,
       0, 0, 0.09507202357053757);
  //

  // Affine transformation between IR and RGB cameras:
  pcl::getTransformation(translationIR_RGB(0), translationIR_RGB(1),
                         translationIR_RGB(2), rotationAnglesIR_RGB(0),
                         rotationAnglesIR_RGB(1),
                         rotationAnglesIR_RGB(2), depthToRGBTransform);
  //

  //// INITIALIZATION HEAD POSE ESTIMATION AND FACE FEATURES DETECTION /////
  std::string face_cascade = face_config_dir
      + "haarcascade_frontalface_alt.xml";
  std::string headpose_config_file = face_config_dir + "config_headpose.txt";
  std::string ffd_config_file = face_config_dir + "config_ffd.txt";
  // parse config file
  ForestParam mp_param;
  loadConfigFile(ffd_config_file, mp_param);
  FaceForestOptions faceForestOption;
  faceForestOption.face_detection_option.path_face_cascade = face_cascade;
  ForestParam head_param;
  loadConfigFile(headpose_config_file, head_param);
  faceForestOption.head_pose_forest_param = head_param;
  faceForestOption.mp_forest_param = mp_param;
  faceForestOption.mp_forest_param.treePath = face_config_dir + "trees_ffd/";
  faceForestOption.head_pose_forest_param.treePath = face_config_dir
      + "trees_headpose/tree_";
  FaceForest faceForestObject = FaceForest(faceForestOption);
  //

  //READ NUMBER OF USERS & DESCRIPTORS
  //read number of users
  int number_of_users = 0;
  ifstream ReadFile(num_users_filePath.c_str());
  ReadFile >> number_of_users;
  std::cout << "--- Number of users " << number_of_users << " ---"
            << std::endl;

  //used only if test mode
  Mat descriptors;
  // mat in cui ogni riga è un descrittore, l'utente i ha i descr in
  // righe 30*i...30*i+29
  vector<Mat> users_faces;
  // jpg files with the face of users, index i->user i
  vector<DMatch> match;
  FlannBasedMatcher matcher;
  Mat h;
  float b = 0;
  Mat unknown = imread(unknownPath.c_str());
  std::vector<float> med;
  std::vector<float> dev;
  onlineId.fillMedDevWithZero(med, dev, mode);

    if (trainingFlag)
    {
      number_of_users++;
    }
  //  else
  //  {
  //    std::cout << "--- Reading descriptors. ---" << std::endl << "--- 0%  ";
  //    //if in test mode, read descriptors (based on mode choice)
  //    onlineId.readDescriptors(descriptors, users_faces, number_of_users,
  //                             descriptorsPath, mode);
  //    std::cout << "  50%  ";
  //    if (!svmFlag)
  //    { //only if NN normalize descriptors now
  //      onlineId.calculateMeanStdDev(descriptors, med, dev, number_of_users);
  //      onlineId.normalizeData(descriptors, med, dev);
  //      std::cout << "100% ---" << endl;
  //    }
  //    else
  //    { //if SVM read h and b
  //      onlineId.read_h_b(h, b, mode);
  //      std::cout << "100% ---" << endl;
  //    }
  //    std::cout << "--- Descriptors loaded and normalized. ---" << std::endl;
  //  }
  //  //

  //  if (!trainingFlag)
  //  {
  //    //list of users window
  //    onlineId.usersWindow(users_faces, cols - 100, rows);
  //    //
  //  }

  //timer
  //Timing evaluationTimer;
  //int counterTimer = 0;
  Timing timeFromStart;
  //

  Timing code;
  bool coutFlag = false;

  //main cycle
  while (ros::ok())
  {

    ros::spinOnce();

    if (onlineId.do_identification == 0)
    { //if first identification or last authorized track was lost

      if (onlineId.new_rgb /*&& onlineId.new_pointcloud*/)
      { //at least one rgb frame
        const float cols_proportion = onlineId.rgb_cv->image.cols / 512.0f;
        const float rows_proportion = onlineId.rgb_cv->image.rows / 424.0f;
        bool _finish(false);

        if (trainingFlag)
        { //TRAINING

          for (std::vector<opt_msgs::Track>::const_iterator iter =
               onlineId.tracks_.tracks.begin();
               iter < onlineId.tracks_.tracks.end() && !_finish; ++iter)
          {
            // Face ROI
            const opt_msgs::Track& t = *iter;
            if (t.age < minimum_age_to_be_considered
                ||
                t.distance > maximum_distance_to_be_considered) continue;
            const int ind = t.id;
            const float TOP_LEFT_X = t.box_2D.x * cols_proportion;
            const float TOP_LEFT_Y = t.box_2D.y * rows_proportion;
            const float WIDTH = t.box_2D.width * cols_proportion;
            const float HEIGHT =t.box_2D.height * rows_proportion;
            const float SCALED_HEIGHT = HEIGHT * height_percentage;
            cv::Point top_left(TOP_LEFT_X, TOP_LEFT_Y);
            cv::Point bottom_right = top_left + cv::Point(WIDTH, HEIGHT);
            cv::Point scaled_top_left(std::min(top_left.x + WIDTH *
                                               left_width_percentage,
                                               float(onlineId.rgb_cv->image.cols)),
                                      std::max(0.0f, top_left.y - HEIGHT *
                                               top_height_offset));
            cv::Point scaled_bottom_right(std::max(bottom_right.x - WIDTH *
                                                   left_width_percentage,
                                                   float(scaled_top_left.x)),
                                          std::max(float(scaled_top_left.y),
                                                   bottom_right.y -
                                                   HEIGHT *
                                                   bottom_height_offset));

            //face selection
            std::vector<Face> faces;
            cv::Mat imageSelectionGray;
            cv::Mat imageToDisplay = onlineId.rgb_cv->image.clone();
            onlineId.selectFaceWithROI(faceForestObject, 1, true,
                                       onlineId.rgb_cv->image, imageToDisplay,
                                       imageSelectionGray, faces, false,
                                       number_of_users,
                                       counter, trainingFlag, descriptorsPath,
                                       scaled_top_left, scaled_bottom_right);
            if (faces.size() > 0)
            { //if there is at least a face

              // draw green rectangle
              cv::rectangle(imageToDisplay, cv::Point(20, 20),
                            cv::Point(60, 60), cv::Scalar(0, 255, 0), 20);

              // Face descriptor computation:
              Face currentFace = faces[0];
              cv::Mat faceDescriptor(SURF_DESCRIPTOR_DIMENSION,
                                     NUMBER_OF_FIDUCIAL_POINTS, CV_32FC1);
              FaceForest::compute_face_descriptor(imageSelectionGray,
                                                  currentFace, faceDescriptor,
                                                  keypointSize, false);
              //save 30 face descriptors & 30 skeleton descriptors
              if (counter < std::numeric_limits<int>::max())
              {
                //save the face descriptor:
                stringstream ss;
                ss << number_of_users << "_" << counter;
                string str = ss.str();
                std::string filename = descriptorsPath + str
                    + ".yml";
                FileStorage fs(filename, FileStorage::WRITE);
                fs << "faceDescriptor" << faceDescriptor; // write face features
                fs.release();

                std::cout
                    << "--- Saved descriptors&features files number: "
                    << (counter + 1) << " ---" << std::endl;
                counter++;

              }
              else
              {
                //save new number_of_users
                ofstream SaveFile(num_users_filePath.c_str());
                SaveFile << number_of_users;
                SaveFile.close();
                //close node
                cout << "--- Saved all descriptors ---" << endl;
                sc.say("Finished");
                cv::destroyWindow("TRAINING");
                _finish = true;
                ros::shutdown();
                return 0;
              }
            }

            if(!_finish)
            {
              //update display
              cv::imshow("TRAINING", imageToDisplay);
              cv::waitKey(1);
              //
            }

          }
        }
      }
      else
      {
        if (!coutFlag)
        {
          cout << "--- Waiting PointCloud message ---" << endl;
          coutFlag = true;
        }
      }

    }
    else
    {
      //if not identifying, leave processor time for nodes moving the robot
      ros::Duration(1).sleep();
      timeFromStart.restart();
      code.restart();
    }
  }
  //

  cout << "--- Node onlineIdentification will end ---" << endl;
  sc.say("Closing");
  sleepok(2, nh);

  return 0;
}

