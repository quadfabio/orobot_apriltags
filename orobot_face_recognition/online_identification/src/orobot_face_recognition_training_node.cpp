/*
 * mainIdentification.cpp
 *
 *  Created on: Oct 17, 2012
 *      Author: Matteo Munaro
 */

// ROS includes:
#include <ros/ros.h>
#include <ros/package.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#include <onlineIdentification/onlineIdFunctions.h>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <opt_msgs/TrackArray.h>
#include <open_ptrack/opt_utils/conversions.h>
#include <orobot_msgs/FaceRecognition.h>

#include <opencv2/opencv.hpp>

#include <sound_play/sound_play.h>

namespace enc = sensor_msgs::image_encodings;


cv::Mat depthDistortionParameters(1, 5, CV_32F);

cv::Mat _image;
opt_msgs::TrackArray _track_array;
Eigen::Matrix3f _intrinsics;
bool  _sync_callback(false), _info_callback(false);

void
cameraInfoCb(const sensor_msgs::CameraInfoConstPtr& camera_info)
{
  _info_callback = true;
  for(uint i = 0; i < 3; ++i)
    for(uint j = 0; j < 3; ++j)
      _intrinsics(i,j) = camera_info->K[i * 3 + j];
}

void
syncCb(const sensor_msgs::ImageConstPtr& image_msg,
       const opt_msgs::TrackArrayConstPtr& track_array_msg)
{
  try
  {
    cv::cvtColor(cv_bridge::toCvCopy(image_msg, enc::BGR8)->image,
                 _image,
                 CV_BGR2GRAY);
  } catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }
  _track_array = *track_array_msg;
  _sync_callback = true;
}

bool
selectFace(FaceForest& face_forest_object, cv::Mat& image,
	   std::vector<Face>& faces, const cv::Point& shoulder_center,
	   const cv::Point head)
{
  cv::Point head_center((shoulder_center.x + 2 * head.x) / 3,
                        (shoulder_center.y + 2 * head.y) / 3);
  //evaluate head dimension (to decide the box dimensions for face recognition)
  std::complex<float> points_difference(shoulder_center.x - head.x,
                                        shoulder_center.y - head.y);
  float head_dimension = std::abs(points_difference);
  // Select box around head && inside the rgb image:
  cv::Point top_left_corner(std::max((float) 0, head_center.x - head_dimension),
                            std::max((float) 0, head_center.y - head_dimension)
                            );
  cv::Point bottom_right_corner(
        std::min((float) image.cols - 1, head_center.x + head_dimension),
        std::min((float) image.rows - 1, head_center.y + head_dimension));
  int roiWidth = bottom_right_corner.x - top_left_corner.x + 1;
  int roiHeight = bottom_right_corner.y - top_left_corner.y + 1;
  //check if roi is inside the image
  if (top_left_corner.x >= 0 && top_left_corner.x < image.cols
      && bottom_right_corner.x >= 0 && bottom_right_corner.x < image.cols
      && top_left_corner.y >= 0 && top_left_corner.y < image.rows
      && bottom_right_corner.y >= 0 && bottom_right_corner.y < image.rows)
  {

    //Select region of interest
    cv::Mat roi(image,
                cv::Rect(top_left_corner.x, top_left_corner.y,
                         (bottom_right_corner - top_left_corner).x,
                         (bottom_right_corner - top_left_corner).y));

    face_forest_object.analize_image(image, faces);

    if (faces.size() > 1)		// If more than one face is detected
    {
      // choose the biggest one
      int biggest_width = 0;
      int biggest_index = 0;
      for (uint i = 0; i < faces.size(); i++)
      {
        if (faces[i].bbox.width > biggest_width)
        {
          biggest_width = faces[i].bbox.width;
          biggest_index = i;
        }
      }
      // put the selected face at the first position
      faces[0] = faces[biggest_index];
      return true;
    }
  }
  else
  {
    ROS_ERROR("Roi out of image");
  }
  return false;
}

int main(int argc, char** argv)
{

  ros::init(argc, argv, "onlineId");

  ros::NodeHandle node_handle;
  ros::NodeHandle private_node_handle("~");

  // Parameters reading
  int seconds_to_take_decision(1);
  int cycle_rate(30);
  double matches_percentage(0.5);
  int reidentification_mode(0);
  bool camera_info_from_topic(false);
  int surf_descriptor_dimension(64);
  int number_of_fiducial_points(10);
  int keypoint_size(6);
  if(!private_node_handle.getParam("camera_info_from_topic",
                                   camera_info_from_topic))
  {
    ROS_WARN("camera_info_from_topic not set, assuming false => "
             "default intrinsics");
    _intrinsics << 365.9466857910156, 0, 257.2561950683594,
        0, 365.9466857910156, 202.9217071533203,
        0, 0, 1;
  }
  if(!private_node_handle.getParam("cycle_rate", cycle_rate))
  {
    ROS_WARN("cycle_rate not set, assuming 30Hz");
  }
  if(!private_node_handle.getParam("seconds_to_take_decision",
                                   seconds_to_take_decision))
  {
    ROS_WARN("seconds_to_take_decision not set, assuming 1");
  }
  if(!private_node_handle.getParam("reidentification_mode",
                                   reidentification_mode))
  {
    ROS_WARN("reidentification_mode not set, assuming 0 (Nearest Neighbor)");
  }
  if(!private_node_handle.getParam("number_of_fiducial_points",
                                   number_of_fiducial_points))
  {
    ROS_WARN("number_of_fiducial_points not set, assuming 10");
  }
  if(!private_node_handle.getParam("surf_descriptor_dimension",
                                   surf_descriptor_dimension))
  {
    ROS_WARN("surf_descriptor_dimension not set, assuming 64");
  }
  if(!private_node_handle.getParam("keypoint_size", keypoint_size))
  {
    ROS_WARN("keypoint_size not set, assuming 6");
  }
  ros::Subscriber camera_info_sub;
  if(camera_info_from_topic)
  {
    camera_info_sub= node_handle.subscribe
        <sensor_msgs::CameraInfo>("camera_info", 1, cameraInfoCb);
  }
  message_filters::Subscriber<sensor_msgs::Image> image_sub
      (node_handle, "image", 1);
  message_filters::Subscriber<opt_msgs::TrackArray> tracks_sub
      (node_handle, "/tracks", 1);
  typedef message_filters::sync_policies::ApproximateTime
      <sensor_msgs::Image, opt_msgs::TrackArray> MSP;
  message_filters::Synchronizer<MSP> sync (MSP(50), image_sub, tracks_sub);
  sync.registerCallback(boost::bind(&syncCb, _1, _2));
  ros::Publisher pub = node_handle.advertise<orobot_msgs::FaceRecognition>
      ("/face_match", 1);

  CandidateUsers cand_us;
  while(!_info_callback && camera_info_from_topic)
  {
    ROS_WARN("Waiting for camera_info topic");
    ros::spinOnce();
  }
  onlineIdFunctions onlineId
      ( _intrinsics(0,0), _intrinsics(0,1), _intrinsics(0,2),
        _intrinsics(1,0), _intrinsics(1,1), _intrinsics(1,2),
        _intrinsics(2,0), _intrinsics(2,1), _intrinsics(2,2));

  // descriptors reading
  std::string descriptors_path =
      ros::package::getPath("online_identification") + "/descriptors/";
  std::string num_users_file_path =
      ros::package::getPath("online_identification") +
      "/descriptors/number_of_users.txt";
  std::string unknown_path =
      ros::package::getPath("online_identification") +
      "/descriptors/unknown.jpg";
  std::string face_config_dir =
      ros::package::getPath("online_identification") + "/data/";

  // INITIALIZATION HEAD POSE ESTIMATION AND FACE FEATURES DETECTION
  std::string face_cascade = face_config_dir
      + "haarcascade_frontalface_alt.xml";
  std::string headpose_config_file = face_config_dir + "config_headpose.txt";
  std::string ffd_config_file = face_config_dir + "config_ffd.txt";
  ForestParam mp_param;
  loadConfigFile(ffd_config_file, mp_param);
  FaceForestOptions faceForestOption;
  faceForestOption.face_detection_option.path_face_cascade = face_cascade;
  ForestParam head_param;
  loadConfigFile(headpose_config_file, head_param);
  faceForestOption.head_pose_forest_param = head_param;
  faceForestOption.mp_forest_param = mp_param;
  faceForestOption.mp_forest_param.treePath = face_config_dir + "trees_ffd/";
  faceForestOption.head_pose_forest_param.treePath = face_config_dir
      + "trees_headpose/tree_";
  FaceForest faceForestObject = FaceForest(faceForestOption);

  // Read number of users
  int number_of_users = 0;
  ifstream read_file(num_users_file_path.c_str());
  read_file >> number_of_users;

  // Initialization
  cv::Mat descriptors;  // each row a descriptor. User i has descriptors in rows
  // 30 * i ... 30 * i + 29
  std::vector<cv::Mat> users_faces; // jpg files with the face of users,
  // index i->user i
  std::vector<cv::DMatch> match;
  cv::FlannBasedMatcher matcher;
  cv::Mat h;
  float b = 0;
  cv::Mat unknown = cv::imread(unknown_path.c_str());
  std::vector<float> med;
  std::vector<float> dev;
  onlineId.fillMedDevWithZero(med, dev, 0);

  number_of_users++;

  // Reading descriptors
  //  std::cout << "--- Reading descriptors. ---" << std::endl << "--- 0%  ";
  //  onlineId.readDescriptors(descriptors, users_faces, number_of_users,
  //                           descriptors_path, 0);
  //  std::cout << "  50%  ";
  //  if (reidentification_mode == 0) // NN
  //  {
  //    onlineId.calculateMeanStdDev(descriptors, med, dev, number_of_users);
  //    onlineId.normalizeData(descriptors, med, dev);
  //    std::cout << "100% ---" << endl;
  //  }
  //  else if (reidentification_mode == 1) // SVM
  //  {
  //    onlineId.read_h_b(h, b, 0);
  //    std::cout << "100% ---" << endl;
  //  }
  //  std::cout << "--- Descriptors loaded and normalized. ---" << std::endl;

  Timing timeFromStart;
  Timing code;
  int counter = 0; // Frames counter

  ros::Rate rate(30);

  cv::namedWindow("test", CV_WINDOW_AUTOSIZE);

  while(ros::ok())
  {
    while(!_sync_callback)
    {
      rate.sleep();
      ros::spinOnce();
    }
    _sync_callback = false;

    for (uint ind = 0; ind < _track_array.tracks.size(); ind++)
    {
      // Current track
      const opt_msgs::Track& current_track = _track_array.tracks[ind];

      // Head ROI estimation
      const opt_msgs::BoundingBox2D& box_2d = current_track.box_2D;

      // Face selection
      std::vector<Face> faces;
      cv::Point shoulder_center(current_track.y - box_2d.y,
                                (current_track.x - box_2d.x) / 4);
      cv::Point head(box_2d.y, current_track.x);
      if(!selectFace(faceForestObject, _image, faces, shoulder_center, head))
      {
        ROS_WARN("User %d has the face not visible", current_track.id);
        continue;
      }

      // Face descriptor computation
      cv::Mat face_descriptor(surf_descriptor_dimension,
                              number_of_fiducial_points, CV_32FC1);
      FaceForest::compute_face_descriptor(
            _image, faces[0],
          face_descriptor, keypoint_size, false);

      if (counter < 30)
      {
        // Save the face descriptor:
        std::stringstream ss;
        ss << number_of_users << "_" << counter;
        std::string str = ss.str();
        std::string filename = descriptors_path + str
            + ".yml";
        cv::FileStorage fs(filename, cv::FileStorage::WRITE);
        fs << "faceDescriptor" << face_descriptor; // write face features
        fs.release();
        //

//        //save skeleton features:
//        ss.str(std::string());
//        ss << number_of_users << "s_" << counter;
//        str = ss.str();
//        filename = descriptorsPath + str + ".yml";
//        FileStorage fs1(filename, FileStorage::WRITE);
//        fs1 << "skeletonFeatures" << skeletonFeatures; // write skeleton features
//        fs1.release();
//        //

        std::cout
            << "--- Saved descriptors&features files number: "
            << (counter + 1) << " ---" << std::endl;
        counter++;
        //									}
      }
      else
      {
        //save new number_of_users
        ofstream SaveFile(num_users_file_path.c_str());
        SaveFile << number_of_users;
        SaveFile.close();
        //close node
        std::cout << "--- Saved all descriptors ---" << std::endl;
        break;
      }

    } // Tracks cycle
    rate.sleep();
    ros::spinOnce();
  } // Main ROS cycle

  ros::shutdown();

  return 0;
}
