# README #

Orobot - Turtlebot with Apriltags

Per ora la documentazione in italiano per semplicità. Successivamente verrà tradotta.

### write_markers ###

Salva la posa dei marker dei tag riconosciuti durante la navigazione. I tag vengono salvati in un file YAML con tutte le informazioni necessarie per essere pubblicati come marker di RViz.

```
#!c++

roslaunch apriltags write_markers.launch
```


### vis_markers ###

Semplice visualizzazione delle detection in real time.


```
#!c++

roslaunch apriltags vis_markers.launch

```

### estimated_localization.launch ###

Stima della posizione del frame /base_footprint dai tag caricati dal file YAML. Per ora il nodo si limita alla pubblicazione della /tf stimata e di un marker per la visualizzazione della stima su RViz.
Alla versione attuale deve essere corretta la concatenazione delle /tf.

```
#!c++

roslaunch apriltags estimated_localization.launch
```

