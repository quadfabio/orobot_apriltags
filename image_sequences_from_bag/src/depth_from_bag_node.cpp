#include <iomanip>
#include <iostream>
#include <fstream>

#include <ros/ros.h>

#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <sensor_msgs/PointCloud2.h>
#include <pcl_ros/io/pcd_io.h>

#include <depth_image_proc/depth_conversions.h>
#include <image_geometry/pinhole_camera_model.h>

using namespace sensor_msgs;
using namespace message_filters;

typedef sensor_msgs::PointCloud2 PointCloudT;

std::string path = "/data/fall_detection_lab/lab2";
std::ofstream cloud_count_header;
std::ofstream rgb_count_header;
std::ofstream depth_count_header;
int cloud_counter = 0;
int depth_counter = 0;
int rgb_counter = 0;
int number_of_digits = 5;

/*
void
test(const sensor_msgs::ImageConstPtr & depth_msg,
     sensor_msgs::PointCloud2::Ptr & cloud_msg,
     const image_geometry::PinholeCameraModel & model)
{
    depth_image_proc::convert<uint16_t>(depth_msg,
        cloud_msg,
        model);
}
*/

void callback(const PointCloudT::ConstPtr& cloud,
  const sensor_msgs::ImageConstPtr & depth_img_msg,
  const sensor_msgs::ImageConstPtr & rgb_img_msg)
{
    // cloud

    /*
    std::stringstream cloud_ss;
    cloud_ss << path << "/cloud/cloud_"
      << std::setfill('0') << std::setw(number_of_digits) << cloud_counter 
      << ".pcd";

    //pcl::io::savePCDFile(cloud_ss.str(),*cloud); // memory leak!

    cloud_count_header << cloud_counter << " " << cloud->header.stamp << std::endl;

    std::cout << "cloud_counter" << cloud_counter << std::endl;
    cloud_counter++;
    */

    // raw depth

    cv_bridge::CvImagePtr depth_cv_ptr = cv_bridge::toCvCopy(depth_img_msg, depth_img_msg->encoding); // 16UC1, 8UC1
    cv::Mat depth_frame = depth_cv_ptr->image;

    std::stringstream raw_depth_ss;
    raw_depth_ss << path << "/raw_depth/raw_depth_"
       //<< cv_ptr->header.stamp
       << std::setfill('0') << std::setw(number_of_digits) << depth_counter
       << ".png";
    cv::imwrite(raw_depth_ss.str(), depth_frame);

    // normalized depth

    //Normalize the pixel value
    //cv::normalize(depth_frame, adjMap, 1, 0, cv::NORM_MINMAX);
    double min;
    double max;
    cv::minMaxIdx(depth_frame, &min, &max);
    cv::Mat adjMap;
    cv::convertScaleAbs(depth_frame, adjMap, 255 / max);

    std::stringstream depth_ss;
    depth_ss << path << "/depth/depth_"
       //<< cv_ptr->header.stamp
       << std::setfill('0') << std::setw(number_of_digits) << depth_counter
       << ".jpg";
    cv::imwrite(depth_ss.str(), adjMap);

    depth_count_header << depth_counter << " " << depth_cv_ptr->header.stamp << std::endl;

    std::cout << "depth_counter" << depth_counter << std::endl;
    depth_counter++;

    // rgb

    cv_bridge::CvImagePtr rgb_cv_ptr = cv_bridge::toCvCopy(rgb_img_msg, rgb_img_msg->encoding); // 16UC1, 8UC1
    cv::Mat rgb_frame = rgb_cv_ptr->image;

    std::stringstream rgb_ss;
    rgb_ss << path << "/rgb/rgb_"
       //<< cv_ptr->header.stamp
       << std::setfill('0') << std::setw(number_of_digits) << rgb_counter
       << ".jpg";
    cv::imwrite(rgb_ss.str(), rgb_frame);

    rgb_count_header << rgb_counter << " " << rgb_cv_ptr->header.stamp << std::endl;

    std::cout << "rgb_counter" << depth_counter << std::endl;
    rgb_counter++;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "vision_node");

  ros::NodeHandle nh;

  cloud_count_header.open ((path + "/cloud_count_header.txt").c_str());
  rgb_count_header.open ((path + "/rgb_count_header.txt").c_str());
  depth_count_header.open ((path + "/depth_count_header.txt").c_str());
  cloud_count_header << "cloud" << " " << "cloud_cv_ptr->header.stamp" << std::endl;
  rgb_count_header << "rgb" << " " << "rgb_cv_ptr->header.stamp" << std::endl;
  depth_count_header << "depth" << " " << "depth_cv_ptr->header.stamp" << std::endl;

  message_filters::Subscriber<PointCloudT> cloud_sub(nh, "/kinect2_head/depth_ir/points", 0);
  message_filters::Subscriber<Image> depth_image_sub
            (nh, "/kinect2_head/depth_rect/image_repub", 0); // 0 infinite queue
  message_filters::Subscriber<Image> rgb_image_sub
            (nh, "/kinect2_head/rgb_rect/image_repub", 0); // 0 infinite queue

  typedef sync_policies::ApproximateTime<PointCloudT, Image, Image> MySyncPolicy;
  // ApproximateTime takes a queue size as its constructor argument,
  // hence MySyncPolicy(10)
  Synchronizer<MySyncPolicy> sync(MySyncPolicy(100), cloud_sub, depth_image_sub, rgb_image_sub);
  sync.registerCallback(boost::bind(&callback, _1, _2, _3));

  ros::spin();

  cloud_count_header.close();
  depth_count_header.close();
  rgb_count_header.close();

  return 0;
}
