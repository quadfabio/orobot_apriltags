cmake_minimum_required(VERSION 2.8.3)
project(image_sequences_from_bag)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  image_transport
  cv_bridge
  sensor_msgs
  message_filters
  depth_image_proc
  image_geometry
)

find_package(OpenCV REQUIRED)
include_directories(${OpenCV_INCLUDE_DIRS})

find_package(PCL 1.7 REQUIRED)
include_directories(${PCL_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})

catkin_package(
)

include_directories(
  ${catkin_INCLUDE_DIRS}
)

add_executable(depth_from_bag_node src/depth_from_bag_node.cpp)

target_link_libraries(depth_from_bag_node
  ${catkin_LIBRARIES}
  ${OpenCV_LIBRARIES}
  ${PCL_LIBRARIES}
)
