orobot_teleop
================

Turtlebot Teleoperation implementation. 
This package used to be in orobot_apps repository. It has been temporarily migrated into orobot 
because it is useful for both robot(orobot_apps) side and user side pc(orobot_interactions). 
