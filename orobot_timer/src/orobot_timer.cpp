#include <orobot_timer/orobot_timer.h>

namespace orobot
{
    void OrobotTimer::callback(const ros::TimerEvent& timer_event)
    {
        ROS_INFO_STREAM(name_ << ": Event! " << timer_event.current_real);
        activation_flag_ = true;
    }

    void OrobotTimer::readParametersFromYAML()
    {
        private_nh_.param("callback_rate", callback_rate_, 60.0);
        private_nh_.param("name", name_, std::string("raw_timer"));
    }
}
