#include <orobot_timer/orobot_timer.h>
#include <sound_play/sound_play.h>
#include <orobot_speech_recognition/SpeechRecognition.h>
#include <ros/package.h>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "test_orobot_timer");

    orobot::OrobotTimer orobot_timer;

    ros::NodeHandle nh;
    orobot_speech_recognition::SpeechRecognition msg;

    ros::ServiceClient client = nh.serviceClient<orobot_speech_recognition::SpeechRecognition>("speech_recognition");

    ros::Rate rate(60);
    while(ros::ok())
    {
        if(orobot_timer.isActive())
        {
            std::stringstream ss;
            ss << "canberra-gtk-play -f "<<
                  ros::package::getPath("orobot_timer") <<
                  "/conf/medicina.ogg";
            int ret = system(ss.str().c_str());
            //sc.say("Hai preso la medicina?", "voice_lp_diphone");
            msg.request.junk = 0;
            if(client.call(msg))
            {
                switch(msg.response.response)
                {
                case 0:
                {
                    ROS_INFO_STREAM("Medicina non presa");
                    std::stringstream ss2;
                    ss2 << "canberra-gtk-play -f "<<
                          ros::package::getPath("orobot_timer") <<
                          "/conf/male.ogg";
                    ret = system(ss2.str().c_str());
                    ros::Duration(30.0).sleep();
                    break;
                }
                case 1:
                {
                    ROS_INFO_STREAM("Medicina presa!");
                    std::stringstream ss3;
                    ss3 << "canberra-gtk-play -f "<<
                          ros::package::getPath("orobot_timer") <<
                          "/conf/bravo.ogg";
                    ret = system(ss3.str().c_str());
                    orobot_timer.resetTimer();
                    break;
                }
                default:
                    ROS_ERROR_STREAM("Error, Google had not understood the audio or limit exceeded");
                }
            }
            else
            {
                ROS_ERROR_STREAM("Error! Cannot call the speech_recognition service!");
            }
        }

        ros::spinOnce();
        rate.sleep();
    }

    ros::shutdown();
    return 0;
}
